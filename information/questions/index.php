<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(""); ?> 
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "info",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div> 
<div id="rightArea">
<?$APPLICATION->SetTitle('Вопрос эксперту'); ?>
<?$APPLICATION->AddChainItem('Вопрос эксперту'); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1>Вопрос эксперту</h1>
<h2>Задать вопрос</h2>
<? include($_SERVER['DOCUMENT_ROOT'].'/form/question.php'); ?>
</div>
 <? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>