<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?

//подкючаем инфоблоки
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/metro.php');

?>

<div id="leftArea"><?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "info",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?><?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
	<div id="filter">
		<span id="filterName">Фильтр</span>
		<div id="areas">
			<ul class="clearfix">
				<li id="all" class="active"><a onclick="setArea('all',<?echo '\''.$_GET['url'].'\'';?>)" class="ajax">Все округа</a></li>
				<? //выбираем все округа
				$fl_areas = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 10, "ACTIVE"=>"Y")); 
				while($one = $fl_areas->GetNextElement()){ $oneId = $one->getFields(); ?>
					<li id="<?echo $oneId['ID'];?>"><a onclick="setArea(<?echo $oneId['ID'].',\''.$_GET['url'].'\'';?>)" class="ajax"><? echo $oneId['NAME']; ?></a></li>
				<? }; ?>
			</ul>
		</div>
		<div id="metro">
			<span class="inputName">Поиск по станции метро:</span>
			<input type="text" onkeyup="" value="" id="autoMetro" />
			<div id="findMetroId"><ul></ul></div>
		</div>
		<div id="street">
			<span class="inputName">Поиск по улице:</span>
			<input type="text" onkeyup="" value="" id="autoStreet"/>
			<div id="findStreetId"><ul></ul></div>
		</div>
	</div>
</div>
 
<div id="rightArea">
<?$APPLICATION->SetTitle("Полезная информация"); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1>Полезная информация</h1>
<div id="showHideMap">
	<img src="/images/map.png" alt="">
	<a id="show">Показать карту</a>
	<a id="hide">Скрыть карту</a>
</div>
<div id="map_holder">
	<div id="map_canvas" style="width:100%;"></div>
</div>
<p>На этой странице вы сможете найти информацию об основных понятих в недвижимости, организациях г. Москвы необходимых для работы с недвижимостью, а так же информацию по странам, в которых мы занимаемся продажей недвижимости.</p>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes_v2",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>

<? //собираем все типы для цикла
	$items = array(); $itemTypes = array(); $itemTypesName = array(); $sectionMarkersArray = array(); $streets = array();
	$sections = CIBlockElement::GetList(Array("ID" => "ASC"), Array("IBLOCK_ID" => 12, "ACTIVE"=>"Y"));
	while($section = $sections->GetNextElement()){
		
		$sectionMarkers = NULL;
		
		//собираем свой массив с типами
		$sectionFields = $section->GetFields(); 
		$sectionProps = $section->GetProperties();
		$itemTypes[$sectionFields['ID']] = $sectionProps;
		$itemTypesName[$sectionFields['ID']] = $sectionFields;
		
		//собираем все объекты типа в цикле
		$objects = CIBlockElement::GetList(Array("NAME" => "ASC"), Array("IBLOCK_ID" => 13, "ACTIVE"=>"Y", "PROPERTY_24" => $sectionFields['ID']));
		while($item = $objects->GetNextElement()){
			//собираем свой массив с позициями
			$itemId = $item->GetFields();
			$itemProps = $item->GetProperties();
			//проверяем есть ли координаты Google Maps
			if($itemProps['g_maps']['VALUE']){
				$items[$itemId['ID']] = $itemProps;
			};
			$streets[] = $itemProps['street']['VALUE'];
		};
		
		foreach($items as $key => $oneItem){		
			//собираем закрытие блоков для карты
			$closeArray .= 'infowindow'.$key.'.close(); ';
			//собираем все маркеры
			$sectionMarkers .= 'marker'.$key.',';
		};
		
		//убираем последнюю запятую из строки
		$sectionMarkers = substr($sectionMarkers, 0, -1);
		$sectionMarkersArray[$sectionFields['ID']] = $sectionMarkers;
	};
	
	$streets = array_unique($streets);

?>
</div>
<script type="text/javascript">
function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(55.749792,37.6324949),
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);
		
	<? foreach($items as $key => $oneItem){ ?>
		
		var marker<?echo $key;?> = new google.maps.Marker({
			icon: '/images/info/<?echo $itemTypes[$oneItem['type']['VALUE']]['url']['VALUE'];?>/icon.png',
			position: new google.maps.LatLng(<?echo $oneItem['g_maps']['VALUE'];?>),
			map: map,
		});
		
		var contentString<?echo $key;?> = '<div class=\"g_maps_window\">'+
			'<div><strong><a href=\"http://<?echo $_SERVER['HTTP_HOST'];?>/information/institutions/<?echo $oneItem['url']['VALUE'];?>\"><?echo $oneItem['title']['VALUE'];?></a></strong></div>'+
			'<div class="section"><?echo $itemTypesName[$oneItem['type']['VALUE']]['NAME'];?></div>'+
			'<div><strong>Районы:</strong> <? $dis = str_replace("\r\n",'', $oneItem['district']['VALUE']['TEXT']); $dis = str_replace("\n",'',$dis); echo $dis;;?></div>'+
			'<div><strong>Адрес:</strong> <?echo $oneItem['address']['VALUE'];?></div>'+
			'<div><strong>Телефоны:</strong> <? $phone = htmlspecialchars_decode($oneItem['phone']['VALUE']); $phone = str_replace("<br/>",' ', $phone); echo $phone;;?></div>'+
			'<div><strong>Сайт:</strong> <a href="<?echo $oneItem['site']['VALUE'];?>" target="_blank" rel="nofollow"><?echo $oneItem['site']['VALUE'];?></a></div>'+
		'</div>';
		
		var infowindow<?echo $key;?> = new google.maps.InfoWindow({
		    content: contentString<?echo $key;?>
		});
		
		google.maps.event.addListener(marker<?echo $key;?>, 'click', function() {
			close();
			infowindow<?echo $key;?>.open(map,marker<?echo $key;?>);
		});
		
	<? }; ?>
	
	<? foreach($itemTypes as $key => $oneType){ ?>
	
	var markers = [];

		markers.push(<?echo $sectionMarkersArray[$key]; ?>);
		
		var styles = [[{
			width:17,
			height:27,
			url:"/images/info/<?echo $oneType['url']['VALUE'];?>/icon.png",
			opt_markerAnchor:[26,8],
			opt_hoverUrl:"/images/info/<?echo $oneType['url']['VALUE'];?>/icon-a.png",
			textColor: '#fff',
			textSize: 10,
			anchor:[3],
		}]];
		
		var mcOptions = {gridSize: 50, maxZoom: 16, styles: styles[0]};
		
		var markerCluster = new MarkerClusterer(map, markers, mcOptions);
	
	<? }; ?>
	
	function close(){
		<?echo $closeArray;?> 
	}
};
google.maps.event.addDomListener(window, 'load', initialize);

$(function() {

	var metro = [<? foreach($stations as $key => $oneItem){ echo "{label:'{$oneItem[1]}',value:'{$oneItem[1]}',id:'{$oneItem[0]}',stationId:'{$key}'},"; }; ?>];
	var images = {<? foreach($branchs as $key => $oneItem){ echo "'{$key}': '".CFile::GetPath($branchs[$key][2])."', "; }; ?>};
	var streets = [<? foreach($streets as $street){ echo "'{$street}',"; }; ?>];
	
	$("#autoMetro").autocomplete({
		source: metro,
		minLength: 2,
		select: function (event, ui) {
			var img = new Image;
			img.src = images[ui.item.id];
			setMetro(ui.item.stationId, <?echo "'".$_GET['url']."'";?>, img, ui.item.label);
		}
     }).data("ui-autocomplete")._renderItem = function (ul, item) {
   		 var img = new Image;
		 img.src = images[item.id];
         return $("<li></li>")
             .data("item.autocomplete", item)
             .append($("<a></a>").append(img).append( item.label ))
             .appendTo(ul);
     };
     
     $("#autoStreet").autocomplete({
         source: streets,
         minLength: 2,
         select: function (event, ui) {
			setStreet(ui.item.label, <?echo "'".$_GET['url']."'";?>);
		}
     })
	 
});

</script>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>