<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if(CModule::IncludeModule("iblock")){

    $res = CIBlockElement::GetList(
		array("ID" => "ASC"),
		array("IBLOCK_ID" => 12, "ACTIVE" => "Y"),
		false,
		false,
		array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "CODE")
	);
	
	$aMenuLinksExt = array();
	
	$aMenuLinksExt[] = Array(
        'Гос.учереждения',
        '#',
        array(),
        array(
        	'header' => 1,
        ),
        ""
    );
	
    while($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields(); // берем поля

		if($arFields['PROPERTY_38_URL_VALUE']){
            $url = $arFields['PROPERTY_38_URL_VALUE'].'/';
        }else{
            $url = $arFields['ID'].'/';
        };

        $aMenuLinksExt[] = Array(
            $arFields['NAME'],
            $url,
            Array(),
            Array(),
            ""
        );
    }

}

$aMenuLinks = array_merge($aMenuLinks,$aMenuLinksExt);