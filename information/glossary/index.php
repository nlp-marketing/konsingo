<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?
//подключаем класс обеъектов
	require_once $_SERVER['DOCUMENT_ROOT'].'/class/getlist.php';
	$getlist = new GetList(33);
	$glossaries = $getlist->objects();
?>
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "info",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div>
<div id="rightArea">
<?$APPLICATION->SetTitle('Словарь риелтора'); ?>
<?$APPLICATION->AddChainItem('Словарь риелтора'); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1>Словарь риелтора</h1>
<input id="glossary" type="text" value="" placeholder="поиск" />
<? foreach($glossaries as $item){ ?>
		
	<div class="glossary">
		<div name="<?echo $item[0]['NAME'];?>"><strong><i><?echo $item[0]['NAME'];?></i></strong> — <?echo $item[0]['DETAIL_TEXT'];?></div>
	</div>	
		
<? }; ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>