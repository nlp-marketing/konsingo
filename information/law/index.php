<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?
//подключаем класс обеъектов
	require_once $_SERVER['DOCUMENT_ROOT'].'/class/getlist.php';
	$getlist = new GetList(34);
	$glossaries = $getlist->objects();
?>
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "info",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div>
<div id="rightArea">
<?$APPLICATION->SetTitle('Основные законы'); ?>
<?$APPLICATION->AddChainItem('Основные законы'); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1>Основные законы</h1>
<? foreach($glossaries as $item){ ?>
	<? $file = CFile::GetByID($item[1]['file']['VALUE']); $url = CFile::GetPath($item[1]['file']['VALUE']); $arFile = $file->Fetch(); $size = filesize($_SERVER['DOCUMENT_ROOT'].'/'.$url); ?>
	<div class="law clearfix">
		<div class="right word">
			<div class="ila_fd"><p><a href="<?echo $url;?>"><img align="left" alt="<?echo $item[0]['NAME'];?>" src="/images/fticons/doc.png">Загрузить</a></p><p class="ilfd">Документ Microsoft Word,<br><?echo $size/1024;?> кб</p></div>
		</div>
		<div><a href="<?echo $url;?>"><?echo $item[0]['NAME'];?></a></div>
		<div><?echo $item[0]['PREVIEW_TEXT'];?></div>
	</div>
<? }; ?>
<div class="text">
	<p>По Гражданскому кодексу РФ сделка с недвижимостью – серьезная причина для установления гражданских прав, при необходимости, их изменения, в определенных случаях их прекращения. В столице такая процедура с недвижимостью фиксируется в Управлении ФС картографии, регистрации, кадастра. Причем регистрация сделок с недвижимостью в Москве, подтверждающая их законность, является по сути конечным этапом процедуры получения прав на недвижимость. Между прочим, оспорить эти полномочия на недвижимость, если зарегистрированы они именно на этом уровне, можно только в судебном порядке.</p>
	<p>Оформление сделки в каждом единичном случае требует сбора и точного утверждения необходимой документации. По окончании всех проверок поданных документов (продолжительность - около полумесяца), осуществляется государственная регистрация.</p>
	<p>Для оформления процедуры физические лица представляют документы:</p>
	<ul>
		<li>для удостоверения личности;</li>
		<li>квитанцию, которая фиксирует оплату клиентом госпошлины;</li>
		<li>устанавливающие право, также подтверждающие неоспоримые полномочия на недвижимость;</li>
		<li>договор (в трех экземплярах), устанавливающий куплю-продажу, дарение;</li>
		<li>запись из домовой книги;</li>
		<li>экспликацию БТИ, кадастровый паспорт;</li>
		<li>доверенность на имя представителей сторон, заверенную нотариусом.</li>
		<li>Комплект документов может различаться в зависимости от особенностей проводимой сделки.</li>
	</ul>
</div>
</div>
 <? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>