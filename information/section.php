<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(""); ?> 
<div id="leftArea">
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "info",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?><?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"institutes",
	Array(
		"ROOT_MENU_TYPE" => "institutes",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>

<div id="filter">
	<span id="filterName">Фильтр</span>
	<div id="areas">
		<ul class="clearfix">
			<li id="all" class="active"><a onclick="setArea('all',<?echo '\''.$_GET['url'].'\'';?>)" class="ajax">Все округа</a></li>
			<? //выбираем все округа
			$fl_areas = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 10, "ACTIVE"=>"Y")); 
			while($one = $fl_areas->GetNextElement()){ $oneId = $one->getFields(); ?>
				<li id="<?echo $oneId['ID'];?>"><a onclick="setArea(<?echo $oneId['ID'].',\''.$_GET['url'].'\'';?>)" class="ajax"><? echo $oneId['NAME']; ?></a></li>
			<? }; ?>
		</ul>
	</div>
	<div id="metro">
		<span class="inputName">Поиск по станции метро:</span>
		<input type="text" onkeyup="" value="" id="autoMetro" />
		<div id="findMetroId"><ul></ul></div>
	</div>
	<div id="street">
		<span class="inputName">Поиск по улице:</span>
		<input type="text" onkeyup="" value="" id="autoStreet"/>
		<div id="findStreetId"><ul></ul></div>
	</div>
</div>

</div>
<div id="rightArea">

<?
//собираем линии
$lines = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 6, "ACTIVE"=>"Y")); $branchs = array();
while($branch = $lines->GetNextElement()){	
	//собираем свой массив со станциями
	$branchId = $branch->GetFields();
	$branchProps = $branch->GetProperties();
	$branchs[$branchId['ID']] = $branchProps;
};

//собираем все станции метро
$metro = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y")); $stations = array();
while($station = $metro->GetNextElement()){	
	//собираем свой массив со станциями
	$stationId = $station->GetFields();
	$stationProps = $station->GetProperties();
	$stations[$stationId['ID']] = array($stationProps['branch']['VALUE'],$stationId['NAME']);
};
//выбираем нужный раздел
$section = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 12, "ACTIVE"=>"Y", "PROPERTY_38" => $_GET['url']))->GetNext(); ?>
<?$APPLICATION->SetTitle($section['NAME']); ?>
<?$APPLICATION->AddChainItem($section['NAME']); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1><?echo $section['NAME'];?></h1>
<div id="showHideMap">
	<img src="/images/map.png" alt="">
	<a id="show">Показать карту</a>
	<a id="hide">Скрыть карту</a>
</div>
<? //собираем все объекты нужного раздела
	$objects = CIBlockElement::GetList(Array("NAME" => "ASC"), Array("IBLOCK_ID" => 13, "ACTIVE"=>"Y", "PROPERTY_24" => $section['ID']));
	
	//массивы для слобцов
	$items = array(); $areas = array(); $district = array(); $name = array(); $address = array(); $head = array(); $phone = array(); $site = array(); $name_person = array(); $post_person = array(); $actual_a = array(); $legal_a = array(); $details = array(); $streets = array();
	
	while($item = $objects->GetNextElement()){
		
		//собираем свой массив с позициями
		$itemId = $item->GetFields();
		$itemProps = $item->GetProperties();
		$items[$itemId['ID']] = $itemProps;
		//проверяем есть ли координаты Google Maps
		if($itemProps['g_maps']['VALUE']){
			$itemsMaps[$itemId['ID']] = $itemProps;
		};
		
		//собираем улицы для фильтра
		$streets[] = $itemProps['street']['VALUE'];
	
	};
	
	$streets = array_unique($streets);
	
	foreach($itemsMaps as $key => $oneItem){
		//собираем закрытие блоков для карты
		$closeArray .= 'infowindow'.$key.'.close(); ';
		//собираем все маркеры
		$sectionMarkers .= 'marker'.$key.',';
	}
	
	//убираем последнюю запятую из строки
	$sectionMarkers = substr($sectionMarkers, 0, -1);
	
	foreach($items as $key => $oneItem){
		//смотрим какие столбцы выводить
		$areas[] = $oneItem['areas']['VALUE'];
		$district[] = $oneItem['district']['VALUE'];
		$name[] = $oneItem['title']['VALUE'];
		$address[] = $oneItem['address']['VALUE'];
		$head[] = $oneItem['head']['VALUE'];
		$phone[] = $oneItem['phone']['VALUE'];
		$site[] = $oneItem['site']['VALUE'];
		$name_person[] = $oneItem['name_person']['VALUE'];
		$post_person[] = $oneItem['post_person']['VALUE'];
		$actual_a[] = $oneItem['actual_address']['VALUE'];
		$legal_a[] = $oneItem['legal_address']['VALUE'];
		$details[] = $oneItem['details']['VALUE'];
	};
	
	//чистим массивы от пустышек
	$areas = array_diff($areas, array(''));
	$district = array_diff($district, array(''));
	$site = array_diff($site, array(''));
	$head = array_diff($head, array(''));
	$address = array_diff($address, array(''));
	$actual_a = array_diff($actual_a, array(''));
	$legal_a = array_diff($legal_a, array(''));
	$phone = array_diff($phone, array(''));
	$name_person = array_diff($name_person, array(''));
	$post_person = array_diff($post_person, array(''));
	$details = array_diff($details, array(''));

?>
<div id="map_holder">
	<div id="map_canvas" style="width:100%;"></div>
</div>
<table id="objectsTable" width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr class="first">
		<? if(!empty($areas)){ ?><td>Округ</td><? }; ?>
		<? if(!empty($district)){ ?><td>Районы</td><? }; ?>
		<td>Название организации</td>
		<? if(empty($legal_a) and empty($address) and empty($actual_a)){}elseif(empty($legal_a)){ ?><td>Адрес</td><? }else{ ?><td>Фактический адрес</td><td>Юридический адрес</td><? }; ?>
		<? if(!empty($head)){ ?><td>Руководитель</td><? }; ?>
		<? if(!empty($post_person)){ ?><td>Должность контактного лица</td><? }; ?>
		<? if(!empty($name_person)){ ?><td>Имя контактного лица</td><? }; ?>
		<? if(!empty($phone)){ ?><td>Телефон</td><? }; ?>
		<? if(!empty($site)){ ?><td>Сайт</td><? }; ?>
		<? if(!empty($details)){ ?><td>Реквизиты</td><? }; ?>
	</tr>
<? // выводим таблицу
foreach($items as $key => $oneItem){
	$area = CIBlockElement::GetByID($oneItem['areas']['VALUE'])->GetNext(); ?>
	<tr>
		<? if(!empty($areas)){ ?><td><?echo $area['NAME']; ?></td><? }; ?>
		<? if(!empty($district)){ ?><td><? $dis = str_replace("\r\n",'', $oneItem['district']['VALUE']['TEXT']); $dis = str_replace("\n",'',$dis); echo $dis; ?></td><? }; ?>
		<td><a href="<?echo 'http://'.$_SERVER['HTTP_HOST'].'/information/institutions/'.$oneItem['url']['VALUE']; ?>"><?echo $oneItem['title']['VALUE'];?></a></td>
		<? if(empty($legal_a) and empty($address) and empty($actual_a)){}elseif(empty($legal_a)){ ?><td><?echo $oneItem['address']['VALUE'];?></td><? }else{ ?><td><?echo $oneItem['actual_address']['VALUE'];?></td><td><?echo $oneItem['legal_address']['VALUE'];?></td><? }; ?>
		<? if(!empty($head)){ ?><td><?echo $oneItem['head']['VALUE'];?></td><? }; ?>
		<? if(!empty($post_person)){ ?><td><?echo $oneItem['post_person']['VALUE'];?></td><? }; ?>
		<? if(!empty($name_person)){ ?><td><?echo $oneItem['name_person']['VALUE'];?></td><? }; ?>
		<? if(!empty($phone)){ ?><td><? $phone = htmlspecialchars_decode($oneItem['phone']['VALUE']); echo '<nobr>'.$phone.'</nobr>'; ?></td><? }; ?>
		<? if(!empty($site)){ ?><td><noindex><a href="<?echo $oneItem['site']['VALUE'];?>" rel="nofollow" target="_blank"><?echo $oneItem['site']['VALUE'];?></a></noindex></td><? }; ?>
		<? if(!empty($details)){ ?><td><? $ditail = str_replace("\r\n",'', $oneItem['details']['VALUE']['TEXT']); $ditail = str_replace("\n",'',$ditail); echo htmlspecialchars_decode($ditail); ?></td><? }; ?>
	</tr>
<? }; ?>
</table>
</div>
<script type="text/javascript">
var markersArray = [];
function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(55.749792,37.6324949),
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);
		
	<? foreach($itemsMaps as $key => $oneItem){ ?>
		
		var marker<?echo $key;?> = new google.maps.Marker({
			icon: '/images/info/<?echo $_GET['url'];?>/icon.png',
			position: new google.maps.LatLng(<?echo $oneItem['g_maps']['VALUE'];?>),
			map: map,
		});
		
		markersArray.push(marker<?echo $key;?>);
		
		var contentString<?echo $key;?> = '<div class=\"g_maps_window\">'+
			'<div><strong><a href=\"http://<?echo $_SERVER['HTTP_HOST'];?>/information/institutions/<?echo $oneItem['url']['VALUE'];?>\"><?echo $oneItem['title']['VALUE'];?></a></strong></div>'+
			'<div class="section"><?echo $section['NAME'];?></div>'+
			'<div><strong>Районы:</strong> <? $dis = str_replace("\r\n",'', $oneItem['district']['VALUE']['TEXT']); $dis = str_replace("\n",'',$dis); echo $dis;;?></div>'+
			'<div><strong>Адрес:</strong> <?echo $oneItem['address']['VALUE'];?></div>'+
			'<div><strong>Телефоны:</strong> <? $phone = htmlspecialchars_decode($oneItem['phone']['VALUE']); $phone = str_replace("<br/>",' ', $phone); echo $phone;;?></div>'+
			'<div><strong>Сайт:</strong> <a href="<?echo $oneItem['site']['VALUE'];?>" target="_blank" rel="nofollow"><?echo $oneItem['site']['VALUE'];?></a></div>'+
		'</div>';
		
		var infowindow<?echo $key;?> = new google.maps.InfoWindow({
		    content: contentString<?echo $key;?>
		});
		
		google.maps.event.addListener(marker<?echo $key;?>, 'click', function() {
			close();
			infowindow<?echo $key;?>.open(map,marker<?echo $key;?>);
		});
		
	<? }; ?>
	
	var markers = [];
	
	markers.push(<?echo $sectionMarkers;?>);
	
	var styles = [[{
		width:17,
		height:27,
		url:"/images/info/<?echo $_GET['url'];?>/icon.png",
		opt_markerAnchor:[26,8],
		opt_hoverUrl:"/images/info/<?echo $_GET['url'];?>/icon-a.png",
		textColor: '#fff',
		textSize: 10,
		anchor:[3],
	}]];
	
	var mcOptions = {gridSize: 20, maxZoom: 16, styles: styles[0]};
	
	var markerCluster = new MarkerClusterer(map, markers,mcOptions);
	
	function close(){
		<?echo $closeArray;?> 
	}
	
}

google.maps.event.addDomListener(window, 'load', initialize);

$(function() {

	var metro = [<? foreach($stations as $key => $oneItem){ echo "{label:'{$oneItem[1]}',value:'{$oneItem[1]}',id:'{$oneItem[0]}',stationId:'{$key}'},"; }; ?>];
	var images = {<? foreach($branchs as $key => $oneItem){ echo "'{$key}': '".CFile::GetPath($branchs[$key]['pic']['VALUE'])."', "; }; ?>};
	var streets = [<? foreach($streets as $street){ echo "'{$street}',"; }; ?>];
	
	$("#autoMetro").autocomplete({
		source: metro,
		minLength: 2,
		select: function (event, ui) {
			var img = new Image;
			img.src = images[ui.item.id];
			setMetro(ui.item.stationId, <?echo "'".$_GET['url']."'";?>, img, ui.item.label);
		}
     }).data("ui-autocomplete")._renderItem = function (ul, item) {
   		 var img = new Image;
		 img.src = images[item.id];
         return $("<li></li>")
             .data("item.autocomplete", item)
             .append($("<a></a>").append(img).append( item.label ))
             .appendTo(ul);
     };
     
     $("#autoStreet").autocomplete({
         source: streets,
         minLength: 2,
         select: function (event, ui) {
			setStreet(ui.item.label, <?echo "'".$_GET['url']."'";?>);
		}
     })
	 
});

</script>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>