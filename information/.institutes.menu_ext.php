<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

if(CModule::IncludeModule("iblock")){

	$res = CIBlockElement::GetList(
		array("ID" => "ASC"),
		array("IBLOCK_ID" => 12, "ACTIVE" => "Y"),
		false,
		false,
		array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_URL")
	);

	while($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields(); // берем поля

		if($arFields['PROPERTY_URL_VALUE']){
			$url = $arFields['PROPERTY_URL_VALUE'].'/';
		}else{
			$url = $arFields['ID'].'/';
		};

		$aMenuLinksExt[] = Array(
			$arFields['NAME'],
			'/information/'.$url,
			Array(),
			Array(),
			""
		);
	}

}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);

?>