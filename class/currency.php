<?
	class Currency{
		
		private $number; //число
		private $usd; //35
		private $eur; //49
		
		public function __construct($number){
			
			$this->number = $number;
			
			$currencyUsd = CIBlockElement::GetList(
				array("SORT" => "ASC"), 
				array("IBLOCK_ID" => 18, "ID" => 20652),
				false,
				false,
				array("PROPERTY_curr")
			)->GetNext();

			$this->usd = str_replace(',','.',$currencyUsd['PROPERTY_CURR_VALUE']);
			
			
			$currencyEur = CIBlockElement::GetList(
				array("SORT" => "ASC"), 
				array("IBLOCK_ID" => 18, "ID" => 20653),
				false,
				false,
				array("PROPERTY_curr")
			)->GetNext();
			
			$this->eur = str_replace(',','.',$currencyEur['PROPERTY_CURR_VALUE']);
			
		}
		
		function usd2rub(){
			return $this->number*$this->usd;
		}
		function eur2rub(){
			return $this->number*$this->eur;
		}
		function rub2usd(){
			return $this->number/$this->usd;
		}
		function eur2usd(){
			return $this->number*$this->eur/$this->usd;
		}
		function rub2eur(){
			return $this->number/$this->eur;
		}
		function usd2eur(){
			return $this->number*$this->usd/$this->eur;
		}
		
	}
?>