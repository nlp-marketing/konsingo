<?
	class GetList{
		
		private $idblock;
		private $prop;
		private $sort;
		
		public function __construct($id,$prop,$sort){
			
			$this->id = $id;
			$this->prop = $prop;
			$rhis->sort = $sort;
		}
		
		function objects(){
			
			($this->prop)?$property=$this->prop:$property=array();
			($this->sort)?$sort=$this->sort:$sort=array("SORT"=>"ASC");
			
			$objects = CIBlockElement::GetList(
				$sort, 
				Array(
					"IBLOCK_ID" => $this->id,
					"ACTIVE"=>"Y",
					$property,
				)
			);
			$items = array();
			while($item = $objects->GetNextElement()){
				$itemId = $item->GetFields();
				$itemProps = $item->GetProperties();
				$items[$itemId['ID']] = array($itemId,$itemProps);
			};
			
			return $items;
		}
		
		function prices(){
			
			($this->prop)?$property=$this->prop:$property=array();
			($this->sort)?$sort=$this->sort:$sort=array("SORT"=>"ASC");
		
			$objects = CIBlockElement::GetList(
				$sort, 
				array(
					"IBLOCK_ID" => $this->id,
					"ACTIVE"=>"Y",
					$property,
				),
				false,
				false,
				array("PROPERTY_price","PROPERTY_currency")
			);
			$items = array();
			while($item = $objects->GetNextElement()){
				$itemId = $item->GetFields();
				$items[] = array($itemId['PROPERTY_PRICE_VALUE'],$itemId['PROPERTY_CURRENCY_VALUE']);
			};
			
			return $items;
			
		}
		
	}

?>