<?
	class Values{
		
		public function __construct(){
			
			//подключаем класс обеъектов
			require_once $_SERVER['DOCUMENT_ROOT'].'/class/getlist.php';
			
			//квартиры и комнаты
			$getList = new GetList(16);
			$apartments = $getList->prices();
			
			//недвижимость за границей
			$getList = new GetList(17);
			$abroad = $getList->prices();
			
			//Загородная недвижимость
			$getList = new GetList(20);
			$country = $getList->prices();
			
			//Коммерческая недвижимость
			$getList = new GetList(20);
			$commercial = $getList->prices();
			
			//Аренда жилья
			$getList = new GetList(20);
			$rent = $getList->prices();
			
			$price = array();
			$allobjects = array_merge($apartments,$abroad,$country,$commercial,$rent);
			
			foreach($allobjects as $itemProps){
			
				//цена в разных валютах
				if($itemProps[1]==20652 or $itemProps[1]==20653){
					$price[] = $itemProps[0]*$itemProps[1];
				}else{
					$price[] = intval($itemProps[0]);
				};
			};
			
			//чистим
			$price = array_unique($price);
			$price = array_diff($price,array(0));
			
			$this->price = $price;			
		}
		
		function maxPrice(){
			return round(max($this->price));
		}
		
		function minPrice(){
			return floor(min($this->price));
		}
		
	}

?>