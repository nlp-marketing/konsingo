<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
//$metro = include(__DIR__.'/data-metro.php');
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;
$branches = array();

foreach($metro as $elem) {
    $i+=10;
    $branch = $elem['branch'];
    if(!isset($branches[$branch])){
        $branchID = $branches[$branch] = $block->Add(array(
            "MODIFIED_BY" => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => 6,
            "SORT" => $i,
            "NAME" => trim($branch),
            "PROPERTY_VALUES"=>array(
                "title"=>trim($branch)
            ),
            "ACTIVE" => "Y"
        ));
        if(!$branchID){
            echo '<h1 style="font-size: 140%">Ветку "',$branch,'" и станцию "',$elem['name'],'" не удалось добавить!</h1>';
            continue;
        }
    } else
    $branchID = $branches[$branch];
    $ID = $block->Add(array(
        "MODIFIED_BY" => $USER->GetID(),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => 11,
        "SORT" => $i,
        "NAME" => trim($elem['name']),
        "PROPERTY_VALUES"=>array(
            'title'=>trim($elem['name']),
            'old_id'=>$elem['old_id'],
            'g_maps'=>$elem['x'].','.$elem['y'],
            'branch'=>$branchID,
        ),
        "ACTIVE" => "Y"
    ));
    if(!$ID){
        echo '<h1 style="font-size: 140%">Cтанцию "',$elem['name'],'" не удалось добавить!</h1>';
    }
}

?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>