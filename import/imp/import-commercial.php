<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
$objects = include(__DIR__.'/data-commercial.php');
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;

$currency = array(
	'1'=>'20652',
	'2'=>'20653',
	'3'=>'20654',
);

$locations = array(
	'1'=>'21105',
	'2'=>'21106',
	'3'=>'21107',
);

$districts = array(
	'1'=>array('new'=>'72','name'=>'ЦАО'),
	'2'=>array('new'=>'64','name'=>'САО'),
	'3'=>array('new'=>'71','name'=>'СВАО'),
	'4'=>array('new'=>'70','name'=>'ВАО'),
	'5'=>array('new'=>'69','name'=>'ЮВАО'),
	'6'=>array('new'=>'68','name'=>'ЮАО'),
	'7'=>array('new'=>'67','name'=>'ЮЗАО'),
	'8'=>array('new'=>'66','name'=>'ЗАО'),
	'9'=>array('new'=>'65','name'=>'СЗАО'),
);

$rental = array(
	'1'=>21111,
	'2'=>21112,
);

foreach($objects as $elem){ $i++;
	
	//if($i>5){ break; };
	
	//var_dump($elem);
	
	//дорога
		$road = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['road']))->GetNext();
	//направление
		$direction = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 7, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['direction']))->GetNext();
	//регион
		$region = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 9, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['region']))->GetNext();
	//ищем метро
		$m1 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro1']))->GetNext();
		$m2 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro2']))->GetNext();
		$m3 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro3']))->GetNext();
		$m4 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro4']))->GetNext();
		$m5 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro5']))->GetNext();
		$metro = array();
		$metro[] = $m1['ID'];
		if($m1['ID']!=$m2['ID']){
			$metro[] = $m2['ID'];
		}
		if($m2['ID']!=$m3['ID'] && $m1['ID']!=$m3['ID']){
			$metro[] = $m3['ID'];
		}
		if($m3['ID']!=$m4['ID'] && $m1['ID']!=$m4['ID'] && $m2['ID']!=$m4['ID']){
			$metro[] = $m4['ID'];
		}
		if($m4['ID']!=$m5['ID'] && $m1['ID']!=$m5['ID'] && $m2['ID']!=$m5['ID'] && $m3['ID']!=$m5['ID']){
			$metro[] = $m5['ID'];
		}
		
	//расположение
	if($elem['ring1']==1 and $elem['ring2']==1 and $elem['ring3']==1){
		$location_msk = 21108;
	}elseif($elem['ring1']==1 and $elem['ring2']==1 and $elem['ring3']==0){
		$location_msk = 21109;
	}elseif($elem['ring1']==1 and $elem['ring2']==0 and $elem['ring3']==0){
		$location_msk = 21110;
	}else{
		$location_msk = NULL;
	}
	
	$material = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 21, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['material']))->getNext();
	$type = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 29, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['type']))->getNext();
	$localy = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 8, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['dplace']))->getNext();
	
	//новый елемент
	    $el = new CIBlockElement;
		
	$PROP = array();
	
	$PROP['old_id'] = $elem['sq_id'];
	$PROP['g_maps'] = trim($elem['sq_location_lat']).','.trim($elem['sq_location_long']);
	$PROP['address'] = $elem['address'];
	$PROP['articul'] = $elem['id'];
	$PROP['currency'] = $currency[$elem['currency']];
	$PROP['all_s'] = $elem['maxarea'];
	$PROP['min_all_s'] = $elem['minarea'];
	$PROP['rooms'] = $elem['rooms'];
	$PROP['floor'] = $elem['floor'];
	$PROP['floors'] = $elem['floors'];
	$PROP['metro'] = $metro;
	$PROP['price'] = $elem['maxprice'];
	$PROP['min_price'] = $elem['minprice'];
	$PROP['mprice'] = $elem['maxmprice'];
	$PROP['min_mprice'] = $elem['minmprice'];
	$PROP['main_type'] = 20000;
	$PROP['type'] = $type['ID'];
	$PROP['discount'] = $elem['discount'];
	$PROP['location'] = $locations[$elem['situation']];
	$PROP['location_msk'] = $location_msk;
	$PROP['direction'] = $direction['ID'];
	//$PROP['distance'] = $elem['cdistance'];
	$PROP['road'] = $road['ID'];
	$PROP['region'] = $region['ID'];
	$PROP['areas'] = $districts[$elem['district']]['new'];
	$PROP['rental'] = $rental[$elem['category']];
	$PROP['localy'] = $localy['ID'];
	$PROP['localy_distane'] = $elem['cdistance'];
	$PROP['materials'] = $material['ID'];
	
	$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"      => 30,
		"SORT" 	         => $i,
		"PROPERTY_VALUES"=> $PROP,
		"DETAIL_TEXT_TYPE" => "html",
        "DETAIL_TEXT" => $elem['comment'],
		"NAME"           => trim($elem['name']),
		"ACTIVE"         => "Y",
	);
	
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
		echo "New ID: ".$PRODUCT_ID."<br>";
	else
		echo "Error: ".$el->LAST_ERROR;
	
	echo '<hr/>';

};

?>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>