<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;

$regions = array(
  array('sq_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'3','sq_language'=>'rus','sq_original'=>'3','sq_deleted'=>'0','name'=>'Владимирская Область'),
  array('sq_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'4','sq_language'=>'rus','sq_original'=>'4','sq_deleted'=>'0','name'=>'Тульская область'),
  array('sq_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'5','sq_language'=>'rus','sq_original'=>'5','sq_deleted'=>'0','name'=>'Краснодарский край'),
  array('sq_id'=>'6','sq_visibility'=>'1','sq_ordering'=>'6','sq_language'=>'rus','sq_original'=>'6','sq_deleted'=>'0','name'=>'Астраханская Область'),
  array('sq_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'7','sq_language'=>'rus','sq_original'=>'7','sq_deleted'=>'0','name'=>'Калужская область')
);

$directions = array(
  array('sq_id'=>'1','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'1','sq_language'=>'rus','sq_original'=>'1','sq_deleted'=>'0','name'=>'г. Электросталь'),
  array('sq_id'=>'2','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'2','sq_language'=>'rus','sq_original'=>'2','sq_deleted'=>'0','name'=>'Мышецкое'),
  array('sq_id'=>'3','sq_parent_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'3','sq_language'=>'rus','sq_original'=>'3','sq_deleted'=>'0','name'=>'г. Мытищи'),
  array('sq_id'=>'4','sq_parent_id'=>'6','sq_visibility'=>'1','sq_ordering'=>'4','sq_language'=>'rus','sq_original'=>'4','sq_deleted'=>'0','name'=>'Внуково'),
  array('sq_id'=>'5','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'5','sq_language'=>'rus','sq_original'=>'5','sq_deleted'=>'0','name'=>'г. Одинцово'),
  array('sq_id'=>'6','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'6','sq_language'=>'rus','sq_original'=>'6','sq_deleted'=>'0','name'=>'г. Люберцы'),
  array('sq_id'=>'7','sq_parent_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'7','sq_language'=>'rus','sq_original'=>'7','sq_deleted'=>'0','name'=>'г. Подольск'),
  array('sq_id'=>'8','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'8','sq_language'=>'rus','sq_original'=>'8','sq_deleted'=>'0','name'=>'г. Сходня'),
  array('sq_id'=>'9','sq_parent_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'9','sq_language'=>'rus','sq_original'=>'9','sq_deleted'=>'0','name'=>'г. Пушкино'),
  array('sq_id'=>'10','sq_parent_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'10','sq_language'=>'rus','sq_original'=>'10','sq_deleted'=>'0','name'=>'г. Лосино-Петровский'),
  array('sq_id'=>'12','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'12','sq_language'=>'rus','sq_original'=>'12','sq_deleted'=>'0','name'=>'г. Ногинск'),
  array('sq_id'=>'13','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'13','sq_language'=>'rus','sq_original'=>'13','sq_deleted'=>'0','name'=>'г. Реутов'),
  array('sq_id'=>'14','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'14','sq_language'=>'rus','sq_original'=>'14','sq_deleted'=>'0','name'=>'дер. Гаврилково'),
  array('sq_id'=>'16','sq_parent_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'16','sq_language'=>'rus','sq_original'=>'16','sq_deleted'=>'0','name'=>'г. Юбилейный'),
  array('sq_id'=>'17','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'17','sq_language'=>'rus','sq_original'=>'17','sq_deleted'=>'0','name'=>'г. Солнечногорск'),
  array('sq_id'=>'18','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'18','sq_language'=>'rus','sq_original'=>'18','sq_deleted'=>'0','name'=>'г. Химки'),
  array('sq_id'=>'19','sq_parent_id'=>'6','sq_visibility'=>'1','sq_ordering'=>'19','sq_language'=>'rus','sq_original'=>'19','sq_deleted'=>'0','name'=>'г. Апрелевка'),
  array('sq_id'=>'20','sq_parent_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'20','sq_language'=>'rus','sq_original'=>'20','sq_deleted'=>'0','name'=>'п. Устиновка'),
  array('sq_id'=>'21','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'21','sq_language'=>'rus','sq_original'=>'21','sq_deleted'=>'0','name'=>'п. Немчиновка'),
  array('sq_id'=>'22','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'22','sq_language'=>'rus','sq_original'=>'22','sq_deleted'=>'0','name'=>'Горки-10'),
  array('sq_id'=>'23','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'23','sq_language'=>'rus','sq_original'=>'23','sq_deleted'=>'0','name'=>'Волоколамское шоссе'),
  array('sq_id'=>'24','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'24','sq_language'=>'rus','sq_original'=>'24','sq_deleted'=>'0','name'=>'Новорижское шоссе'),
  array('sq_id'=>'25','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'25','sq_language'=>'rus','sq_original'=>'25','sq_deleted'=>'0','name'=>'г. Красногорск'),
  array('sq_id'=>'26','sq_parent_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'26','sq_language'=>'rus','sq_original'=>'26','sq_deleted'=>'0','name'=>'пос. Нахабино'),
  array('sq_id'=>'27','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'27','sq_language'=>'rus','sq_original'=>'27','sq_deleted'=>'0','name'=>'пгт Свердловский'),
  array('sq_id'=>'28','sq_parent_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'28','sq_language'=>'rus','sq_original'=>'28','sq_deleted'=>'0','name'=>'пгт Октябрьский'),
  array('sq_id'=>'29','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'29','sq_language'=>'rus','sq_original'=>'29','sq_deleted'=>'0','name'=>'г. Железнодорожный'),
  array('sq_id'=>'30','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'30','sq_language'=>'rus','sq_original'=>'30','sq_deleted'=>'0','name'=>'д. Войново-Гора'),
  array('sq_id'=>'31','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'31','sq_language'=>'rus','sq_original'=>'31','sq_deleted'=>'0','name'=>'г. Лобня'),
  array('sq_id'=>'32','sq_parent_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'32','sq_language'=>'rus','sq_original'=>'32','sq_deleted'=>'0','name'=>'Нахабино'),
  array('sq_id'=>'33','sq_parent_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'33','sq_language'=>'rus','sq_original'=>'33','sq_deleted'=>'0','name'=>'г. Люберцы'),
  array('sq_id'=>'34','sq_parent_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'34','sq_language'=>'rus','sq_original'=>'34','sq_deleted'=>'0','name'=>'г. Клин'),
  array('sq_id'=>'35','sq_parent_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'35','sq_language'=>'rus','sq_original'=>'35','sq_deleted'=>'0','name'=>'пос. Горки Ленинские'),
  array('sq_id'=>'36','sq_parent_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'36','sq_language'=>'rus','sq_original'=>'36','sq_deleted'=>'0','name'=>'г. Королев'),
  array('sq_id'=>'37','sq_parent_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'37','sq_language'=>'rus','sq_original'=>'37','sq_deleted'=>'0','name'=>'пос. Горки Ленинские'),
  array('sq_id'=>'38','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'38','sq_language'=>'rus','sq_original'=>'38','sq_deleted'=>'0','name'=>'г. Балашиха'),
  array('sq_id'=>'39','sq_parent_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'39','sq_language'=>'rus','sq_original'=>'39','sq_deleted'=>'0','name'=>'г. Видное'),
  array('sq_id'=>'40','sq_parent_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'40','sq_language'=>'rus','sq_original'=>'40','sq_deleted'=>'0','name'=>'г. Чехов'),
  array('sq_id'=>'41','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'41','sq_language'=>'rus','sq_original'=>'41','sq_deleted'=>'0','name'=>'г. Химки')
);

$materials = array(
  array('sq_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'1','sq_language'=>'rus','sq_original'=>'1','sq_deleted'=>'0','name'=>'Кирпичный'),
  array('sq_id'=>'2','sq_visibility'=>'1','sq_ordering'=>'2','sq_language'=>'rus','sq_original'=>'2','sq_deleted'=>'0','name'=>'Блочный'),
  array('sq_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'3','sq_language'=>'rus','sq_original'=>'3','sq_deleted'=>'0','name'=>'Монолитный'),
  array('sq_id'=>'4','sq_visibility'=>'1','sq_ordering'=>'4','sq_language'=>'rus','sq_original'=>'4','sq_deleted'=>'0','name'=>'Панельный'),
  array('sq_id'=>'5','sq_visibility'=>'1','sq_ordering'=>'5','sq_language'=>'rus','sq_original'=>'5','sq_deleted'=>'0','name'=>'Бревенчатый'),
  array('sq_id'=>'6','sq_visibility'=>'1','sq_ordering'=>'6','sq_language'=>'rus','sq_original'=>'6','sq_deleted'=>'0','name'=>'Брус'),
  array('sq_id'=>'7','sq_visibility'=>'1','sq_ordering'=>'7','sq_language'=>'rus','sq_original'=>'7','sq_deleted'=>'0','name'=>'Пеноблочный'),
  array('sq_id'=>'8','sq_visibility'=>'1','sq_ordering'=>'8','sq_language'=>'rus','sq_original'=>'8','sq_deleted'=>'0','name'=>'Деревянный'),
  array('sq_id'=>'9','sq_visibility'=>'1','sq_ordering'=>'9','sq_language'=>'rus','sq_original'=>'9','sq_deleted'=>'0','name'=>'Деревянно-кирпичный'),
  array('sq_id'=>'10','sq_visibility'=>'1','sq_ordering'=>'10','sq_language'=>'rus','sq_original'=>'10','sq_deleted'=>'0','name'=>'Монолитно-кирпичный'),
  array('sq_id'=>'11','sq_visibility'=>'1','sq_ordering'=>'11','sq_language'=>'rus','sq_original'=>'11','sq_deleted'=>'0','name'=>'Сталинский'),
  array('sq_id'=>'12','sq_visibility'=>'1','sq_ordering'=>'12','sq_language'=>'rus','sq_original'=>'12','sq_deleted'=>'0','name'=>'Хрущевка'),
  array('sq_id'=>'14','sq_visibility'=>'1','sq_ordering'=>'14','sq_language'=>'rus','sq_original'=>'14','sq_deleted'=>'0','name'=>'Элитный')
);

$estate_types = array(
  array('sq_id'=>'3','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'3','sq_language'=>'rus','sq_original'=>'3','sq_deleted'=>'0','sq_unique_name'=>'luxury','name'=>'Апартаменты','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'4','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'4','sq_language'=>'rus','sq_original'=>'4','sq_deleted'=>'0','sq_unique_name'=>'houses','name'=>'Дома','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'5','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'5','sq_language'=>'rus','sq_original'=>'5','sq_deleted'=>'0','sq_unique_name'=>'villas','name'=>'Виллы','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'6','sq_parent_id'=>'1','sq_visibility'=>'0','sq_ordering'=>'6','sq_language'=>'rus','sq_original'=>'6','sq_deleted'=>'0','sq_unique_name'=>'land','name'=>'Земельные участки','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'7','sq_parent_id'=>'1','sq_visibility'=>'0','sq_ordering'=>'7','sq_language'=>'rus','sq_original'=>'7','sq_deleted'=>'0','sq_unique_name'=>'commercial','name'=>'Коммерческая недвижимость','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'8','sq_parent_id'=>'1','sq_visibility'=>'0','sq_ordering'=>'8','sq_language'=>'rus','sq_original'=>'8','sq_deleted'=>'0','sq_unique_name'=>'investment','name'=>'Инвестиционные проекты','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'9','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'9','sq_language'=>'rus','sq_original'=>'9','sq_deleted'=>'0','sq_unique_name'=>'penthouses','name'=>'Пентхаусы','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'10','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'10','sq_language'=>'rus','sq_original'=>'10','sq_deleted'=>'0','sq_unique_name'=>'townhouses','name'=>'Таунхаусы','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'11','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'11','sq_language'=>'rus','sq_original'=>'11','sq_deleted'=>'0','sq_unique_name'=>'resorts','name'=>'Курортные комлексы','thumbnail'=>'','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'12','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'12','sq_language'=>'rus','sq_original'=>'12','sq_deleted'=>'1','sq_unique_name'=>'св._елена','name'=>'Курортный Комплекс Св.&nbsp;Елена','thumbnail'=>'f5839db23b.jpg','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'13','sq_parent_id'=>'3','sq_visibility'=>'1','sq_ordering'=>'13','sq_language'=>'rus','sq_original'=>'13','sq_deleted'=>'1','sq_unique_name'=>'поморие','name'=>'Феста Поморие','thumbnail'=>'e758af5a84.jpg','sq_thumbnail_changes'=>'0'),
  array('sq_id'=>'14','sq_parent_id'=>'1','sq_visibility'=>'1','sq_ordering'=>'14','sq_language'=>'rus','sq_original'=>'14','sq_deleted'=>'0','sq_unique_name'=>'jilay','name'=>'Жилая недвижимость','thumbnail'=>'','sq_thumbnail_changes'=>'0')
);

foreach($estate_types as $elem){ $i++;
	
	$news = NULL;
	
	$news = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 27, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['sq_id']))->GetNext();
	
	if(!$news){
	    
	    $el = new CIBlockElement;
		
		$PROP = array();
		$PROP['old_id'] = $elem['sq_id'];
		
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => 27,
			"SORT" => $i,
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => trim($elem['name']),
			"ACTIVE"         => "Y",
		);
		
		if($PRODUCT_ID = $el->Add($arLoadProductArray))
			echo "New ID: ".$PRODUCT_ID."<br>";
		else
			echo "Error: ".$el->LAST_ERROR;
		
	};
	
	echo '<hr/>';

}

/*
print '<pre>';
var_dump($institution);
print '</pre>';
*/


?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>