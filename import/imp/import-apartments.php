<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
//$objects = include(__DIR__.'/estate-objects.php');
//$objects = include(__DIR__.'/data-abroad.php');
//$objects = include(__DIR__.'/data-zagorod.php');
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;

$currency = array(
	'1'=>'20652',
	'2'=>'20653',
	'3'=>'20654',
);

$districts = array(
	'1'=>array('new'=>'72','name'=>'ЦАО'),
	'2'=>array('new'=>'64','name'=>'САО'),
	'3'=>array('new'=>'71','name'=>'СВАО'),
	'4'=>array('new'=>'70','name'=>'ВАО'),
	'5'=>array('new'=>'69','name'=>'ЮВАО'),
	'6'=>array('new'=>'68','name'=>'ЮАО'),
	'7'=>array('new'=>'67','name'=>'ЮЗАО'),
	'8'=>array('new'=>'66','name'=>'ЗАО'),
	'9'=>array('new'=>'65','name'=>'СЗАО')
);

foreach($objects as $elem){ $i++;
	
	//if($i>1){ break; };
	
	//var_dump($elem);
	
	$one = NULL;
	
	//$one = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 16, "ACTIVE"=>"Y", "NAME" => $elem['name']))->GetNext();
	//$one = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 17, "ACTIVE"=>"Y", "NAME" => $elem['name']))->GetNext();
	$one = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 20, "ACTIVE"=>"Y", "NAME" => $elem['name']))->GetNext();
	
	//if(!$one){
		
		//квартиры
		/*
			
		//ищем метро
		$m1 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro1']))->GetNext();
		$m2 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro2']))->GetNext();
		$m3 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro3']))->GetNext();
		
		$metro = array();
		
		$metro[] = $m1['ID'];
		
		if($m1['ID']!=$m2['ID']){
			$metro[] = $m2['ID'];
		}
		if($m2['ID']!=$m3['ID'] && $m1['ID']!=$m3['ID']){
			$metro[] = $m3['ID'];
		}
	    
	    //новый елемент
	    $el = new CIBlockElement;
		
		$PROP = array();
		
		$PROP['old_id'] = $elem['sq_id'];
		$PROP['metro'] = $metro;
		$PROP['g_maps'] = trim($elem['sq_location_lat']).','.trim($elem['sq_location_long']);
		$PROP['address'] = $elem['address'];
		$PROP['price'] = $elem['price'];
		$PROP['mprice'] = $elem['mprice'];
		$PROP['articul'] = $elem['id'];
		$PROP['all_s'] = $elem['tarea'];
		$PROP['live_s'] = $elem['larea'];
		$PROP['room_s'] = $elem['larea_comment'];
		$PROP['kitchen_s'] = $elem['karea'];
		$PROP['rooms'] = $elem['rooms'];
		$PROP['all_rooms'] = $elem['rooms_total'];
		$PROP['floor'] = $elem['floor'];
		$PROP['all_roof'] = $elem['floors'];
		//$PROP['comment'] = array('VALUE'=> array('TEXT'=>trim($elem['comment'])));
		$PROP['main_type'] = 19995;
		$PROP['areas'] = $districts[$elem['district']]['new'];
		$PROP['discount'] = $elem['discount'];
				
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => 16,
			"SORT" 	         => $i,
			"PROPERTY_VALUES"=> $PROP,
			"DETAIL_TEXT_TYPE" => "html",
	        "DETAIL_TEXT" => $elem['comment'],
			"NAME"           => trim($elem['name']),
			"ACTIVE"         => "Y",
		);
		
		*/
		
		//недвижимость за границей
		/*
		
		//новый елемент
	    $el = new CIBlockElement;
		
		$PROP = array();
		
		$PROP['old_id'] = $elem['sq_id'];
		$PROP['g_maps'] = trim($elem['sq_location_lat']).','.trim($elem['sq_location_long']);
		$PROP['address'] = $elem['address'];
		$PROP['articul'] = $elem['id'];
		$PROP['currency'] = $currency[$elem['currency']];
		$PROP['price'] = $elem['maxprice'];
		$PROP['all_s'] = $elem['maxarea'];
		$PROP['rooms'] = $elem['maxrooms'];
		$PROP['min_price'] = $elem['minprice'];
		$PROP['min_all_s'] = $elem['minarea'];
		$PROP['min_rooms'] = $elem['minrooms'];
		$PROP['main_type'] = 20001;
		$PROP['discount'] = $elem['discount'];
				
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => 17,
			"SORT" 	         => $i,
			"PROPERTY_VALUES"=> $PROP,
			"DETAIL_TEXT_TYPE" => "html",
	        "DETAIL_TEXT" => $elem['comment'],
			"NAME"           => trim($elem['name']),
			"ACTIVE"         => "Y",
		);
		
		if($PRODUCT_ID = $el->Add($arLoadProductArray))
			echo "New ID: ".$PRODUCT_ID."<br>";
		else
			echo "Error: ".$el->LAST_ERROR;
		
		*/
		
		//загородная недвижимость
		
		//новый елемент
	    $el = new CIBlockElement;
		
		$PROP = array();
		
		//дорога
		$road = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['road']))->GetNext();
		//направление
		$direction = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 7, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['direction']))->GetNext();
		//регион
		$region = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 9, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['region']))->GetNext();
		
		$PROP['old_id'] = $elem['sq_id'];
		$PROP['g_maps'] = trim($elem['sq_location_lat']).','.trim($elem['sq_location_long']);
		$PROP['address'] = $elem['address'];
		$PROP['articul'] = $elem['id'];
		$PROP['currency'] = $currency[$elem['currency']];
		$PROP['price'] = $elem['price'];
		$PROP['all_s'] = $elem['barea'];
		$PROP['rooms'] = $elem['maxrooms'];
		$PROP['s_area'] = $elem['parea'];
		$PROP['main_type'] = 19997;
		$PROP['discount'] = $elem['discount'];
		$PROP['direction'] = $direction['ID'];
		$PROP['distance'] = $elem['cdistance'];
		$PROP['road'] = $road['ID'];
		$PROP['region'] = $region['ID'];
				
		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => 20,
			"SORT" 	         => $i,
			"PROPERTY_VALUES"=> $PROP,
			"DETAIL_TEXT_TYPE" => "html",
	        "DETAIL_TEXT" => $elem['comment'],
			"NAME"           => trim($elem['name']),
			"ACTIVE"         => "Y",
		);
		
		if($PRODUCT_ID = $el->Add($arLoadProductArray))
			echo "New ID: ".$PRODUCT_ID."<br>";
		else
			echo "Error: ".$el->LAST_ERROR;

		
	//};
	
	echo '<hr/>';
}

/*
print '<pre>';
var_dump($institution);
print '</pre>';
*/


?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>