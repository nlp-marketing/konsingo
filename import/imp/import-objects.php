<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
//$institution = include(__DIR__.'/data-institution.php');
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;

foreach($institution as $elem){

	//ищем район
	if($elem['district']){
		$areas = NULL;
		$areas = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 10, "ACTIVE"=>"Y", "NAME" => $elem['district']))->GetNext();
		$areasId = $areas['ID'];
	}else{
		$areasId = '';
	}
		
	//ищем метро
	$m1 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro1']))->GetNext();
	$m2 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro2']))->GetNext();
	$m3 = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y", "PROPERTY_20"=> $elem['metro3']))->GetNext();
	
	$metro = array();
	
	$metro[] = $m1['ID'];
	
	if($m1['ID']!=$m2['ID']){
		$metro[] = $m2['ID'];
	}
	if($m2['ID']!=$m3['ID'] && $m1['ID']!=$m3['ID']){
		$metro[] = $m3['ID'];
	}

	$i+=10;
	
	//чистим телефон
	$phone = trim($elem['phone']);
	$phone = str_replace('<br/>', '', $phone); $phone = str_replace('<br>', '', $phone);
	$phone = preg_replace('/\s*&nbsp;*\s*/','',$phone);
	$phone = str_replace(',', ',<br/>', $phone);
	$phone = trim($phone);

	$ID = $block->Add( array( 
        "MODIFIED_BY" => $USER->GetID(),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => 13,
        "SORT" => $i,
        "NAME" => trim($elem['sq_title']),
        "PROPERTY_VALUES"=> array(
            'title'=>trim($elem['sq_title']),
            'type'=>($elem['type']+246),
            'district'=> array('VALUE'=> array('TEXT'=>trim($elem['areas']))),
            'areas'=>$areasId,
            'address'=>trim($elem['address']),
            'actual_address'=>trim($elem['address']),
            'legal_address'=>trim($elem['raddress']),
            'head'=>trim($elem['head']),
            'name_person'=>trim($elem['cpname']),
            'post_person'=>trim($elem['cpposition']),
            'phone'=>$phone,
            'details'=> array('VALUE'=> array('TEXT'=>trim($elem['bank_details']))),
            'site'=>trim($elem['site']),
            'g_maps'=>trim($elem['sq_location_lat']).','.trim($elem['sq_location_long']),
			'metro'=>$metro,
			'url'=>trim($elem['sq_unique_name']),
        ),
        "ACTIVE" => "Y"
    ));

    if(!$ID){
    	echo '<h1 style="font-size: 140%">Объект "',$elem['sq_title'],'" не удалось добавить!</h1>';
    }
}

/*
print '<pre>';
var_dump($institution);
print '</pre>';
*/


?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>