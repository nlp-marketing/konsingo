<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php") ?>

<?
$institution = include(__DIR__.'/data-abroad.php');
CModule::IncludeModule('iblock');
$block = new CIBlockElement();
$i = 0;

$type = CIBlockElement::GetList(
	Array("SORT" => "ASC"), 
	Array("IBLOCK_ID" => 27, "ACTIVE"=>"Y")
);
$types = array();
while($item = $type->GetNextElement()){
	$typeId = $item->GetFields();
	$typeProps = $item->GetProperties();
	$types[$typeProps['old_id']['VALUE']] = $typeId['ID'];
};

$country = CIBlockElement::GetList(
	Array("SORT" => "ASC"), 
	Array("IBLOCK_ID" => 19, "ACTIVE"=>"Y")
);
$countries = array();
while($item = $country->GetNextElement()){
	$countId = $item->GetFields();
	$countProps = $item->GetProperties();
	$countries[$countProps['old_id']['VALUE']] = $countId['ID'];
};

foreach($institution as $elem){ $i++;

	$objects = array();
	$objects = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 17, "ACTIVE"=>"Y", "PROPERTY_old_id" => $elem['sq_id'])
	);
	$items = array();
	while($item = $objects->GetNextElement()){
		$itemId = $item->GetFields();
		$itemProps = $item->GetProperties();
		$items[$itemId['ID']] = array($itemId,$itemProps);
	};
	
	CIBlockElement::SetPropertyValuesEx($itemId['ID'], 17, array(
		78 => $types[$elem['type']],
		79 => $countries[$elem['scountry']],
	));
	
	echo '<hr/>';
	
}

/*
print '<pre>';
var_dump($institution);
print '</pre>';
*/


?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>