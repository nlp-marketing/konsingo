<?
//подключаем инфоблоки
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/currency.php');
?>
<form name="feedback" class="feedback" id="feedback" action="#feedback" method="post">
	<div class="row clearfix">
		<label>Стоимость объекта:</label>
		<input class="s_area" type="text" name="Calculator[price]" value="<?echo $_POST['Calculator']['price'];?>" />
		<select name="Calculator[curr]">
			<? foreach($currencies as $key => $item){ ?>
				<option value="<?echo $key;?>"><?echo $item['sign'];?></option>
			<? }; ?>
		</select>
		<? if(isset($_POST['Calculator']) and empty($_POST['Calculator']['price'])){ ?>
			<div class="error"><nobr>Не указана сумма</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Первоначальный взнос:</label>
		<input class="s_area" type="text" name="Calculator[ante]" value="<?echo $_POST['Calculator']['ante'];?>" /> руб.
		<? if(isset($_POST['Calculator']) and empty($_POST['Calculator']['ante'])){ ?>
			<div class="error"><nobr>Не указана сумма</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Процентная ставка:</label>
		<input class="numeric" type="text" name="Calculator[rate]" value="<?echo $_POST['Calculator']['rate'];?>" /> %
		<? if(isset($_POST['Calculator']) and empty($_POST['Calculator']['rate'])){ ?>
			<div class="error"><nobr>Не указана ставка</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Период кредитования:</label>
		<input class="numeric" type="text" name="Calculator[crediting]" value="<?echo $_POST['Calculator']['crediting'];?>" /> мес.
		<? if(isset($_POST['Calculator']) and empty($_POST['Calculator']['crediting'])){ ?>
			<div class="error"><nobr>Не указан период</nobr></div>
		<? }; ?>
	</div>
	<div class="formSubmit">
		<input type="submit" value="Расчитать" />
	</div>
</form>