<?
//подключаем инфоблоки
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/materials.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/conditions.php');
?>
<form name="feedback" class="feedback" id="estimate_form" action="#feedback" method="post">
	<div class="row clearfix">
		<label>Адрес квартиры:</label>
		<textarea id="e_address" class="adress" name="Estimate[adress]"><?echo $_POST['Estimate']['adress'];?></textarea>
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['adress'])){ ?>
			<div class="error"><nobr>Не указан адрес</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label></label>
		<a class="ajax">Найти квартиру на карте</a>
	</div>
	<div class="row clearfix">
		<label>Тип дома:</label>
		<select name="Estimate[materials]">
			<option selected="1" disabled="1">Выберите</option>
			<? foreach($materials as $key => $item){ ?>
				<option value="<?echo $key;?>"><?echo $item;?></option>
			<? }; ?>
		</select>
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['materials'])){ ?>
			<div class="error"><nobr>Не выбран тип</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Состояние:</label>
		<select name="Estimate[condition]">
			<option selected="1" disabled="1">Выберите</option>
			<? foreach($conditions as $key => $item){ ?>
				<option value="<?echo $key;?>"><?echo $item;?></option>
			<? }; ?>
		</select>
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['condition'])){ ?>
			<div class="error"><nobr>Не выбрано состояние</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Общая площадь:</label>
		<input class="s_area" type="text" name="Estimate[all_s]" value="<?echo $_POST['Estimate']['all_s'];?>" /> м²
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['all_s'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Жилая площадь:</label>
		<input class="s_area" type="text" name="Estimate[live_s]" value="<?echo $_POST['Estimate']['live_s'];?>" /> м²
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['live_s'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Площадь кухни:</label>
		<input class="s_area" type="text" name="Estimate[kitchen_s]" value="<?echo $_POST['Estimate']['kitchen_s'];?>" /> м²
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['kitchen_s'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Количество комнат:</label>
		<input class="numeric" type="text" name="Estimate[rooms]" value="<?echo $_POST['Estimate']['rooms'];?>" />
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['rooms'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Этаж:</label>
		<input class="numeric" type="text" name="Estimate[floor]" value="<?echo $_POST['Estimate']['floor'];?>" />
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['floor'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="row clearfix">
		<label>Кол-во этажей в доме:</label>
		<input class="numeric" type="text" name="Estimate[floors]" value="<?echo $_POST['Estimate']['floors'];?>" />
		<? if(isset($_POST['Estimate']) and empty($_POST['Estimate']['floors'])){ ?>
			<div class="error"><nobr>Не указано</nobr></div>
		<? }; ?>
	</div>
	<div class="formSubmit">
		<input type="submit" value="Оценить квартиру" />
	</div>
</form>
<div id="estimate_map">
	<div id="estimate_map_map"></div>
</div>
<script type="text/javascript">



function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(55.749792,37.6324949),
		zoom: 10
	};
	var map = new google.maps.Map(document.getElementById("estimate_map_map"),
		mapOptions);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>