<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle("Консинго"); ?>
<? include($_SERVER['DOCUMENT_ROOT'].'/estate/var-array.php'); ?>
<?

//подключаем все инфоблоки
include($_SERVER['DOCUMENT_ROOT'].'/getlist/mainType.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/metro.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/types.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/country.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/currency.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/localy.php');
include($_SERVER['DOCUMENT_ROOT'].'/getlist/materials.php');

//подключаем класс обеъектов
	require_once $_SERVER['DOCUMENT_ROOT'].'/class/getlist.php';
//подключаем класс значений
	require_once $_SERVER['DOCUMENT_ROOT'].'/class/values.php';
	$values = new Values();
	
	//квартиры и комнаты
	$getList = new GetList(16,array("PROPERTY_index_page_VALUE" => "Да"),Array("RAND" => "ASC"));
	$in_apartments = $getList->objects();
	
	//загородная недвижимость
	$getList = new GetList(20,array("PROPERTY_index_page_VALUE" => "Да"),Array("RAND" => "ASC"));
	$in_countries = $getList->objects();

	//коммерческая недвижимость
	$getList = new GetList(30,array("PROPERTY_index_page_VALUE" => "Да"),Array("RAND" => "ASC"));
	$in_commercials = $getList->objects();
	
//элитные предложения:
	//квартиры
	$getList = new GetList(16,array("PROPERTY_index_elite_VALUE" => "Да"),Array("RAND" => "ASC"));
	$elite_apartments = $getList->objects();

	//загородные
	$getList = new GetList(20,array("PROPERTY_index_elite_VALUE" => "Да"),Array("RAND" => "ASC"));
	$elite_countries = $getList->objects();

	//заграницей
	$getList = new GetList(17,array("PROPERTY_index_elite_VALUE" => "Да"),Array("RAND" => "ASC"));
	$elite_abroads = $getList->objects();

	//коммерческие
	$getList = new GetList(30,array("PROPERTY_index_elite_VALUE" => "Да"),Array("RAND" => "ASC"));
	$elite_commercials = $getList->objects();

//все элитные в один массив	
	$elite = array();
	$elite = array_merge($elite_apartments,$elite_countries,$elite_abroads,$elite_commercials);
	
?>
<div id="area" class="withRightMenu">
	<div id="specOffers">
		<div class="tabs">
			<a id="tab1" class="active">Квартиры и комнаты</a><a id="tab2">За городом</a><a id="tab3">Коммерческая</a>
		</div>
		<div class="offers clearfix">
			<div class="offersTab clearfix active" id="o-tab1">
				<? 
					$x=0; foreach($in_apartments as $oneItem){ $x++;
					if(sizeof($in_apartments)==1){ 
						$colum = 2;
						$width = 143;
					}elseif(sizeof($in_apartments)>4){
						$colum = 4;
						$width = 72;
					}else{
						$colum = sizeof($in_apartments);
						$width = 287/$colum;
					};
				?>
				<div class="col c-<?echo $colum;?> clearfix">
					<div class="l">
						<? $x=0; foreach($oneItem[1]['photos']['VALUE'] as $pic){ $x++; if($x==1){ $photo = CFile::ResizeImageGet($pic, array('width'=>$width, 'height'=>135), BX_RESIZE_IMAGE_EXACT, true); }; }; ?>
						<a class="img" href="/estate/apartments/<?echo $oneItem[1]['old_id']['VALUE']?>/"><img style="border:3px solid #<?echo $main_types[$oneItem[1]['main_type']['VALUE']][1]['color']['VALUE']?>;" alt="<?echo $oneItem[0]['NAME']; ?>" src="<?echo $photo['src'];?>" /></a>
					</div>
					<div class="r">
						<div class="head">
							<a href="/estate/apartments/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
						</div>
						<div class="price"><?echo number_format($oneItem[1]['price']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$oneItem[1]['currency']['VALUE']]['sign']; ?></div>
						<? if($oneItem[1]['rooms']['VALUE']){ ?><div class="info"><?echo $oneItem[1]['rooms']['VALUE'];?> комн.</div><? }; ?>
						<? if($oneItem[1]['metro']['VALUE']){ ?><div class="info"><?foreach($oneItem[1]['metro']['VALUE'] as $st){ echo '<img src="'.CFile::GetPath($branchs[$stations[$st][0]][2]).'" height="10" /> '.$stations[$st][1].'<br/>'; }; ?></div><? }; ?>
						<div>
							<a class="ajax">Описание</a>
						</div>
					</div>
				</div>
				<? }; ?>
			</div>
			<div class="offersTab clearfix" id="o-tab2">
				<? 
					$x=0; foreach($in_countries as $oneItem){ $x++;
					if(sizeof($in_countries)==1){ 
						$colum = 2;
						$width = 143;
					}elseif(sizeof($in_countries)>4){
						$colum = 4;
						$width = 72;
					}else{
						$colum = sizeof($in_countries);
						$width = 287/$colum;
					};
				?>
				<div class="col c-<?echo $colum;?>">
					<div class="l">
						<? $x=0; foreach($oneItem[1]['photos']['VALUE'] as $pic){ $x++; if($x==1){ $photo = CFile::ResizeImageGet($pic, array('width'=>$width, 'height'=>135), BX_RESIZE_IMAGE_EXACT, true); }; }; ?>
						<a href="/estate/country/<?echo $oneItem[1]['old_id']['VALUE']?>/"><img style="border:3px solid #<?echo $main_types[$oneItem[1]['main_type']['VALUE']][1]['color']['VALUE']?>;" alt="<?echo $oneItem[0]['NAME']; ?>" src="<?echo $photo['src'];?>" /></a>
					</div>
					<div class="r">
						<div class="head">
							<a href="/estate/country/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
						</div>
						<div class="price"><?echo number_format($oneItem[1]['price']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$oneItem[1]['currency']['VALUE']]['sign']; ?></div>
						<? if($oneItem[1]['road']['VALUE']){ ?><div class="info"><?echo $roads[$oneItem[1]['road']['VALUE']];?></div><? }; ?>
						<? if($oneItem[1]['region']['VALUE']){ ?><div class="info"><?echo $oblasty[$oneItem[1]['region']['VALUE']];?></div><? }; ?>
						<div>
							<a class="ajax">Описание</a>
						</div>
					</div>
				</div>
				<? }; ?>
			</div>
			<div class="offersTab clearfix" id="o-tab3">
				<? 
					$x=0; foreach($in_commercials as $oneItem){ $x++;
					if(sizeof($in_commercial)==1){ 
						$colum = 2;
						$width = 143;
					}elseif(sizeof($in_commercial)>4){
						$colum = 4;
						$width = 72;
					}else{
						$colum = sizeof($in_commercial);
						$width = 287/$colum;
					};
				?>
				<div class="col c-<?echo $colum;?>">
					<div class="l">
						<? $x=0; foreach($oneItem[1]['photos']['VALUE'] as $pic){ $x++; if($x==1){ $photo = CFile::ResizeImageGet($pic, array('width'=>$width, 'height'=>135), BX_RESIZE_IMAGE_EXACT, true); }; }; ?>
						<a href="/estate/commercial/<?echo $oneItem[1]['old_id']['VALUE']?>/"><img style="border:3px solid #<?echo $main_types[$oneItem[1]['main_type']['VALUE']][1]['color']['VALUE']?>;" alt="<?echo $oneItem[0]['NAME']; ?>" src="<?echo $photo['src'];?>" /></a>
					</div>
					<div class="r">
						<div class="head">
							<a href="/estate/commercial/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
						</div>
						<? if($oneItem[1]['localy']['VALUE']){ ?><div class="info"><?echo $localyes[$oneItem[1]['localy']['VALUE']]; ?></div><? }; ?>
						<? if($oneItem[1]['road']['VALUE']){ ?><div class="info"><?echo $roads[$oneItem[1]['road']['VALUE']]; ?></div><? }; ?>
						<? if($oneItem[1]['direction']['VALUE']){ ?><div class="info"><?echo $directions[$oneItem[1]['direction']['VALUE']]; ?></div><? }; ?>
						<div>
							<a class="ajax">Описание</a>
						</div>
					</div>
				</div>
				<? }; ?>
			</div>
		</div>
	</div>
	<div id="filter" class="index clearfix">
		<span id="filterName">Фильтр</span>
		<form method="get" action="/estate/apartments/">
			<input class="hidden" type="text" value="1000000;1000" name="divid" />
			<input class="hidden" type="text" value="21112" name="rental" id="rental" />
			<input class="hidden" type="text" value="21105" name="region" id="region" />
			<div class="half-60">
				<div class="areas">
					<ul class="clearfix">
						<li class="active"><a onclick="setRental(21112,'sale')" class="ajax">Продажа</a></li>
						<li><a onclick="setRental(21111,'rent')" class="ajax">Аренда</a></li>
					</ul>
				</div>
				<div id="objectTypes" class="areas">
					<ul class="clearfix">
						<li class="active sale"><a onclick="setType('apartments')" class="ajax">Квартиры и комнаты</a></li>
						<li class="sale"><a onclick="setType('country')" class="ajax">Загородная недвижимость</a></li>
						<li class="rent hidden"><a onclick="setType('rent')" class="ajax">Аренда жилья</a></li>
						<li class="rent sale"><a onclick="setType('commercial')" class="ajax">Коммерческая недвижимость</a></li>
						<li class="sale"><a onclick="setType('abroad')" class="ajax">Недвижимость за границей</a></li>
					</ul>
				</div>
				<div id="regionTypes" class="areas">
					<ul class="clearfix">
						<li class="active apartments commercial"><a onclick="setRegion(21105)" class="ajax">Москва</a></li>
						<li class="apartments country commercial rent"><a onclick="setRegion(21106)" class="ajax">Подмосковье</a></li>
						<li class="country commercial hidden"><a onclick="setRegion(21107)" class="ajax">Регионы</a></li>
					</ul>
				</div>
				<div id="rooms">
					<span class="inputName">Количество комнат:</span>
					<ul class="clearfix">
						<li class="">
							<input type="radio" name="rooms" class="" id="rooms_1" value="1" />
							<label for="rooms_1">1</label>
						</li>
						<li class="">
							<input type="radio" name="rooms" class="" id="rooms_2" value="2" />
							<label for="rooms_2">2</label>
						</li>
						<li class="">
							<input type="radio" name="rooms" class="" id="rooms_3" value="3" />
							<label for="rooms_3">3</label>
						</li>
						<li class="">
							<input type="radio" name="rooms" class="" id="rooms_more" value="4" />
							<label for="rooms_more">больше</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="half-40">
				<div class="slider">
					<?
						$ch++;
						$title = array('Общая цена','Любая');
						$divide = 1000000;
						$slide_name = 'price';
						$slide_label = ' млн.';
						$slide_label_hide = ' млн.';
						$range = 1;
						$serif = 5;
						$class = 'oneFilter a-all';
						$currType = 1;
					?>
					<? 
						foreach($currencies as $key => $oneCurr){
							if($s['curr_'.$key.'_'.$slide_name]=='on'){
								$showId = $key;
							};
						};
						if(!$showId){ $showId = 20654; };
						
						if($s[$showId.'_'.$slide_name.'-min']!=$s['in-'.$showId.'_'.$slide_name.'-min'] or $s[$showId.'_'.$slide_name.'-max']!=$s['in-'.$showId.'_'.$slide_name.'-max']){
							$b = $block;
							$label = '<label>'.$s[$showId.'_'.$slide_name.'-min'].' — '.$s[$showId.'_'.$slide_name.'-max'].' '.$slide_label.'<a class="dltStation" onclick="clearSlider(\''.$slide_name.'\')">×</a></label>';
						}else{
							$b = '';
							$label = '<label>'.$title[1].'</label>';
						};
					?>
					<div class="<?echo $class;?>">
						<div class="oneChose" id="ch<?echo $ch;?>">Цена</div>
						<div class="oneChoseVal show" id="fch<?echo $ch;?>">
							<? foreach($currencies as $key => $oneCurr){ ?>
								<div class="currDiv cu<?echo $key;?><? if($key==$showId){ echo ' show'; }; ?>">
								<?

									$min = 0;									
									$max = 500;
									$s_min = $s[$key.'_'.$slide_name.'-min'];
									$s_max = $s[$key.'_'.$slide_name.'-max'];
									
									if($key!=20654){
										$curr_min = number_format($min/$oneCurr['curr'], 0, '.', ' ');
										$curr_max = number_format($max/$oneCurr['curr'], 0, '.', ' ');
										$fixed = 1;
									}else{
										$curr_min = $min;
										$curr_max = $max;
										$fixed = 0;
									};
									
								?>
								<div class="slider-kon"
									data-postname="<?echo $key.'_'.$slide_name;?>"
									data-currency="1"
									data-ajax="1"
									data-showid="<?echo $showId;?>"
									<?echo($s_min!=$min)?'data-optedmin="'.$s_min.'"':'';echo($s_max!=$max)?'data-optedmax="'.$s_max.'"':'';?>
									data-start="<?echo $curr_min;?>"
									data-finish="<?echo $curr_max;?>"
									data-serif="<?echo $serif;?>"
									data-range="<?echo $range;?>"
									data-label="<?echo $slide_label;?>"
									data-label-hide="<?echo $slide_label_hide;?>"
									data-fixed="<?echo $fixed;?>"></div>
								</div>
							<? }; ?>
						</div>
					</div>
					<? if($currType==1){$currType=0;}; ?>
				</div>
			</div>
			<div class="center"><a href="/estate/">Расширенный фильтр</a><input class="submit" type="submit" value="Найти"></div>
		</form>
	</div>
	<div id="main-text">
		<h1>Агентство недвижимости в Москве и Подмосковье</h1>
		<div class="border-padd">
			<p><strong>Агентство недвижимости Консинго</strong> — Консалтинговая Инжинирингово-Внедренческая Многопрофильная Управляющая Компания.</p><p>Наше агентство зарекомендовало себя как надежный партнер на рынке, как элитного жилья, так и жилья эконом-класса, а также по <em>продаже недвижимости в Подмосковье</em> (коттеджи, дома, участки, дачи).</p>
			<p>Отличительная черта нашего <em>агентства недвижимости</em> — индивидуальный подход к каждому клиенту, сопровождение сделки от начала и до конца одним специалистом, который постоянно информирует участников о ходе сделки, заботится об интересах, несет персональную ответственность за результат.</p><br>
			<p><strong>Квартиры в Подмосковье</strong></p>
			<div style="text-align:center;">
				<a href="#">Дубки</a>
				<a href="#">Железнодорожный</a>
				<a href="#">Лыткарино</a>
				<a href="#">Люберцы</a>
				<a href="#">Мытищи</a>
				<a href="#">Нахабино</a>
				<a href="#">Одинцово</a>
				<a href="#">Подольск</a>
				<a href="#">Пушкино</a>
			</div>
		</div>
	</div>
	<div class="banners clearfix">
		<div id="bn-1" class="banner">
			<div class="pic"><a href="/service/estimate/"><img src="/images/banners/estimate.jpg" alt="" /></a></div>
			<div class="head"><a href="/service/estimate/">Оценка стоимости квартиры</a></div>
		</div>
		<div id="bn-2" class="banner">
			<div class="pic"><a href="/service/mortgage/"><img src="/images/banners/mortgage.png" alt="" /></a></div>
			<div class="head"><a href="/service/mortgage/	">Кредитный калькулятор</a></div>
		</div>
		<div id="bn-3" class="banner">
			<div class="pic"><a href="/estate/"><img src="/images/banners/estate.png" alt="" /></a></div>
			<div class="head"><a href="/estate/">Наши объекты на карте</a></div>
		</div>
	</div>
	<div id="elitOffers">
		<div class="tabs">
			<span id="tab1">Элитные предложения</span>
		</div>
		<div class="offers clearfix">
			<div class="offersTab clearfix active" id="o-tab1">
				<?
					$x=0; foreach($elite as $oneItem){ $x++;
					
					if(sizeof($elite)==1){ 
						$colum = 2;
						$width = 143;
					}elseif(sizeof($elite)>4){
						$colum = 4;
						$width = 72;
					}else{
						$colum = sizeof($elite);
						$width = 287/$colum;
					};
				?>
				<div class="col c-<?echo $colum;?> clearfix">
					<div class="l">
						<? $x=0; foreach($oneItem[1]['photos']['VALUE'] as $pic){ $x++; if($x==1){ $photo = CFile::ResizeImageGet($pic, array('width'=>$width, 'height'=>135), BX_RESIZE_IMAGE_EXACT, true); }; }; ?>
						<a class="img" href="/estate/<?echo $main_types[$oneItem[1]['main_type']['VALUE']][0]['CODE']?>/<?echo $oneItem[1]['old_id']['VALUE']?>/"><img style="border:4px solid #<?echo $main_types[$oneItem[1]['main_type']['VALUE']][1]['color']['VALUE']?>;" alt="<?echo $oneItem[0]['NAME']; ?>" src="<?echo $photo['src'];?>" /></a>
					</div>
					<div class="r">
						<div class="head">
							<a href="/estate/<?echo $main_types[$oneItem[1]['main_type']['VALUE']][0]['CODE']?>/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
						</div>
						<div class="price red"><?echo number_format($oneItem[1]['price']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$oneItem[1]['currency']['VALUE']]['sign']; ?></div>
						<? if($oneItem[1]['rooms']['VALUE']){ ?><div class="info"><?echo $oneItem[1]['rooms']['VALUE'];?> комн.</div><? }; ?>
						<? if($oneItem[1]['metro']['VALUE']){ ?><div class="info"><?foreach($oneItem[1]['metro']['VALUE'] as $st){ echo '<img src="'.CFile::GetPath($branchs[$stations[$st][0]][2]).'" height="10" /> '.$stations[$st][1].'<br/>'; }; ?></div><? }; ?>
						<? if($oneItem[1]['road']['VALUE']){ ?><div class="info"><?echo $roads[$oneItem[1]['road']['VALUE']];?></div><? }; ?>
						<? if($oneItem[1]['region']['VALUE']){ ?><div class="info"><?echo $oblasty[$oneItem[1]['region']['VALUE']];?></div><? }; ?>
						<? if($oneItem[1]['country']['VALUE']){ ?><div class="info"><img alt="<?echo $countries[$oneItem[1]['country']['VALUE']][0];?>" height="10" src="<?echo CFile::GetPath($countries[$oneItem[1]['country']['VALUE']][1]);?>" /> <?echo $countries[$oneItem[1]['country']['VALUE']][0];?></div><? }; ?>
						<div>
							<a class="ajax">Описание</a>
						</div>
					</div>
				</div>
				<? }; ?>
			</div>
		</div>
	</div>
</div>
<div id="rightMenu">
	<div class="banners clearfix">
		<div id="bn-r-1" class="banner">
			<div class="pic"><a href="/estate/"><img src="/images/banners/estate.png" alt="" /></a></div>
			<div class="head"><a href="/estate/">Наши объекты на карте</a></div>
		</div>
		<div id="bn-r-2" class="banner">
			<div class="pic"><a href="/estate/abroad/"><img src="/images/banners/abroad.jpg" alt="" /></a></div>
			<div class="head"><a href="/estate/abroad/">Недвижимость за границей</a></div>
		</div>
	</div>
	<div class="grayBlock" style="width:204px;float:right;">
		<form action="" method="post" class="">
			<div><p>Поиск объекта по ID:</p></div>
			<div><input size="16" type="text" name="" id="" class=""><input class="m-l-10" type="submit" class="" value="Найти"></div>
		</form>
	</div>
	<div id="newsWidget">
		<div class="tabs"><a id="tab1" href="/news/" >Новости</a></div>
		<div class="grayBlock"> 			
			<?$APPLICATION->IncludeComponent(
			"bitrix:news",
			"mini",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"USE_SHARE" => "N",
				"SEF_MODE" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "news",
				"IBLOCK_ID" => "1",
				"NEWS_COUNT" => "3",
				"USE_SEARCH" => "N",
				"USE_RSS" => "Y",
				"USE_RATING" => "N",
				"USE_CATEGORIES" => "N",
				"USE_REVIEW" => "N",
				"USE_FILTER" => "N",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"CHECK_DATES" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(),
				"LIST_PROPERTY_CODE" => array(),
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"DISPLAY_NAME" => "Y",
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_FIELD_CODE" => array(),
				"DETAIL_PROPERTY_CODE" => array(),
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PAGER_TEMPLATE" => "arrows",
				"DETAIL_PAGER_SHOW_ALL" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"USE_PERMISSIONS" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_NOTES" => "",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PAGER_TEMPLATE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
				"PAGER_SHOW_ALL" => "N",
				"NUM_NEWS" => "20",
				"NUM_DAYS" => "30",
				"YANDEX" => "N",
				"SEF_FOLDER" => "/news/",
				"SEF_URL_TEMPLATES" => Array(
					"detail" => "#ELEMENT_ID#/",
					"rss" => "rss/",
					"rss_section" => "#SECTION_ID#/rss/"
				),
				"AJAX_OPTION_JUMP" => "Y",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "Y",
				"VARIABLE_ALIASES" => Array(
					"detail" => Array(),
					"rss" => Array(),
					"rss_section" => Array(),
				)
			)
		);?>
		</div>
   	</div>
</div>
<script>
	$('.slider-kon').smartSlider();
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>