<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
/*

$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"36000000"
	)
);
*/

?>

<div id="bg404">
<h1>Страница не найдена (Ошибка 404)</h1>
<div class="pageText">
	<p>К сожалению, запрашиваемой Вами страницы не существует на сайте.</p>
	<p>Возможно, это случилось по одной из этих причин:</p>
	<ul>
		<li>&ndash; Вы ошиблись при наборе адреса страницы (URL)</li>
		<li>&ndash; перешли по «битой» (неработающей, неправильной) ссылке</li>
		<li>&ndash; запрашиваемой страницы никогда не было на сайте или она была удалена</li>
	</ul>
	<br><br>
	<p>Мы просим прощение за доставленные неудобства и предлагаем следующие пути:</p>
	<ul>
		<li>&ndash; перейти на <a href="/">главную страницу сайта</a></li>
		<li>&ndash; проверить правильность написания адреса страницы (URL)</li>
		<li>&ndash; воспользоваться меню сайта</li>
		<li>&ndash; посетить основные разделы сайта</li>
	</ul>
	<br><br>
	<p>Если Вы уверены в правильности набранного адреса страницы и считаете, что эта ошибка произошла по нашей вине, пожалуйста, сообщите об этом администрации сайта.</p>
</div>
</div>

<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>