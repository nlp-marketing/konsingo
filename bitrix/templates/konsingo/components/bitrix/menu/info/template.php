<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult)) return;

$menu = array();
foreach($arResult as $index => $item)
{
    $class = isset($item['CLASS'])&&$item['CLASS'] ? $item['CLASS'] : '';

    $menu[] = array(
        'text'=>$item['TEXT'],
        'link'=>$item['LINK'],
        'class'=>$item['SELECTED'] ? $class.' active': $class,
    );
}
?>
<div class="info-menu">
    <div>
        <ul class="clearfix">
            <? foreach($menu as $item){ ?>
                <li <? if($item['class']!=='') echo "class=\"{$item['class']}\"" ?>>
                    <a href="<?= $item['link'] ?>"><?= $item['text'] ?></a>
                </li>
            <? } ?>
        </ul>
    </div>
</div>