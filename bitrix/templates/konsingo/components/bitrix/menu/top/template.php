<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult)) return;

/* var_dump($arResult); */

$menu = array();
foreach($arResult as $index => $item)
{
    if(isset($item['PARAMS'])&&isset($item['PARAMS']['position']))
        $position = $item['PARAMS']['position'];
    else
        $position = 'bottom';
    $class = isset($item['CLASS'])&&$item['CLASS'] ? $item['CLASS'] : '';
	
    $menu[$position][] = array(
        'text'=>$item['TEXT'],
        'link'=>$item['LINK'],
        'header'=>$item['PARAMS']['header'],
        'hidden'=>$item['PARAMS']['hidden'],
        'level'=>$item['DEPTH_LEVEL'],
        'class'=>$item['SELECTED'] ? $class.' active': $class,
    );
}
/* var_dump($menu); */
?>

<div class="top-menu">
    <div class="top">
        <ul class="clearfix">
        <? foreach($menu['top'] as $item){ ?>
            <li <? if($item['class']!=='') echo 'class="'.trim($item['class']).'"' ?>>
                <a href="<?= trim($item['link']) ?>"><?= trim($item['text']) ?></a>
            </li>
        <? } ?>
        </ul>
    </div>
    <div class="bottom">
        <ul class="clearfix">
            <? $first = true;$endChild = false; ?>
            <? foreach($menu['bottom'] as $item) { ?>
                <? if($item['level']==1) { ?>
                    <? if($endChild) echo '</ul>'; $endChild = false; ?>
                    <? if(!$first){ echo '</li>';} ?>
                   <li <?($item['hidden']==1)?'class="hidden"':'';?> <? if($item['class']!=='') echo "class=\"{$item['class']}\"" ?>>
                       <a href="<?= $item['link'] ?>"><?= $item['text'] ?></a>
                <? }elseif($item['header']==1){ ?>
                   	<li class="header"><span><?= $item['text'] ?></span></li>
                <? }else{ ?>
                   <? if(!$endChild) echo '<ul class="submenu">'; $endChild = true; ?>
                   	
                    <li <?if($item['hidden']==1){echo'class="hidden"';};?> <? if($item['class']!=='') echo "class=\"{$item['class']}\"" ?>>
                        <a href="<?= $item['link'] ?>"><?= $item['text'] ?></a>
                    </li>
                <? } ?>
            <? } ?>
            <? if($endChild) echo '</ul>'; ?>
            </li>
        </ul>
    </div>
</div>