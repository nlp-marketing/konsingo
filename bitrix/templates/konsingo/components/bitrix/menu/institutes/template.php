<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult)) return;

$menu = array();
foreach($arResult as $index => $item)
{
    $class = isset($item['CLASS'])&&$item['CLASS'] ? $item['CLASS'] : '';

    $menu[] = array(
        'text'=>$item['TEXT'],
        'link'=>$item['LINK'],
        'class'=>$item['SELECTED'] ? $class.' active': $class,
    );
}
?>
<div class="menuHolder">
<p id="openSubmenu">
	<b>
		<a href="#">Гос. учреждения</a>
		<img class="c0" alt="" src="/images/obm2.png">
	</b>
</p>
<div class="institutes-menu">
    <div id="obsmenu">			  
        <ul class="clearfix">	  
        	<p id="openedSubmenu">
				<a href="#">Гос. учреждения</a>
			</p>
            <? foreach($menu as $item){ ?>
                <li <? if($item['class']!=='') echo "class=\"{$item['class']}\"" ?>>
                    <a href="<?= $item['link'] ?>"><?= $item['text'] ?></a>
                </li>
            <? } ?>
        </ul>
    </div>
</div>
</div>