<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? IncludeTemplateLangFile(__FILE__); ?>

<!DOCTYPE html>
<html>
<head>
    <?$APPLICATION->ShowHead();?>

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="/js/slider.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="/js/sitefunc.js"></script>
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCiIABtOG_JD1rw0mSpx3XaboN9u5F2zYM&sensor=true"></script>
	<script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
	<script type="text/javascript" src="/js/highslide/highslide-with-gallery.js"></script>
	<script type="text/javascript" src="/js/highslide/highslide.config.js"></script>
	<script type="text/javascript" src="/js/preloader.js"></script>
	<script type="text/javascript" src="/js/grid.js"></script>
	<script type="text/javascript" src="/js/explode.js"></script>
	<script type="text/javascript" src="/js/highlight.js"></script>
	<script type="text/javascript" src="/js/map-master.js"></script>
	<link rel="stylesheet" type="text/css" href="/js/highslide/highslide.css" />
    <link href="<?= SITE_TEMPLATE_PATH ?>/styles.css"type="text/css" rel="stylesheet">
    <title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<?
$ho=1;
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;
?>
<style>
	.tablebodytext {
		display: none;
	}
</style>
<div id="loaderImage"></div>
<?$APPLICATION->ShowPanel();?>
<div id="page-wrapper">

    <header>
    	<div class="bodier">
	        <div id="logo">
	            <a href="<?=SITE_DIR?>" title="<?=GetMessage('CFT_MAIN')?>">
	                <?
	                $APPLICATION->IncludeFile(
	                    SITE_DIR."include/company_name.php",
	                    Array(),
	                    Array("MODE"=>"html")
	                );
	                ?>
	            </a>
	        </div>
	        <div class="phones">
	            <div class="info">
	                Наши телефоны:<br>
	                <span class="phone-tap">
	                    +7 (495) 777-26-20<br>
	                    +7 (495) 925-88-13
	                </span>
	            </div>
	            <div class="icon"></div>
	        </div>
	        <div id="main-menu">
	            <?
	            $APPLICATION->IncludeComponent("bitrix:menu", "top", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "2",
	"CHILD_MENU_TYPE" => "submenu",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "Y"
	),
	false
);
	            ?>
	        </div>
    	</div>
    </header>
    <div id="page" class="bodier clearfix">