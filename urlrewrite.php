<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/information/(opinion)|(questions)|(glossary)|(law)/\$#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/information/(opinion)|(questions)|(glossary)|(law)/index.php",
	),
	array(
		"CONDITION" => "#^/information/institutions/([a-z0-9]+)/\$#",
		"RULE" => "url=\$1",
		"ID" => "",
		"PATH" => "/information/position.php",
	),
	array(
		"CONDITION" => "#^/company/vacancies/([a-zA-Z0-9_]+)/\$#",
		"RULE" => "news=\$1",
		"ID" => "",
		"PATH" => "/company/vacancies/detail.php",
	),
	array(
		"CONDITION" => "#^/company/partners/([a-zA-Z0-9_]+)/\$#",
		"RULE" => "news=\$1",
		"ID" => "",
		"PATH" => "/company/partners/detail.php",
	),
	array(
		"CONDITION" => "#^/estate/([a-z_\\-]+)/([a-z0-9]+)/\$#",
		"RULE" => "SECTION_CODE=\$1&ELEMENT_ID=\$2",
		"ID" => "",
		"PATH" => "/estate/position.php",
	),
	array(
		"CONDITION" => "#^/news/([a-zA-Z0-9_]+)/(\\?.*)?\$#",
		"RULE" => "news=\$1",
		"ID" => "",
		"PATH" => "/news/detail.php",
	),
	array(
		"CONDITION" => "#^/information/([a-z\\-]+)/\$#",
		"RULE" => "url=\$1",
		"ID" => "",
		"PATH" => "/information/section.php",
	),
	array(
		"CONDITION" => "#^/estate/([a-z_\\-]+)/?.+\$#",
		"RULE" => "url=\$1",
		"ID" => "",
		"PATH" => "/estate/section.php",
	),
	array(
		"CONDITION" => "#^/information/\$#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/information/index.php",
	),
	array(
		"CONDITION" => "#^/services/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/services/index.php",
	),
	array(
		"CONDITION" => "#^/estate/\$#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/estate/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>