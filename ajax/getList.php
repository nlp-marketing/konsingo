<?
	$prop = array(); $msk = 0; $mkad = 0; $reg = 0;
	
	if($_GET['rental']){
		$_SESSION[$_GET['url']] = $_GET;
		$s = $_SESSION[$code_url];
	};
	
	//регион (Москва, подмосковье, регионы)
	if(isset($s['region']) and $s['region']!="all" and $s['region']!=""){
		$prop[] = array("PROPERTY_location" => $s['region']);
	};
	
	$divid = explode(';', $s['divid']);
	if($s['code_id']){
		$code = explode(';', $s['code_id']);
	}else{
		$code[0] = $code_url;
		$code[1] = $id;
	}
	
	//аренда/продажа
	if(isset($s['rental']) and $s['rental']!="all"){
		$prop[] = array("PROPERTY_rental" => $s['rental']);
	};
	
	if($code[0]=='apartments'){
		//тип (квартира/комната)
		if(isset($s['type']) and $s['type']!="all"){
			$prop[] = array("PROPERTY_type" => $s['type']);
		};
	}else{
		//страна
		if($s['countries'] and !in_array('all',$s['countries'])){
			$prop[] = array("PROPERTY_country" => $s['countries']);
		};
	
		//тип недвижимости за границей
		if($s['abroad_type'] and !in_array('all',$s['abroad_type'])){
			$prop[] = array("PROPERTY_type" => $s['abroad_type']);
		};
		
		//тип загородной недвижимости
		if($s['country_type'] and !in_array('all',$s['country_type'])){
			$prop[] = array("PROPERTY_type" => $s['country_type']);
		};
		
		//тип коммерческой недвижимости
		if($s['commercial_type'] and !in_array('all',$s['commercial_type'])){
			$prop[] = array("PROPERTY_type" => $s['commercial_type']);
		};
	}
	
	//включаем нужный регион поиска
	if($s['region']==21105){
		$msk = 1;
	}elseif($s['region']==21106){
		$mkad = 1;
	}elseif($s['region']==21107){
		$reg = 1;
	}else{
		$msk = 1; $mkad = 1; $reg = 1;
	}
	
	//районы Москвы
	if(isset($s['areas']) and $s['areas']!="all" and $msk==1){
		$prop[] = array("PROPERTY_areas" => $s['areas']);
	};
	
	//станции метро
	if($s['metro'] and ($msk==1 or $mkad==1)){
		$prop[] = array("PROPERTY_metro" => $s['metro']);
	};
	
	//расположение в Москве
	if(isset($s['location']) and $s['location']!="all" and $msk==1){
		if($s['location']==21110){
			$prop[] = array("PROPERTY_location_msk" => array(21108,21109,21110));
		}elseif($s['location']==21109){
			$prop[] = array("PROPERTY_location_msk" => array(21108,21109));
		}elseif($s['location']==21108){
			$prop[] = array("PROPERTY_location_msk" => 21108);
		};
	};
	
	//удаленность от метро
	if($s['distance']>0 and ($msk==1 or $mkad==1)){
		$prop[] = array("<PROPERTY_distance" => $s['distance']);
	};
	
	//удаленность от Москвы
	if($s['localy_distane']>0 and $mkad==1){
		$prop[] = array("<PROPERTY_localy_distane" => $s['localy_distane']);
	};
	
	//населенный пункт
	if($s['localitie']!="all" and (($code[0]=='apartments' and $mkad==1) or ($code[0]=='country' and $reg==1))){
		$prop[] = array("PROPERTY_localy" => $s['localitie']);
	};
	
	//дорога
	if(isset($s['road']) and $s['road']!="all" and $mkad==1){
		$prop[] = array("PROPERTY_road" => $s['road']);
	};
	
	//направление
	if(isset($s['direction']) and $s['direction']!="all" and $mkad==1){
		$prop[] = array("PROPERTY_direction" => $s['direction']);
	};
	
	//область
	if(isset($s['oblast']) and $s['oblast']!="all" and $reg==1){
		$prop[] = array("PROPERTY_region" => $s['oblast']);
	};
	
	//вся площадь
	if($s['all_s-min']!=$s['in-all_s-min'] or $s['all_s-max']!=$s['in-all_s-max']){
		$prop[] = array("><PROPERTY_all_s" => array($s['all_s-min'],$s['all_s-max']));
	};
	
	//живая площадь
	if($s['live_s-min']!=$s['in-live_s-min'] or $s['live_s-max']!=$s['in-live_s-max']){
		$prop[] = array("><PROPERTY_live_s" => array($s['live_s-min'],$s['live_s-max']));
	};
	
	//площадь кухни
	if($s['kitchen_s-min']!=$s['in-kitchen_s-min'] or $s['kitchen_s-max']!=$s['in-kitchen_s-max']){
		$prop[] = array("><PROPERTY_kitchen_s" => array($s['kitchen_s-min'],$s['kitchen_s-max']));
	};
	
	//кол-во комнат
	if(isset($s['rooms']) and !empty($s['rooms'])){
		if($s['rooms']==4){
			$prop[] = array(">PROPERTY_rooms" => 3);
		}else{
			$prop[] = array("PROPERTY_rooms" => $s['rooms']);
		};
	}elseif($s['rooms-min']!=$s['in-rooms-min'] or $s['rooms-max']!=$s['in-rooms-max']){
		$prop[] = array("><PROPERTY_rooms" => array($s['rooms-min'],$s['rooms-max']));
	};
	
	//этаж
	if($s['floor-min']!=$s['in-floor-min'] or $s['floor-max']!=$s['in-floor-max']){
		$prop[] = array("><PROPERTY_floor" => array($s['floor-min'],$s['floor-max']));
	};
	
	//всего этажей
	if($s['floors-min']!=$s['in-floors-min'] or $s['floors-max']!=$s['in-floors-max']){
		$prop[] = array("><PROPERTY_all_floor" => array($s['floors-min'],$s['floors-max']));
	};
	
	//курсы валют
	$currency = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 18, "ACTIVE"=>"Y")
	);
	$currencies = array();
	while($item = $currency->GetNextElement()){
		$currId = $item->GetFields();
		$currProps = $item->GetProperties();
		$currInt = str_replace(',','.',$currProps['curr']['VALUE']); $currInt = floatval($currInt);
		$currencies[$currId['ID']] = array('label'=>$currId['NAME'],'curr'=>$currInt,'sign'=>$currProps['sign']['VALUE']);
	};

	foreach($currencies as $key => $oneCurr){
		if($s['curr_'.$key.'_price']=='on'){
			$showId = $key;
		};
	};
	
	//подключаем класс конверции валют
	require_once $_SERVER['DOCUMENT_ROOT'].'/class/currency.php';
	
	//var_dump($s);
	
	//цена
	if($s[$showId.'_price-min']!=$s['in-'.$showId.'_price-min'] or $s[$showId.'_price-max']!=$s['in-'.$showId.'_price-max']){
		
		$min_price = $s[$showId.'_price-min']*$divid[0];
		$max_price = $s[$showId.'_price-max']*$divid[0];
		
		$convert_min = new Currency($min_price);
		$convert_max = new Currency($max_price);
	
		if($showId==20654){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20652, "><PROPERTY_price" => array($convert_min->rub2usd(),$convert_max->rub2usd())),
				array("PROPERTY_currency" => 20653, "><PROPERTY_price" => array($convert_min->rub2eur(),$convert_max->rub2eur())),
			);
		}elseif($showId==20652){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20653, "><PROPERTY_price" => array($convert_min->usd2eur(),$convert_max->usd2eur())),
				array("PROPERTY_currency" => 20654, "><PROPERTY_price" => array($convert_min->usd2rub(),$convert_max->usd2rub())),
			);
		}elseif($showId==20653){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20652, "><PROPERTY_price" => array($convert_min->eur2usd(),$convert_max->eur2usd())),
				array("PROPERTY_currency" => 20654, "><PROPERTY_price" => array($convert_min->eur2rub(),$convert_max->eur2rub())),
			);
		};
	};
	
	//цена за метр
	if($s[$showId.'_mprice-min']!=$s['in-'.$showId.'_mprice-min'] or $s[$showId.'_mprice-max']!=$s['in-'.$showId.'_mprice-max']){
		
		$min_price = $s[$showId.'_mprice-min']*$divid[1];
		$max_price = $s[$showId.'_mprice-max']*$divid[1];
		
		$convert_min = new Currency($min_price);
		$convert_max = new Currency($max_price);
	
		if($showId==20654){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20652, "><PROPERTY_price" => array($convert_min->rub2usd(),$convert_max->rub2usd())),
				array("PROPERTY_currency" => 20653, "><PROPERTY_price" => array($convert_min->rub2eur(),$convert_max->rub2eur())),
			);
		}elseif($showId==20652){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20653, "><PROPERTY_price" => array($convert_min->usd2eur(),$convert_max->usd2eur())),
				array("PROPERTY_currency" => 20654, "><PROPERTY_price" => array($convert_min->usd2rub(),$convert_max->usd2rub())),
			);
		}elseif($showId==20653){
			$prop[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_currency" => $showId, "><PROPERTY_price" => array($min_price,$max_price)),
				array("PROPERTY_currency" => 20652, "><PROPERTY_price" => array($convert_min->eur2usd(),$convert_max->eur2usd())),
				array("PROPERTY_currency" => 20654, "><PROPERTY_price" => array($convert_min->eur2rub(),$convert_max->eur2rub())),
			);
		};
	};
	
	$sorti = include($_SERVER['DOCUMENT_ROOT'].'/ajax/sorti.php');
	
	//var_dump($sorti);
	//var_dump($prop);
	
	if($code[1]!=NULL){
		return CIBlockElement::GetList(
			$sorti,
			Array(
				"IBLOCK_ID" => $code[1],
				"ACTIVE"=>"Y",
				$prop
			)
		);
	}else{
		return NULL;
	};
?>