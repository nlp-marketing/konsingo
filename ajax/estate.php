<?
header('Content-Type: text/html; charset=windows-1251');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');
define("STOP_STATISTICS", true);
ob_start();
//var_dump($_POST);

//собираем все объекты нужного раздела
	
	$code = explode(';', $_POST['code_id']);
	
//code_url
	$code_url = $code[0];
	
	//создаем сессию
	$_SESSION[$code[0]] = $_POST; $s = $_SESSION[$code[0]];
	
	//var_dump($s);
	
	$objects = include($_SERVER['DOCUMENT_ROOT'].'/ajax/getList.php');
	
	if($objects){
	
		while($item = $objects->GetNextElement()){
			
			//собираем свой массив с позициями
			$itemId = $item->GetFields();
			$itemProps = $item->GetProperties();
			$items[$itemId['ID']] = array($itemId,$itemProps);
			if($itemProps['g_maps']['VALUE']){
				$itemsMaps[$itemId['ID']] = array($itemId,$itemProps);
			};
		};
	
	//собираем для карты
	$forMap = array();

	foreach($itemsMaps as $key => $oneItem){
			$coords = explode(',', $oneItem[1]['g_maps']['VALUE']);
			$img = '';
			foreach($oneItem[1]['photos']['VALUE'] as $pic){
				$miniPic = CFile::ResizeImageGet($pic, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_EXACT, true);
				$img = '<img class="small" src="'.$miniPic['src'].'" />';
				break;
			}
			$forMap[] = array(
				'id'=>$key,
				'marker'=>array(
					'icon'=>'/images/estate/'.$code_url.'/icon.png',
					'lat'=>(float)$coords[0], // Широта
					'long'=>(float)$coords[1], // Долгота
	
				),
				'info'=>'<div class="g_maps_window">'.
						'<a class="pic" href="//'.$_SERVER['HTTP_HOST'].'/estate/'.$code_url.'/'.$oneItem[1]['old_id']['VALUE'].'/">'.
						$img.
						'</a>'.
						'<div><strong><a href="//'.$_SERVER['HTTP_HOST'].'/estate/'.$code_url.'/'.$oneItem[1]['old_id']['VALUE'].'/">'.
						$oneItem[0]['NAME'].
						'</a></strong></div>'.
						'<div><strong>Общая площадь:</strong>'.$oneItem[1]['all_s']['VALUE'].' м²</div>'.
						'<div><strong>Жилая площадь:</strong>'.$oneItem[1]['live_s']['VALUE'].' м²</div>'.
						'<div><strong>Площадь кухни:</strong>'.$oneItem[1]['kitchen_s']['VALUE'].' м²</div>'.
						'<div><strong>Общая цена:</strong> '.$oneItem[1]['price']['VALUE'].' р.</div>'.
						'<div><strong>Цена за метр:</strong> '.$oneItem[1]['mprice']['VALUE'].' р.</div>'.
					'</div>'
			);
			
	}
	
	//подключаем все инфоблоки
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/mainType.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/metro.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/types.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/country.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/currency.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/localy.php');
		include($_SERVER['DOCUMENT_ROOT'].'/getlist/materials.php');
		
	//вывод таблицы	
		include($_SERVER['DOCUMENT_ROOT'].'/estate/table-first.php');
		foreach($items as $key => $oneItem){
			include($_SERVER['DOCUMENT_ROOT'].'/estate/table-row.php');
		};
		$itemsFilter = $items;
		include($_SERVER['DOCUMENT_ROOT'].'/estate/table-off.php');
	
	}else{
		
	};
$content = ob_get_contents();
ob_clean();
echo json_encode(array('html'=>$content,'forMap'=>$forMap));
?>