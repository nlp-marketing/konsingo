<?
header('Content-Type: text/html; charset=windows-1251');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');
define("STOP_STATISTICS", true);

//собираем линии
	$lines = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 6, "ACTIVE"=>"Y")); $branchs = array();
	while($branch = $lines->GetNextElement()){	
		//собираем свой массив со станциями
		$branchId = $branch->GetFields();
		$branchProps = $branch->GetProperties();
		$branchs[$branchId['ID']] = array($branchId['ID'],$branchId['NAME'],$branchProps['pic']['VALUE']);
	};

//собираем все станции метро
	$metro = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y")); $stations = array();
	while($station = $metro->GetNextElement()){	
		//собираем свой массив со станциями
		$stationId = $station->GetFields();
		$stationProps = $station->GetProperties();
		$stations[$stationId['ID']] = array($stationProps['branch']['VALUE'],$stationId['NAME']);
	};
?>

<div id="findMetroId">
	<ul>
		<? foreach($_POST['metro'] as $st){ ?>
			<li id="<?echo $st;?>"><input type="text" value="<?echo $st;?>" style="display:none;" name="metro[]" /><img src="<?echo CFile::GetPath($branchs[$stations[$st][0]][2]);?>" height="10" /><?echo $stations[$st][1];?><a class="dltStation" onclick="unsetStation(<?echo $st;?>)">×</a></li>
		<? }; ?>
	</ul>
</div>