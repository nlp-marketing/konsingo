<?
header('Content-Type: text/html; charset=windows-1251');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');
define("STOP_STATISTICS", true);

//выбираем нужный раздел
$section = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 12, "ACTIVE"=>"Y", "PROPERTY_38" => $_POST['section']))->GetNext();

//работаем с учереждениями
$iblog = array("IBLOCK_ID" => 13, "ACTIVE"=>"Y", "PROPERTY_24" => $section['ID']);

$area = array();
$metro = array();
$street = array();

/*

if($_POST['type']=='area'){

	//проверям какой район выбран
	if($_POST['area']!='all'){
		$area = array("PROPERTY_25" => $_POST['area']);
	};

	//собираем объекты нужного метро
	if(!empty($_POST['stations'])){
		$marray = array();
		$marray = explode(',', $_POST['stations']);
		$metro = $marray;
		
		$metro = array("PROPERTY_37" => $metro);
		
	};
	
}elseif($_POST['type']=='metro'){

	//проверям какой район выбран
	if($_POST['area']!='all'){
		$area = array("PROPERTY_25" => $_POST['area']);
	};

	//собираем объекты нужного метро
	if(!empty($_POST['stations'])){
		$marray = array();
		$marray = explode(',', $_POST['stations']);
		if(!empty($_POST['metro'])){
			$marray[] = $_POST['metro'];
		};
		$metro = $marray;
	}else{
		$metro = $_POST['metro'];
	};
	
	$metro = array("PROPERTY_37" => $metro);
	
}elseif($_POST['type']=='delmetro'){

	//проверям какой район выбран
	if($_POST['area']!='all'){
		$area = array("PROPERTY_25" => $_POST['area']);
	};
	
	//собираем объекты нужного метро
	if(!empty($_POST['stations'])){
		$marray = array();
		$marray = explode(',', $_POST['stations']);
		$marray[] = $_POST['metro'];
		$metro = $marray;
		
		$metro = array("PROPERTY_37" => $metro);
		
	};
	
}elseif($_POST['type']=='street'){
	
	//проверям какой район выбран
	if($_POST['area']!='all'){
		$area = array("PROPERTY_25" => $_POST['area']);
	};
	
	//собираем объекты нужного метро
	if(!empty($_POST['stations'])){
		$marray = array();
		$marray = explode(',', $_POST['stations']);
		$marray[] = $_POST['metro'];
		$metro = $marray;
		
		$metro = array("PROPERTY_37" => $metro);
		
	};
	
	//собираем объекты на нужной улице
	if(!empty($_POST['street'])){
		$street = array("PROPERTY_41" => $_POST['street']);
	}
	
}
*/

//проверям какой район выбран
if(!empty($_POST['area']) and $_POST['area']!='all'){
	$area = array("PROPERTY_25" => $_POST['area']);
};

//собираем объекты нужного метро
if(!empty($_POST['stations'])){
	$marray = array();
	$marray = explode(',', $_POST['stations']);
	if(!empty($_POST['metro'])){
		$marray[] = $_POST['metro'];
	};
	$metro = $marray;
	$metro = array("PROPERTY_37" => $metro);
}elseif(!empty($_POST['metro'])){
	$metro = $_POST['metro'];
	$metro = array("PROPERTY_37" => $metro);
};

//собираем объекты на нужной улице/улицах
if(!empty($_POST['streets'])){
	$sarray = array();
	$sarray = explode(',', $_POST['streets']);
	if(!empty($_POST['street'])){
		$sarray[] = $_POST['street'];
	};
	$street = $sarray;
	$street = array("PROPERTY_41" => $street);
}elseif(!empty($_POST['street'])){
	$street = $_POST['street'];
	$street = array("PROPERTY_41" => $street);
};

//собираем все фильтры
$arFilter = array_merge((array)$iblog, (array)$area, (array)$metro, (array)$street);

/*
echo '<hr/>';
var_dump($iblog);
echo '<hr/>';
var_dump($area);
echo '<hr/>';
var_dump($metro);
echo '<hr/>';
var_dump($street);
echo '<hr/>	';
var_dump($arFilter);
*/

//собираем объекты с выбранными фильтрами
$objects = CIBlockElement::GetList(Array("NAME" => "ASC"), Array($arFilter));

//массивы для слолбцов
$items = array(); $areas = array(); $district = array(); $name = array(); $address = array(); $head = array(); $phone = array(); $site = array(); $name_person = array(); $post_person = array(); $actual_a = array(); $legal_a = array(); $details = array();

//проверяем есть ли контент
if(count($objects)>0){ while($item = $objects->GetNextElement()){

	//собираем свой массив с позициями
	$itemId = $item->GetFields();
	$itemProps = $item->GetProperties();
	$items[$itemId['ID']] = $itemProps;
	//проверяем есть ли координаты Google Maps
	if($itemProps['g_maps']['VALUE']){
		$itemsMaps[$itemId['ID']] = $itemId['ID'];
	};
	
}; };

if(count($items)>0){

$x=0; foreach($items as $key => $oneItem){ $x++;
	//смотрим какие столбцы выводить
	$areas[] = $oneItem['areas']['VALUE'];
	$district[] = $oneItem['district']['VALUE'];
	$name[] = $oneItem['title']['VALUE'];
	$address[] = $oneItem['address']['VALUE'];
	$head[] = $oneItem['head']['VALUE'];
	$phone[] = $oneItem['phone']['VALUE'];
	$site[] = $oneItem['site']['VALUE'];
	$name_person[] = $oneItem['name_person']['VALUE'];
	$post_person[] = $oneItem['post_person']['VALUE'];
	$actual_a[] = $oneItem['actual_address']['VALUE'];
	$legal_a[] = $oneItem['legal_address']['VALUE'];
	$details[] = $oneItem['details']['VALUE'];
};

//чистим массивы от пустышек
$areas = array_diff($areas, array(''));
$district = array_diff($district, array(''));
$site = array_diff($site, array(''));
$head = array_diff($head, array(''));
$address = array_diff($address, array(''));
$actual_a = array_diff($actual_a, array(''));
$legal_a = array_diff($legal_a, array(''));
$phone = array_diff($phone, array(''));
$name_person = array_diff($name_person, array(''));
$post_person = array_diff($post_person, array(''));
$details = array_diff($details, array(''));

?>
<tr class="first">
	<? if(!empty($areas)){ ?><td>Округ</td><? }; ?>
	<? if(!empty($district)){ ?><td>Районы</td><? }; ?>
	<td>Название организации</td>
	<? if(empty($legal_a) and empty($address) and empty($actual_a)){}elseif(empty($legal_a)){ ?><td>Адрес</td><? }else{ ?><td>Фактический адрес</td><td>Юридический адрес</td><? }; ?>
	<? if(!empty($head)){ ?><td>Руководитель</td><? }; ?>
	<? if(!empty($post_person)){ ?><td>Должность контактного лица</td><? }; ?>
	<? if(!empty($name_person)){ ?><td>Имя контактного лица</td><? }; ?>
	<? if(!empty($phone)){ ?><td>Телефон</td><? }; ?>
	<? if(!empty($site)){ ?><td>Сайт</td><? }; ?>
	<? if(!empty($details)){ ?><td>Реквизиты</td><? }; ?>
</tr>
<? // выводим таблицу
foreach($items as $key => $oneItem){
	$area = CIBlockElement::GetByID($oneItem['areas']['VALUE'])->GetNext(); ?>
	<tr>
		<? if(!empty($areas)){ ?><td><?echo $area['NAME']; ?></td><? }; ?>
		<? if(!empty($district)){ ?><td><? $dis = str_replace("\r\n",'', $oneItem['district']['VALUE']['TEXT']); $dis = str_replace("\n",'',$dis); echo $dis; ?></td><? }; ?>
		<td><a href="<?echo 'http://'.$_SERVER['HTTP_HOST'].'/information/institutions/'.$oneItem['url']['VALUE']; ?>"><?echo $oneItem['title']['VALUE'];?></a></td>
		<? if(empty($legal_a) and empty($address) and empty($actual_a)){}elseif(empty($legal_a)){ ?><td><?echo $oneItem['address']['VALUE'];?></td><? }else{ ?><td><?echo $oneItem['actual_address']['VALUE'];?></td><td><?echo $oneItem['legal_address']['VALUE'];?></td><? }; ?>
		<? if(!empty($head)){ ?><td><?echo $oneItem['head']['VALUE'];?></td><? }; ?>
		<? if(!empty($post_person)){ ?><td><?echo $oneItem['post_person']['VALUE'];?></td><? }; ?>
		<? if(!empty($name_person)){ ?><td><?echo $oneItem['name_person']['VALUE'];?></td><? }; ?>
		<? if(!empty($phone)){ ?><td><? $phone = htmlspecialchars_decode($oneItem['phone']['VALUE']); echo '<nobr>'.$phone.'</nobr>'; ?></td><? }; ?>
		<? if(!empty($site)){ ?><td><noindex><a href="<?echo $oneItem['site']['VALUE'];?>" rel="nofollow" target="_blank"><?echo $oneItem['site']['VALUE'];?></a></noindex></td><? }; ?>
		<? if(!empty($details)){ ?><td><? $ditail = str_replace("\r\n",'', $oneItem['details']['VALUE']['TEXT']); $ditail = str_replace("\n",'',$ditail); echo htmlspecialchars_decode($ditail); ?></td><? }; ?>
	</tr>
<? }; ?>
<? }else{ ?>
<span>Не найдено ни одного учреждения.</span>
<? }; ?>