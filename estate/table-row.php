<tr class="oneRow">
	<td valign="top">
		<div class="prew-pic">
			<? $x=0; foreach($oneItem[1]['photos']['VALUE'] as $pic){ $x++; if($x==1){ 
				$bigPic = CFile::ResizeImageGet($pic, array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_EXACT, true); echo '<a class="big" href="/estate/'.$code_url.'/'.$oneItem[1]['old_id']['VALUE'].'/"><img src="'.$bigPic['src'].'" /></a>'; 
				$miniPic = CFile::ResizeImageGet($pic, array('width'=>48, 'height'=>38), BX_RESIZE_IMAGE_EXACT, true); echo '<img class="small" src="'.$miniPic['src'].'" />'; }; }; ?>
		</div>
	</td>
	<? if($code_url=='apartments' or $code_url=='commercial'){ ?>
		<td>
			<a href="/estate/<?echo $code_url;?>/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
			<span style="display:block;"><? if($oneItem[1]['rooms']['VALUE']!=0){ echo $oneItem[1]['rooms']['VALUE'].' комн.'; }; ?></span>
			<span style="display:block;"><? if($oneItem[1]['floor']['VALUE']>0){ echo $oneItem[1]['floor']['VALUE']; ?>-й этаж<? if($oneItem[1]['all_floor']['VALUE']>0){ echo ' из '.$oneItem[1]['all_floor']['VALUE']; }; }; ?></span>
		</td>
		<td><?foreach($oneItem[1]['metro']['VALUE'] as $st){ echo '<img src="'.CFile::GetPath($branchs[$stations[$st][0]][2]).'" height="10" /> <nobr>'.$stations[$st][1].'</nobr><br/>'; }; ?></td>
	<? }elseif($code_url=='country' or $code_url=='rent'){ ?>
		<td><a href="/estate/<?echo $code_url;?>/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a></td>
	<? }elseif($code_url=='abroad' or $code_url=='commercial'){ ?>
		<td>
			<a href="/estate/<?echo $code_url;?>/<?echo $oneItem[1]['old_id']['VALUE']?>/"><?echo $oneItem[0]['NAME']; ?></a>
			<span style="display:block;"><? if($oneItem[1]['rooms']['VALUE']!=0){ echo $oneItem[1]['rooms']['VALUE'].' комн.'; }; ?></span>
		</td>
	<? }; ?>
	<? if($code_url!='abroad'){ ?>
		<td><? if($oneItem[1]['localy']['VALUE'] or $oneItem[1]['localy_distane']['VALUE']!=0){ echo $localyes[$oneItem[1]['localy']['VALUE']]; if($oneItem[1]['localy']['VALUE']){ echo '<br/>'; }; echo $oneItem[1]['localy_distane']['VALUE'].' км'; }else{ ?>—<? }; ?></td>
	<? }; ?>
	<? if($code_url=='apartments'){ ?>
		<td><nobr><?echo $oneItem[1]['all_s']['VALUE']; ?> м²</nobr> / <nobr><?echo $oneItem[1]['live_s']['VALUE']; ?> м²</nobr> / <nobr><?echo $oneItem[1]['kitchen_s']['VALUE']; ?> м²</nobr></td>
	<? }elseif($code_url=='country'){ ?>
		<td><nobr><?echo $oneItem[1]['all_s']['VALUE']; ?> м²</nobr> / <nobr><?echo $oneItem[1]['area_s']['VALUE']; ?> сот.</nobr></td>
	<? }elseif($code_url=='abroad' or $code_url=='commercial' or $code_url=='rent'){ ?>
		<td><nobr><?if($oneItem[1]['min_all_s']['VALUE'] or $oneItem[1]['all_s']['VALUE']){ echo($oneItem[1]['min_all_s']['VALUE'] and $oneItem[1]['min_all_s']['VALUE']!=$oneItem[1]['all_s']['VALUE'])?$oneItem[1]['min_all_s']['VALUE'].' — ':'';echo($oneItem[1]['all_s']['VALUE'])?$oneItem[1]['all_s']['VALUE']:''; ?> м²<? }else{ echo '—'; }; ?></nobr></td>
	<? }; ?>
	<? if($code_url=='commercial' or $code_url=='abroad'){ ?>
		<td><? if($oneItem[1]['price']['VALUE']!=0 and $oneItem[1]['min_price']['VALUE']!=0){ echo($oneItem[1]['min_price']['VALUE']!=0 and $oneItem[1]['min_price']['VALUE']!=$oneItem[1]['price']['VALUE'])?'<nobr>'.number_format($oneItem[1]['min_price']['VALUE'], 0, '.', ' ').'</nobr> — ':''; echo($oneItem[1]['price']['VALUE']!=0)?'<nobr>'.number_format($oneItem[1]['price']['VALUE'], 0, '.', ' ').' '.$currencies[$oneItem[1]['currency']['VALUE']]['sign'].'</nobr>':''; }else{ echo '—'; }; ?></td>
	<? }else{ ?>
		<td><nobr><?echo number_format($oneItem[1]['price']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$oneItem[1]['currency']['VALUE']]['sign']; ?></nobr></td>
	<? }; ?>
	<? if($code_url=='abroad'){ ?>
		<td><nobr><?echo '<img src="'.CFile::GetPath($countries[$oneItem[1]['country']['VALUE']][1]).'" height="10" /> '.$countries[$oneItem[1]['country']['VALUE']][0];?></nobr></td>
	<? }; ?>
</tr>