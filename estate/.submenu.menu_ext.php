<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if(CModule::IncludeModule("iblock")) {

	$res = CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => 15, "ACTIVE" => "Y"),
		false,
		false,
		array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "CODE")
	);
	
	$aMenuLinksExt = array();
	
	$aMenuLinksExt[] = array(
		'',
		'#',
		array(),
		array(
			'hidden' => 1,
		),
		""
	);
	
	$aMenuLinksExt[] = array(
		'Продажа',
		'#',
		array(),
		array(
			'header' => 1,
		),
		""
	);

	while($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields(); // берем поля

		if($arFields['CODE']){
			$url = $arFields['CODE'].'/';
		}else{
			$url = $arFields['ID'].'/';
		};
		
		$aMenuLinksExt[] = Array(
			$arFields['NAME'],
			$url,
			Array(),
			Array(),
			""
		);
	}

}

$aMenuLinks = array_merge($aMenuLinks,$aMenuLinksExt);