<? 
	foreach($currencies as $key => $oneCurr){
		if($s['curr_'.$key.'_'.$slide_name]=='on'){
			$showId = $key;
		};
	};
	if(!$showId){ $showId = 20654; };
	
	if($s[$showId.'_'.$slide_name.'-min']!=$s['in-'.$showId.'_'.$slide_name.'-min'] or $s[$showId.'_'.$slide_name.'-max']!=$s['in-'.$showId.'_'.$slide_name.'-max']){
		$b = $block;
		$label = '<label>'.$s[$showId.'_'.$slide_name.'-min'].' — '.$s[$showId.'_'.$slide_name.'-max'].' '.$slide_label.'<a class="dltStation" onclick="clearSlider(\''.$key.'_'.$slide_name.'\',\''.$title[1].'\')">×</a></label>';
		$open = ' open';
	}else{
		$b = '';
		$label = '<label>'.$title[1].'</label>';
		$open = '';
	};
?>
<div class="<?echo $class;?>">
	<div class="oneChose<?echo $open;?>" id="ch<?echo $ch;?>">
		<a class="ajax"><?echo $title[0];?></a>: <?echo $label;?>
	</div>
	<div class="oneChoseVal" id="fch<?echo $ch;?>" <?echo $b;?>>
		<? foreach($currencies as $key => $oneCurr){ ?>
			<div class="currDiv cu<?echo $key;?><? if($key==$showId){ echo ' show'; }; ?>">
			<?
			
				$min = 0;
				$max = round(max($$slide_name)/$divide);
				$s_min = $s[$key.'_'.$slide_name.'-min'];
				$s_max = $s[$key.'_'.$slide_name.'-max'];
				
				if($key!=20654){
					$curr_min = number_format($min/$oneCurr['curr'], 0, '.', ' ');
					$curr_max = number_format($max/$oneCurr['curr'], 0, '.', ' ');
					$fixed = 1;
				}else{
					$curr_min = $min;
					$curr_max = $max;
					$fixed = 0;
				};
				
			?>
			<div class="slider-kon"
				data-postname="<?echo $key.'_'.$slide_name;?>"
				data-currency="1"
				data-ajax="1"
				data-showid="<?echo $showId;?>"
				<?echo($s_min!=$min)?'data-optedmin="'.$s_min.'"':'';echo($s_max!=$max)?'data-optedmax="'.$s_max.'"':'';?>
				data-start="<?echo $curr_min;?>"
				data-finish="<?echo $curr_max;?>"
				data-serif="<?echo $serif;?>"
				data-range="<?echo $range;?>"
				data-label="<?echo $slide_label;?>"
				data-label-hide="<?echo $slide_label_hide;?>"
				data-fixed="<?echo $fixed;?>"></div>
			</div>
		<? }; ?>
	</div>
</div>
<? if($currType==1){$currType=0;}; ?>