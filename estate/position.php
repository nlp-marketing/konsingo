<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<div id="leftArea">
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "estate",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div>
<div id="rightArea">
<?
//подключаем все инфоблоки
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/mainType.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/metro.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/types.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/country.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/currency.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/localy.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/materials.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/realtors.php');

//берем нужный раздел
	$section = CIBlockElement::GetList(
		Array("SORT" => "ASC"),
		Array("IBLOCK_ID" => 15, "ACTIVE"=>"Y", "CODE" => $_GET['SECTION_CODE']), false, false, array("PROPERTY_color", "PROPERTY_iblock_id")
	)->GetNext();
	$sectionId = $section['PROPERTY_IBLOCK_ID_VALUE']; $color = $section['PROPERTY_COLOR_VALUE'];

//берем нужный объект
	$object = CIBlockElement::GetList(
		Array("SORT" => "ASC"),
		Array("IBLOCK_ID" => $sectionId, "ACTIVE"=>"Y", "PROPERTY_old_id" => $_GET['ELEMENT_ID'])
	);
	while($item = $object->GetNextElement()){
		//собираем свой массив со станциями
		$itemId = $item->GetFields();
		$itemProps = $item->GetProperties();
	};	
?>
<?$APPLICATION->SetTitle($itemId['NAME']); ?>
<?$APPLICATION->AddChainItem($main_types[$itemProps['main_type']['VALUE']][0]['NAME'],'/estate/'.$main_types[$itemProps['main_type']['VALUE']][0]['CODE'].'/'); ?>
<?$APPLICATION->AddChainItem($itemId['NAME']); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<style>
	.info-menu li.active a {
		color: #<?echo $main_types[$itemProps['main_type']['VALUE']][1]['color']['VALUE'];?>
	}
</style>
<h1><?echo $itemId['NAME'];?></h1>
<div class="line" style="background:#<?echo $color;?>"></div>
<div id="itemInfo">
	<? if($itemProps['price']['VALUE']!=0){ ?>
	<div class="price"><span style="color:#840000"><?echo number_format($itemProps['price']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$itemProps['currency']['VALUE']]['sign']; ?></span> / <?
	if($itemProps['currency']['VALUE']==20654){
		echo number_format($itemProps['price']['VALUE']/$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20652]['sign'].' / ';
		echo number_format($itemProps['price']['VALUE']/$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20653]['sign'];
	}elseif($itemProps['currency']['VALUE']==20653){
		echo number_format($itemProps['price']['VALUE']*$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20654]['sign'].' / ';
		echo number_format($itemProps['price']['VALUE']*$currencies[20653]['curr']/$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20652]['sign'];
	}elseif($itemProps['currency']['VALUE']==20652){
		echo number_format($itemProps['price']['VALUE']*$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20654]['sign'].' / ';
		echo number_format($itemProps['price']['VALUE']*$currencies[20652]['curr']/$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20653]['sign'];
	};
	?></div>
	<? }; ?>
	<div class="clearfix">
		<div class="half-50">
			<? if($itemProps['country']['VALUE']){ ?>
			<? $flag = CFile::ResizeImageGet($countries[$itemProps['country']['VALUE']][1], array('width'=>15, 'height'=>10), BX_RESIZE_IMAGE_EXACT, true); ?>
			<div class="m-b-15">
				<label>Страна:</label><br/><br/><?echo '<img src="'.$flag['src'].'" /> '.$countries[$itemProps['country']['VALUE']][0];?>
			</div>
			<? }; ?>
			<div class="m-b-15">
				<label>Расположение:</label><br/><?echo $areas[$itemProps['areas']['VALUE']];?><br/><?echo $itemProps['address']['VALUE'];?>
			</div>
			<? if($itemProps['road']['VALUE']){ ?>
			<div class="m-b-15">
			<? echo $roads[$itemProps['road']['VALUE']]; ?>
			</div>
			<? }; ?>
			<div class="m-b-15">
				<?foreach($itemProps['metro']['VALUE'] as $st){ echo '<img src="'.CFile::GetPath($branchs[$stations[$st][0]][2]).'" height="10" /> <strong>'.$stations[$st][1].'</strong><br/>'; }; ?>
				<? if($itemProps['distance']['VALUE']){ $minut = (10*$itemProps['distance']['VALUE'])/1000;  echo '<br/>До ближайшего метро: '.$itemProps['distance']['VALUE'].' м. (Пешком: '.round($minut).' мин.)';}; ?>
			</div>
			<? if($itemProps['mprice']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Цена за метр:</label><br/><span style="color:#840000"><?echo number_format($itemProps['mprice']['VALUE'], 0, '.', ' '); echo ' '.$currencies[$itemProps['currency']['VALUE']]['sign']; ?></span><br/><?
	if($itemProps['currency']['VALUE']==20654){
		echo number_format($itemProps['mprice']['VALUE']/$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20652]['sign'].'<br/>';
		echo number_format($itemProps['mprice']['VALUE']/$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20653]['sign'];
	}elseif($itemProps['currency']['VALUE']==20653){
		echo number_format($itemProps['mprice']['VALUE']*$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20654]['sign'].'<br/>';
		echo number_format($itemProps['mprice']['VALUE']*$currencies[20653]['curr']/$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20652]['sign'];
	}elseif($itemProps['currency']['VALUE']==20652){
		echo number_format($itemProps['mprice']['VALUE']*$currencies[20652]['curr'], 0, '.', ' ').' '.$currencies[20654]['sign'].'<br/>';
		echo number_format($itemProps['mprice']['VALUE']*$currencies[20652]['curr']/$currencies[20653]['curr'], 0, '.', ' ').' '.$currencies[20653]['sign'];
	};
	?></div>
			<? }; ?>
		</div>
		<div class="half-50">
			<div class="m-b-15">
				<label>ID объекта:</label> <?echo $itemProps['articul']['VALUE'];?>
			</div>
			<? if($itemProps['all_s']['VALUE']!=0){ ?>
			<div class="m-b-15">
				<label>Общая площадь:</label> <?echo $itemProps['all_s']['VALUE'];?> м²<? if($itemProps['area_s']['VALUE']){ ?> / <nobr><?echo $itemProps['area_s']['VALUE']; ?> сот.</nobr><? }; ?>
			</div>
			<? }; ?>
			<? if($itemProps['live_s']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Жилая площадь:</label> <?echo $itemProps['live_s']['VALUE'];?> м² <? if($itemProps['room_s']['VALUE']){ echo '('.$itemProps['room_s']['VALUE'].')'; }; ?>
			</div>
			<? }; ?>
			<? if($itemProps['kitchen_s']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Площадь кухни:</label> <?echo $itemProps['kitchen_s']['VALUE'];?> м²
			</div>
			<? }; ?>
			<? if($itemProps['rooms']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Количество комнат:</label> <?echo $itemProps['rooms']['VALUE'];?>
			</div>
			<? }; ?>
			<? if($itemProps['floor']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Этаж:</label> <?echo $itemProps['floor']['VALUE'];?>-й <? if($itemProps['all_floor']['VALUE']>0){ echo ' из '.$itemProps['all_floor']['VALUE']; }; ?>
			</div>
			<? }; ?>
			<? if($itemProps['materials']['VALUE']){ ?>
			<div class="m-b-15">
				<label>Тип дома:</label> <?echo $materials[$itemProps['materials']['VALUE']]; ?>
			</div>
			<? }; ?>
		</div>
		<div class="clear">
			<div class="m-t-10 m-b-15">
				<label>Комментарий:</label>
				<div class="m-t-10 m-b-15"><?echo $itemId['DETAIL_TEXT'];?></div>
			</div>
			<div id="addSee">
				<div id="openZ">
					<img class="m-r-10" src="/images/application/envelope.png" alt="" /><a href="#" class="ajax">Оставить заявку на просмотр</a>
				</div>
				<div id="divZ">
					<h2><img class="m-r-10" src="/images/application/envelope.png" alt="" />Оставить заявку на просмотр</h2>
					<? include($_SERVER['DOCUMENT_ROOT'].'/form/zayavka.php'); ?>
				</div>
			</div>
			<div>
				<label>Контактная информация:</label>
				<? if($itemProps['realtor']['VALUE']!=false){ ?>
					<? foreach($itemProps['realtor']['VALUE'] as $realtor){ ?>
						<div><?echo $realtors[$realtor][0];?>, <?echo $realtors[$realtor][2];?> (<a href="mailto:<?echo $realtors[$realtor][1];?>"><?echo $realtors[$realtor][1];?>)</a></div>
					<? }; ?>
				<? }else{ ?>
					<div>(495) 777-26-20 (<a href="mailto:info@konsingo.ru">info@konsingo.ru</a>)</div>
				<? }; ?>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div id="mainPhoto">
	<? foreach($itemProps['photos']['VALUE'] as $pic){
		$bigPic = CFile::ResizeImageGet($pic, array('width'=>280, 'height'=>280), BX_RESIZE_IMAGE_EXACT, true);
		echo '<a onclick="return hs.expand(this, config1)" id="mp'.$pic.'" href="'.CFile::GetPath($pic).'"><img src="'.$bigPic['src'].'" /></a>';
	}; ?>
	</div>
	<div id="miniPhotos" class="clearfix">
	<? $x=0; foreach($itemProps['photos']['VALUE'] as $pic){ $x++;
		$miniPic = CFile::ResizeImageGet($pic, array('width'=>48, 'height'=>38), BX_RESIZE_IMAGE_EXACT, true);
		echo '<img';
		if($x == 1){
			echo ' class="active"';	 
		}elseif($x%5 == 0){
			echo ' style="margin-right:0;"';	
		};
		echo ' id="p'.$pic.'" src="'.$miniPic['src'].'" />';
	}; ?>
	</div>
</div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>