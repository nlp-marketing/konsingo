<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?
//подключаемся к нужному инфоблоку
	$estate = CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => 15, "ACTIVE"=>"Y", "CODE" => $_GET['url']),
		false,
		false,
		array("DETAIL_TEXT","NAME","PROPERTY_color","PROPERTY_iblock_id","CODE")
	)->GetNext();
	
	$color = $estate['PROPERTY_COLOR_VALUE'];
	$id = $estate['PROPERTY_IBLOCK_ID_VALUE'];
	$code_url = $estate['CODE'];
	
//сессия
	$s = $_SESSION[$code_url];
	
//переменные для стилей
	$block = 'style="display:block;"';
	$active = 'class="active"';
	$checked = 'checked="1"';
	$selected = 'selected="1"';

//подключаем все инфоблоки
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/mainType.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/metro.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/types.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/country.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/currency.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/localy.php');
	include($_SERVER['DOCUMENT_ROOT'].'/getlist/materials.php');

//собираем все объекты нужного раздела
	$sorti = include($_SERVER['DOCUMENT_ROOT'].'/ajax/sorti.php');
	if($id!=NULL){
		$objects = CIBlockElement::GetList(
			$sorti,
			Array("IBLOCK_ID" => $id, "ACTIVE"=>"Y")
		);
	};
	
//объявляем массивы
	$items = array();
	$itemsMaps = array();
	$streets = array();
	$all_s = array();
	$live_s = array();
	$kitchen_s = array();
	$rooms = array();
	$floor = array();
	$floors = array();
	$price = array();
	$mprice = array();
	$itemStations = array();
	
	if($objects){
		while($item = $objects->GetNextElement()){
			
			//собираем свой массив с позициями
			$itemId = $item->GetFields();
			$itemProps = $item->GetProperties();
			$items[$itemId['ID']] = array($itemId,$itemProps);
			//проверяем есть ли координаты Google Maps
			if($itemProps['g_maps']['VALUE']){
				$itemsMaps[$itemId['ID']] = array($itemId,$itemProps);
			};
			
			//собираем улицы для фильтра
			$streets[] = $itemProps['street']['VALUE'];
			
			//станции для фильтра
			$itemStations[] = $itemProps['metro']['VALUE'];
			
			//max/min площади
			$all_s[] = $itemProps['all_s']['VALUE'];
			$live_s[] = $itemProps['live_s']['VALUE'];
			$kitchen_s[] = $itemProps['kitchen_s']['VALUE'];
			$rooms[] = $itemProps['rooms']['VALUE'];
			$floor[] = $itemProps['floor']['VALUE'];
			$floors[] = $itemProps['all_floor']['VALUE'];
			
			//ценник
			if($itemProps['currency']['VALUE']==20652){
				$price[] = $itemProps['price']['VALUE']*$currencies[20652]['curr'];
				$mprice[] = $itemProps['mprice']['VALUE']*$currencies[20652]['curr'];
			}elseif($itemProps['currency']['VALUE']==20653){
				$price[] = $itemProps['price']['VALUE']*$currencies[20653]['curr'];
				$mprice[] = $itemProps['mprice']['VALUE']*$currencies[20653]['curr'];
			}else{
				$price[] = $itemProps['price']['VALUE'];
				$mprice[] = $itemProps['mprice']['VALUE'];
			};
			
		};
	
		$streets = array_unique($streets);
		$all_s = array_unique($all_s);
		$all_s = array_diff($all_s,array(0));
		$live_s = array_unique($live_s);
		$live_s = array_diff($live_s,array(0));
		$kitchen_s = array_unique($kitchen_s);
		$kitchen_s = array_diff($kitchen_s,array(0));
		$rooms = array_unique($rooms);
		$rooms = array_diff($rooms,array(0));
		$floor = array_unique($floor);
		$floor = array_diff($floor,array(0));
		$floors = array_unique($floors);
		$floors = array_diff($floors,array(0));
		$price = array_unique($price);
		$price = array_diff($price,array(0));
		$mprice = array_unique($mprice);
		$mprice = array_diff($mprice,array(0));
		
		foreach($itemsMaps as $key => $oneItem){
			//собираем закрытие блоков для карты
			$closeArray .= 'infowindow'.$key.'.close(); ';
			//собираем все маркеры
			$sectionMarkers .= 'marker'.$key.',';
		}
		
		//убираем последнюю запятую из строки
		$sectionMarkers = substr($sectionMarkers, 0, -1);
	
	};

//собираем все объекты нужного раздела согласно фильтру
	$itemsFilter = array();
	$objectsFl = include($_SERVER['DOCUMENT_ROOT'].'/ajax/getList.php');
	
	if($objectsFl){
		while($itemFl = $objectsFl->GetNextElement()){
			$itemFlId = $itemFl->GetFields();
			$itemsFilter[] = $itemFlId['ID'];
		};
	};
?>
<style>
h1 { color: #<?echo $color;?>; }
#filter { border: 2px solid #<?echo $color;?>; }
#filter .areas ul li.active { border: 1px solid #<?echo $color;?>; }
#objectsTable td { border-bottom: 1px solid #<?echo $color;?>; }
#objectsTable .first td { border-bottom: 2px solid #<?echo $color;?>; }
#detail .info { border: 1px solid #<?echo $color;?>; }
.allItems { border-top: 3px solid #<?echo $color;?>; }
tr.first a.ajax.active { color: #<?echo $color;?>; border-bottom: 1px dashed #<?echo $color;?>; }
</style>
<div id="metroPopups" class="a-mos a-podmos">
	<span class="close"></span>
	<div class="head">
		<img src="/images/subway.png" alt="">Список станций Москвоского метрополитена
	</div>
	<div class="top">
		<a class="ajax show" onclick="showAllStations()">Показать все станции</a>
		<a class="ajax hide" onclick="hideAllStations()">Скрыть станции без объектов</a>
	</div>
	<div id="stations">
		<form>
		<? $itemStationsArr = array(); array_walk_recursive($itemStations, create_function('$i,$k','global $itemStationsArr;if(!in_array($i,$itemStationsArr))$itemStationsArr[]=$i;')); ?>
		<? foreach($branchs as $key => $branch){ ?>
			<ul class="stations">
				<li class="branch"><strong><?echo '<img src="'.CFile::GetPath($branch[2]).'" />'.str_replace(' линия','',$branch[1]); ?></strong></li>
				<? foreach($stations as $skey => $station){ if($station[0]==$key and in_array($skey, $itemStationsArr)){ ?>
					<li class="ok"><input <?echo(in_array($skey, $s['metro']))?$checked:'';?> type="checkbox" id="m<?echo $skey;?>" name="metro[]" value="<?echo $skey;?>" /><label for="m<?echo $skey;?>"><?echo $station[1];?></label></li>
				<? }elseif($station[0]==$key){ ?>
					<li class="none"><?echo $station[1];?></li>
				<? }; }; ?>
			</ul>
		<? }; ?>
		</form>
	</div>
	<div class="chosen" align="center">
		<input type="button" value="Выбрать" onclick="choseStations()" />
		<a class="ajax">Отмена</a>
	</div>
</div>
<div id="leftArea">
	<div id="filter">
		<form>
			<span id="filterName">Фильтр</span>
			<? if($code_url=='commercial'){ ?>
			<div class="areas">
				<ul class="clearfix">
					<li <?echo($s['rental'] && $s['rental']=='all')?$active:''; echo(!$s['rental'])?$active:'';?>>
						<a class="ajax" onclick="setRental('all')">Все</a>
					</li>
					<li <?echo($s['rental'] && $s['rental']==21111)?$active:'';?>>
						<a class="ajax" onclick="setRental(21111)">Аренда</a>
					</li>
					<li <?echo($s['rental'] && $s['rental']==21112)?$active:'';?>>
						<a class="ajax" onclick="setRental(21112)">Продажа</a>
					</li>
				</ul>
			</div>
			<? }; ?>
			<? if($code_url!='abroad'){ ?>
			<div class="areas">
				<ul class="clearfix">
					<li id="all" <?echo(!$s['region'] or $s['region']=='all')?$active:'';?>><a onclick="setRegion('all')" class="ajax">Все</a></li>
					<? foreach($regions as $key => $region){ if(($code_url=='apartments' and $key!=21107) or ($code_url=='country' and $key!=21105) or ($code_url=='commercial')){ ?>
						<li <?echo($s['region']==$key)?$active:'';?>><a onclick="setRegion(<?echo $key;?>)" class="ajax"><?echo $region;?></a></li>
					<? }; }; ?>
				</ul>
			</div>
			<? }; ?>
			<div id="chose">
				<div class="oneFilter a-all">
					<input class="hidden" type="text" value="<?echo $code_url.';'.$id;?>" name="code_id" />
					<input class="hidden" type="text" value="1000000;1000" name="divid"  />
					<input class="hidden" type="text" value="<?echo $s['region'];?>" name="region" id="region" />
					<? if($code_url=='commercial'){ ?>
						<input class="hidden" type="text" value="" name="rental" id="rental" />
					<? }; ?>
					<input class="hidden ASC" type="text" value="<?echo($s['sort'])?$s['sort']:'SORT;ASC';?>"  name="sort" id="sort" />
				</div>
				<?
				
					if($code_url=='abroad' or $code_url=='country' or $code_url=='commercial'){
						
						//! тип
						$ch = 1;
						$title = array('Тип','Любой');
						if($code_url=='abroad'){
							$s_select = $s['abroad_type'];
							$radio_name = 'abroad_type';
							$radio_array = $abroad_types;
						}elseif($code_url=='country'){
							$s_select = $s['country_type'];
							$radio_name = 'country_type';
							$radio_array = $country_types;
						}elseif($code_url=='commercial'){
							$s_select = $s['commercial_type'];
							$radio_name = 'commercial_type';
							$radio_array = $commercial_types;
						};
						
						if((!isset($s_select) or in_array('all', $s_select))){
							$b = NULL;
							$check = $checked;
							$open = NULL;
							$label = $title[1];
						}else{
							$b = $block;
							$check = NULL;
							$open = ' open';
							$label = '';
							foreach($s_select as $s_sel){ $label .= ' '.$radio_array[$s_sel][0]; };
						};
						$class = 'oneFilter a-all';
						
						include('f-checkbox.php');
						
					}else{
				
						//! тип
						$ch = 1;
						$title = array('Тип','все');
						$s_select = $s['type'];
						$radio_name = 'type';
						
						if((!isset($s_select) or $s_select=='all')){
							$b = NULL;
							$check = $checked;
							$open = NULL;
							$label = $title[1];
						}else{
							$b = $block;
							$check = NULL;
							$open = ' open';
							$label = $types[$s_select];
						}
												
						$radio_array = $types;
						$class = 'oneFilter a-all';
					
						include('f-radiobuttons.php');
					
					};
				
					if($code_url=='abroad'){
					
						//! страна
						$ch++;
						$title = array('Страна','Любая');
						$s_select = $s['countries'];
						$radio_name = 'countries';
						$radio_array = $countries;
						
						if((!isset($s_select) or in_array('all', $s_select))){
							$b = NULL;
							$check = $checked;
							$open = NULL;
							$label = $title[1];
						}else{
							$b = $block;
							$check = NULL;
							$open = ' open';
							$label = '';
							foreach($s_select as $s_sel){ $label .= ' <img src="'.CFile::GetPath($radio_array[$s_sel][1]).'" height="10" /> '.$radio_array[$s_sel][0]; };
						};
						$class = 'oneFilter';
						
						include('f-checkbox.php');
					
					};
					
					if($code_url=='apartments' or $code_url=='commercial'){
					
						//! округ
						$title = array('Округ','Любой');
						$s_select = $s['areas'];
						$select_name = 'areas';
						$select_array = $areas;
						$class = 'oneFilter a-mos';
						if($s['region']==21106 or $s['region']==21107){ $class = $class.' off'; };
					
						include('f-select.php');
					
					
						?>
						
						<?
							$class = 'oneFilter a-mos a-podmos';
							if($s['region']==21107){ $class = $class.' off'; };
						?>
						
						<div class="<?echo $class;?>">
							<!--! метро виджет -->
							<div class="oneChose" id="chMetro">
								<a class="ajax">Станция метро</a>: <?echo(isset($s) and $s['metro'])?'<label></label>':'<label>Любая</label>';?>
							</div>
							<div class="oneChoseVal" id="metroList" <?echo(isset($s) and $s['metro'])?$block:'';?>>
								<? if(isset($s) and $s['metro']){ ?>
									<div id="findMetroId">
										<ul>
											<? foreach($s['metro'] as $st){ ?>
												<li id="<?echo $st;?>"><input type="text" value="<?echo $st;?>" style="display:none;" name="metro[]" /><img src="<?echo CFile::GetPath($branchs[$stations[$st][0]][2]);?>" height="10" /><?echo $stations[$st][1];?><a class="dltStation" onclick="unsetStation(<?echo $st;?>)">×</a></li>
											<? }; ?>
										</ul>
									</div>
								<? }; ?>
							</div>
						</div>
						
						<?
						//! расстояние до метро
						$ch++;
						$title = array('Расстояние до метро','Любое');
						$s_input = $s['distance'];
						$select_name = 'distance';
						if($s_input){ $open = ' open'; }else{ $open = NULL; };
						$unit = 'м';
						$class = 'oneFilter a-mos a-podmos';
						if($s['region']==21107){ $class = $class.' off'; };
						
						include('f-input.php');
						
						//! расположение
						$ch++;
						$title = array('Расположение','произвольное');
						$s_select = $s['location'];
						$radio_name = 'location';
					
						if((!isset($s_select) or $s_select=='all')){
							$b = NULL;
							$check = $checked;
							$open = NULL;
							$label = $title[1];
						}else{
							$b = $block;
							$check = NULL;
							$open = ' open';
							$label = $locations[$s_select];
						};
						$radio_array = $locations;
						$class = 'oneFilter a-mos';
						if($s['region']==21106 or $s['region']==21107){ $class = $class.' off'; };
						
						include('f-radiobuttons.php');
					
					};
					
					if($code_url=='country' or $code_url=='apartments' or $code_url=='commercial'){
					
					//! направление
					$title = array('Направление','Любое');
					$s_select = $s['direction'];
					$select_name = 'direction';
					$select_array = $directions;
					$class = 'oneFilter a-podmos';
					if($s['region']==21105 or $s['region']==21107){ $class = $class.' off'; };
			
					include('f-select.php');
					
					};
					
					if($code_url=='country' or $code_url=='commercial'){
						
						//! области
						$title = array('Регион','Любой');
						$s_select = $s['oblast'];
						$select_name = 'oblast';
						$select_array = $oblasty;
						$class = 'oneFilter a-region';
						if($s['region']==21105 or $s['region']==21106){ $class = $class.' off'; };
						
						include('f-select.php');
						
						//! дорога
						$title = array('Дорога','Любая');
						$s_select = $s['road'];
						$select_name = 'road';
						$select_array = $roads;
						$class = 'oneFilter a-podmos';
						if($s['region']==21105 or $s['region']==21107){ $class = $class.' off'; };
						
						include('f-select.php');
						
					};
					
					if($code_url=='country' or $code_url=='apartments' or $code_url=='commercial'){
						
						//! населенный пункт
						$title = array('Населенный пункт','Любой');
						$s_select = $s['localitie'];
						$select_name = 'localitie';
						$select_array = $localyes;
						if($code_url=='country'){
							$class = 'oneFilter a-region';
							if($s['region']==21105 or $s['region']==21106){ $class = $class.' off'; };
						}elseif($code_url=='apartments'){
							$class = 'oneFilter a-podmos';
							if($s['region']==21105 or $s['region']==21107){ $class = $class.' off'; };
						}
					
						include('f-select.php');
					
						//! расстояние до Москвы
						$ch++;
						$title = array('Расстояние до Москвы','любое');
						$s_input = $s['localy_distane'];
						$select_name = 'localy_distane';
						if($s_input){ $open = ' open'; }else{ $open = NULL; };
						$unit = 'км';
						$class = 'oneFilter a-podmos';
						if($s['region']==21105 or $s['region']==21107){ $class = $class.' off'; };
					
						include('f-input.php');
					
					};
					
					//! пошли слайдерые с площадью и ценой
	
					$ch++;
					if($code_url=='apartments'){
						$title = array('Общая площадь','Любая');
					}elseif($code_url=='country'){
						$title = array('Площадь дома','Любая');
					}elseif($code_url=='abroad' or $code_url=='commercial'){
						$title = array('Площадь','Любая');
					};
					$divide = 1;
					$min = floor(min($all_s));
					$max = round(max($all_s)/$divide);
					$s_min = $s['all_s-min'];
					$s_max = $s['all_s-max'];
					$slide_name = 'all_s';
					$slide_label = '';
					$slide_label_hide = ' м²';
					$range = 1;
					$serif = 10;
					$class = 'oneFilter a-all';
					
					include('f-slider.php');
						
					if($code_url=='apartments'){
						
						$ch++;
						$title = array('Жилая площадь','Любая');
						$divide = 1;
						$min = floor(min($live_s));
						$max = round(max($live_s)/$divide);
						$s_min = $s['live_s-min'];
						$s_max = $s['live_s-max'];
						$slide_name = 'live_s';
						$slide_label = '';
						$slide_label_hide = ' м²';
						$range = 1;
						$serif = 10;
						$class = 'oneFilter a-all';
						
						include('f-slider.php');
						
						$ch++;
						$title = array('Площадь кухни','Любая');
						$divide = 1;
						$min = floor(min($kitchen_s));
						$max = round(max($kitchen_s)/$divide);
						$s_min = $s['kitchen_s-min'];
						$s_max = $s['kitchen_s-max'];
						$slide_name = 'kitchen_s';
						$slide_label = '';
						$slide_label_hide = ' м²';
						$range = 1;
						$serif = 10;
						$class = 'oneFilter a-all';
						
						include('f-slider.php');
						
					};
					
					if($code_url=='apartments' or $code_url=='abroad' or $code_url=='commercial'){
						
						$ch++;
						$title = array('Количество комнат','Любое');
						$divide = 1;
						$min = 1;
						$max = round(max($rooms)/$divide);
						if($_GET['rooms']){
							if($_GET['rooms']==4){
								$s_min = 4;
								$s_max = $s['rooms-max'];
							}else{
								$s_min = $s['rooms'];
								$s_max = $s['rooms'];
							}
						}else{
							$s_min = $s['rooms-min'];
							$s_max = $s['rooms-max'];
						};
						$slide_name = 'rooms';
						$slide_label = '';
						$slide_label_hide = '';
						$range = 100/($max-$min);
						$serif = $max-$min;
						$class = 'oneFilter a-all';
						
						include('f-slider.php');
					
					};
					
					if($code_url=='apartments' or $code_url=='commercial'){
						
						$ch++;
						$title = array('Этаж','Любой');
						$divide = 1;
						$min = 1;
						$max = round(max($floor)/$divide);
						$s_min = $s['floor-min'];
						$s_max = $s['floor-max'];
						$slide_name = 'floor';
						$slide_label = '';
						$slide_label_hide = '';
						$range = 100/($max-$min);
						if($max>10){
							$serif = 10;
						}else{
							$serif = $max-1;
						};
						$class = 'oneFilter a-all';
						
						include('f-slider.php');
						
						$ch++;
						$title = array('Количество этажей','Любое');
						$divide = 1;
						$min = 1;
						$max = round(max($floors)/$divide);
						$s_min = $s['floors-min'];
						$s_max = $s['floors-max'];
						$slide_name = 'floors';
						$slide_label = '';
						$slide_label_hide = '';
						$range = 100/($max-$min);
						$serif = 10;
						$class = 'oneFilter a-all';
						
						include('f-slider.php');
					
					};
					
					$ch++;
					$title = array('Общая цена','Любая');
					$divide = 1000000;
					$slide_name = 'price';
					$slide_label = ' млн.';
					$slide_label_hide = ' млн.';
					$range = 1;
					$serif = 5;
					$class = 'oneFilter a-all';
					$currType = 1;
					
					include('f-slider-curr.php');
					
					if($code_url=='apartments'){

						$ch++;
						$title = array('Цена за метр','Любая');
						$divide = 1000;
						$slide_name = 'mprice';
						$slide_label = ' тыс.';
						$slide_label_hide = ' тыс.';
						$range = 1;
						$serif = 5;
						$class = 'oneFilter a-all';
						$currType = 1;
						
						include('f-slider-curr.php');
					
					};
					
				?>
				
			</div>
		</form>
		<div id="unsetFilter" <?echo(isset($s))?$block:'';?> class="padd17 m-b-10">
			<a class="ajax" onclick="unsetFilter('<?echo $code_url;?>')">Сбросить фильтр</a>
		</div>
	</div>
</div>
<div id="rightArea">
<? $APPLICATION->SetTitle($estate['NAME']); ?>
<? $APPLICATION->AddChainItem($estate['NAME']); ?>
<? $APPLICATION->IncludeComponent( "bitrix:breadcrumb", "", Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s1")); ?>
<h1><?echo $estate['NAME'];?></h1>
<div id="showHideMap">
	<img src="/images/map.png" alt="">
	<a id="show">Показать объекты на карте</a>
	<a id="hide">Скрыть карту</a>
</div>
<div>
	<? //var_dump($s); ?>
</div>
<div id="map_holder">
	<div id="map_canvas" style="width:100%;"></div>
</div>
<div id="objectsTable">
<?
//вывод таблицы	
	include($_SERVER['DOCUMENT_ROOT'].'/estate/table-first.php');
	foreach($items as $key => $oneItem){ if(in_array($key, $itemsFilter)){
		include($_SERVER['DOCUMENT_ROOT'].'/estate/table-row.php');
	}; };
	include($_SERVER['DOCUMENT_ROOT'].'/estate/table-off.php');
?>
</div>
<div id="detail">
	<div class="info"><?echo $estate['DETAIL_TEXT'];?></div>
</div>
<?
$objects = array();

foreach($itemsMaps as $key => $oneItem){
		$coords = explode(',', $oneItem[1]['g_maps']['VALUE']);
		$img = '';
		foreach($oneItem[1]['photos']['VALUE'] as $pic){
			$miniPic = CFile::ResizeImageGet($pic, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_EXACT, true);
			$img = '<img class="small" src="'.$miniPic['src'].'" />';
			break;
		}
		$objects[] = array(
			'id'=>$key,
			'marker'=>array(
				'icon'=>'/images/estate/'.$_GET['url'].'/icon.png',
				'lat'=>(float)$coords[0], // Широта
				'long'=>(float)$coords[1], // Долгота

			),
			'info'=>'<div class="g_maps_window">'.
					'<a class="pic" href="//'.$_SERVER['HTTP_HOST'].'/estate/'.$code_url.'/'.$oneItem[1]['old_id']['VALUE'].'/">'.
					$img.
					'</a>'.
					'<div><strong><a href="//'.$_SERVER['HTTP_HOST'].'/estate/'.$code_url.'/'.$oneItem[1]['old_id']['VALUE'].'/">'.
					$oneItem[0]['NAME'].
					'</a></strong></div>'.
					'<div><strong>Общая площадь:</strong>'.$oneItem[1]['all_s']['VALUE'].' м²</div>'.
					'<div><strong>Жилая площадь:</strong>'.$oneItem[1]['live_s']['VALUE'].' м²</div>'.
					'<div><strong>Площадь кухни:</strong>'.$oneItem[1]['kitchen_s']['VALUE'].' м²</div>'.
					'<div><strong>Общая цена:</strong> '.$oneItem[1]['price']['VALUE'].' р.</div>'.
					'<div><strong>Цена за метр:</strong> '.$oneItem[1]['mprice']['VALUE'].' р.</div>'.
				'</div>'
		);
		
}
?>

<script type="text/javascript">

$('.slider-kon').smartSlider();

var markersArray = [];
function initialize() {
	//При первой загрузке запускаем плагин
	var mm = window.MM = new MapMaster("map_canvas",{
		map:{
			center: new google.maps.LatLng(55.749792,37.6324949),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		},
		mcUrl:"/images/estate/<?echo $_GET['url'];?>/cluster.png",
		mcHoverUrl:"/images/estate/<?echo $_GET['url'];?>/cluster-a.png",
	});
	// Далее добавляем объекты на карту
	objs = <? print json_encode($objects)?>; // массив собирается выше, обрати внимание что хотя я его и закодировал в json
											 // в переменной objs будет не json ытрока, а уже готовый js объект
											 // видимо это осбенность работы метода PHP print
	// Сюда мы передаем не json строку, а полноценный Object
	// Этот метод собирает на карте все объекты
	mm.loadObjects(objs);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>