<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<div id="leftArea">
	<div id="filter">
	<span id="filterName">Фильтр</span>
	<div id="areas">
		<ul class="clearfix">
			<li id="all" class="active"><a onclick="" class="ajax">Все</a></li>
			<li id=""><a onclick="" class="ajax">Продажа</a></li>
			<li id=""><a onclick="" class="ajax">Аренда</a></li>
		</ul>
	</div>
	<div id="estateType">
		<span class="inputName">Виды объектов:</span>
		<? $estateTypeList = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 15, "ACTIVE"=>"Y")); ?>
		<ul>
			<li>
				<input type="radio" name="part" class="" id="typeAll" value="" checked="" onclick="">
				<label for="typeAll">Все</label>
			</li>
			<? while($one = $estateTypeList->GetNextElement()){ $oneId = $one->getFields(); $oneProps = $one->GetProperties(); ?>
				<li class="">
					<input type="radio" name="part" class="" id="type<?echo $oneId['ID'];?>" value="" onclick="">
					<label for="type<?echo $oneId['ID'];?>"><?echo $oneId['NAME'];?></label>
					<span class="circle" style="background: #<?echo $oneProps['color']['VALUE']; ?>"></span>
				</li>
			<? }; ?>
		</ul>
	</div>
	<div class="padd17"><span class="note">Выберите конкретный вид, чтобы получить возможность фильтровать объекты по отдельным параметрам.</span></div>
</div>
</div>
<div id="rightArea">
<?$APPLICATION->SetTitle("Продажа и аренда недвижимости"); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	)
);?>
<h1>Продажа и аренда недвижимости</h1>
<div id="map_holder" style="height:600px;margin:0 0 30px 0;">
	<div id="map_canvas" style="width:100%;"></div>
</div>
<div id="estate">
	<? //собираем все типы недвижимости
	$estateTypes = array(); $estateType = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 15, "ACTIVE"=>"Y")); 
	while($one = $estateType->GetNextElement()){ $oneId = $one->getFields(); $oneProps = $one->GetProperties(); $estateTypes[$oneId['ID']] = array($oneId,$oneProps); ?>
		<div class="estateType">
			<div class="head" style="border-bottom:4px solid #<?echo $oneProps['color']['VALUE']; ?>"><a href="/estate/<?echo $oneId['CODE'];?>/"><?echo $oneId['NAME'];?></a></div>
			<div class="text"><?echo $oneId['PREVIEW_TEXT'];?></div>
		</div>
	<? }; ?>
</div>
<?
	//все объекты в массив
	$itemsMaps = array();
	//массив для кластеризации маркеров
	$markersClaster = array();
	
	//собираем все объекты в массив по типу недвижимости
	foreach($estateTypes as $estType){
		
		//очищаем маркеры предыдущего раздела
		$estMarkers = NULL;
		
		//проверяем привязан ли раздел к инфоблоку
		if($estType[1]['iblock_id']['VALUE']){
			$objects = CIBlockElement::GetList(Array("NAME" => "ASC"), Array("IBLOCK_ID" => $estType[1]['iblock_id']['VALUE'], "ACTIVE"=>"Y"));
			while($item = $objects->GetNextElement()){
				//собираем все данные
				$itemId = $item->GetFields();
				$itemProps = $item->GetProperties();
				//проверяем есть ли координаты Google Maps
				if($itemProps['g_maps']['VALUE']){
					//собираем 
					$itemsMaps[$itemId['ID']] = array($itemId,$itemProps,$estType[0]['CODE']);
					//собираем закрытие маркеров для карты
					$closeArray .= 'infowindow'.$itemId['ID'].'.close(); ';
					//собираем все маркеры для одного раздела
					$estMarkers .= 'marker'.$itemId['ID'].',';
				};
			};
			//убираем последнюю запятую из строки
			$estMarkers = substr($estMarkers, 0, -1);
			//кладем в массив
			$markersClaster[] = array($estMarkers,$estType[0]['CODE']);
		};
		
	};

?>
</div>
<script type="text/javascript">
var markersArray = [];
function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(51.588451,23.657886),
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);
		
	<? foreach($itemsMaps as $key => $oneItem){ ?>
		
		var marker<?echo $key;?> = new google.maps.Marker({
			icon: '/images/estate/<?echo $oneItem[2];?>/icon.png',
			position: new google.maps.LatLng(<?echo $oneItem[1]['g_maps']['VALUE'];?>),
			map: map,
		});
		
		markersArray.push(marker<?echo $key;?>);
		
		var contentString<?echo $key;?> = '<div class=\"g_maps_window\">'+
			'<div><strong><a href=\"http://<?echo $_SERVER['HTTP_HOST'];?>/estate/apartments/<?echo $oneItem[1]['old_id']['VALUE'];?>\"><?echo $oneItem[0]['NAME'];?></a></strong></div>'+
			'<div><strong>Общая площадь:</strong> <?echo $oneItem[1]['all_s']['VALUE']; ?> м²</div>'+
			'<div><strong>Жилая площадь:</strong> <?echo $oneItem[1]['live_s']['VALUE']; ?> м²</div>'+
			'<div><strong>Площадь кухни:</strong> <?echo $oneItem[1]['kitchen_s']['VALUE']; ?> м²</div>'+
			'<div><strong>Общая цена:</strong> <?echo $oneItem[1]['price']['VALUE']; ?>	р.</div>'+
			'<div><strong>Цена за метр:</strong> <?echo $oneItem[1]['mprice']['VALUE']; ?> р.</div>'+
		'</div>';
		
		var infowindow<?echo $key;?> = new google.maps.InfoWindow({
		    content: contentString<?echo $key;?>
		});
		
		google.maps.event.addListener(marker<?echo $key;?>, 'click', function() {
			close();
			infowindow<?echo $key;?>.open(map,marker<?echo $key;?>);
		});
		
	<? }; ?>
	
	<? foreach($markersClaster as $mkCl){ ?>
		
		var markers = [];
		markers.push(<?echo $mkCl[0];?>);
		var styles = [[{
			width:21,
			height:33,
			url:"/images/estate/<?echo $mkCl[1];?>/cluster.png",
			opt_markerAnchor:[26,8],
			opt_hoverUrl:"/images/estate/<?echo $mkCl[1];?>/cluster-a.png",
			textColor: "#fff",
			textSize: 10,
			anchor:[3],
		}]];
		var mcOptions = {gridSize: 20, maxZoom: 16, styles: styles[0]};
		var markerCluster = new MarkerClusterer(map, markers,mcOptions);
		
	<? }; ?>
	
	function close(){
		<?echo $closeArray;?> 
	}
	
};
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>