<?
//! линии
	$lines = CIBlockElement::GetList(
		Array("SORT" => "ASC"),
		Array("IBLOCK_ID" => 6, "ACTIVE"=>"Y")
	);
	$branchs = array();
	while($branch = $lines->GetNextElement()){	
		//! свой массив со станциями
		$branchId = $branch->GetFields();
		$branchProps = $branch->GetProperties();
		$branchs[$branchId['ID']] = array($branchId['ID'],$branchId['NAME'],$branchProps['pic']['VALUE']);
	};

//! все станции метро
	$metro = CIBlockElement::GetList(
		Array("NAME" => "ASC"),
		Array("IBLOCK_ID" => 11, "ACTIVE"=>"Y")
	);
	$stations = array();
	while($station = $metro->GetNextElement()){	
		//! свой массив со станциями
		$stationId = $station->GetFields();
		$stationProps = $station->GetProperties();
		$stations[$stationId['ID']] = array($stationProps['branch']['VALUE'],$stationId['NAME']);
	};
?>