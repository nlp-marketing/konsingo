<?
//! типы
	$type = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 22, "ACTIVE"=>"Y")
	);
	$types = array();
	while($item = $type->GetNextElement()){
		$typeId = $item->GetFields();
		$typeProps = $item->GetProperties();
		$types[$typeId['ID']] = $typeId['NAME'];
	};

//! типы недвижимости за границей
	$abroad_type = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 27, "ACTIVE"=>"Y")
	);
	$abroad_types = array();
	while($item = $abroad_type->GetNextElement()){
		$abroadId = $item->GetFields();
		$abroadProps = $item->GetProperties();
		$abroad_types[$abroadId['ID']] = array($abroadId['NAME']);
	};
	
//! типы загородной недвижимости
	$country_type = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 26, "ACTIVE"=>"Y")
	);
	$country_types = array();
	while($item = $country_type->GetNextElement()){
		$countrId = $item->GetFields();
		$countrProps = $item->GetProperties();
		$country_types[$countrId['ID']] = array($countrId['NAME']);
	};

//! типы коммерческой недвижимости
	$commercial_type = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 29, "ACTIVE"=>"Y")
	);
	$commercial_types = array();
	while($item = $commercial_type->GetNextElement()){
		$commId = $item->GetFields();
		$commProps = $item->GetProperties();
		$commercial_types[$commId['ID']] = array($commId['NAME']);
	};
?>