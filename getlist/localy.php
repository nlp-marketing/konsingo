<?
//! выбираем все округа
	$area = CIBlockElement::GetList(
		Array("SORT" => "ASC"),
		Array("IBLOCK_ID" => 10, "ACTIVE"=>"Y")
	);
	$areas = array();
	while($item = $area->GetNextElement()){ 
		$areaId = $item->getFields();
		$areaProps = $item->GetProperties();
		$areas[$areaId['ID']] = $areaId['NAME'];
	};

//! населенные пункты
	$localy = CIBlockElement::GetList(
		Array("NAME" => "ASC"), 
		Array("IBLOCK_ID" => 8, "ACTIVE"=>"Y")
	);
	$localyes = array();
	while($item = $localy->GetNextElement()){
		$locId = $item->GetFields();
		// $locProps = $item->GetProperties();
		$localyes[$locId['ID']] = $locId['NAME'];
	};
	
//! области
	$oblast = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 9, "ACTIVE"=>"Y")
	);
	$oblasty = array();
	while($item = $oblast->GetNextElement()){
		$oblId = $item->GetFields();
		$oblProps = $item->GetProperties();
		$oblasty[$oblId['ID']] = $oblId['NAME'];
	};

//! дороги
	$road = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y")
	);
	$roads = array();
	while($item = $road->GetNextElement()){
		$roadId = $item->GetFields();
		$roadProps = $item->GetProperties();
		$roads[$roadId['ID']] = $roadId['NAME'];
	};

//! расположение
	$location = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 24, "ACTIVE"=>"Y")
	);
	$locations = array();
	while($item = $location->GetNextElement()){
		$locatId = $item->GetFields();
		$locatProps = $item->GetProperties();
		$locations[$locatId['ID']] = $locatId['NAME'];
	};
	
//! варианты расположения
	$region = CIBlockElement::GetList(
		Array("SORT" => "ASC"), 
		Array("IBLOCK_ID" => 23, "ACTIVE"=>"Y")
	);
	$regions = array();
	while($item = $region->GetNextElement()){
		$regionId = $item->GetFields();
		$regionProps = $item->GetProperties();
		$regions[$regionId['ID']] = $regionId['NAME'];
	};

//! направления
	$direction = CIBlockElement::GetList(
		Array("NAME" => "ASC"), 
		Array("IBLOCK_ID" => 7, "ACTIVE"=>"Y")
	);
	$directions = array();
	while($item = $direction->GetNextElement()){
		$directId = $item->GetFields();
		$directProps = $item->GetProperties();
		$directions[$directId['ID']] = $directId['NAME'];
	};
?>