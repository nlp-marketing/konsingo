<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?$APPLICATION->SetTitle('Покупка недвижимости в ипотеку в агентстве недвижимости «Консинго». Кредитный (ипотечный) калькулятор для расчета ипотечного кредита'); ?>
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?> 
</div>
<div id="rightArea">
<? $APPLICATION->AddChainItem('Покупка недвижимости с использованием ипотечного кредита'); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	)
);?>
	<h1>Покупка недвижимости с использованием ипотечного кредита</h1>
	
	<p>Высокая цена на недвижимость в Москве вынуждает многих пользоваться ипотечными кредитами для улучшения жилищных условий. При кажущейся простоте процедуры большое количество времени занимает сбор документов для подачи заявки в банк, одобрение кредита. И это после того как Вы проведете большую работу по выбору более выгодного предложения среди огромного количества ипотеченых продуктов. Без грамотного специалиста есть опасность, что банк не даст одобрение на ту квартиру, которую Вы себе подобрали после длительного поиска.</p>
	<p>Профессионалы нашего агентства грамотно и быстро подберут для Вас индивидуально кредитную программу, которая минимизирует Ваши затраты, как материальные так и моральные.</p>
	<p></p>
	<h2>Кредитный калькулятор</h2>
	<p>Кредитный (ипотечный) калькулятор позволяет расчитать график погашения кредита и переплаты по нему. Результаты, рассчитанные калькулятором, приблизительны. Для получения точных результатов воспользуйтесь кредитными калькуляторами банков-партнеров.</p>
	<? include($_SERVER['DOCUMENT_ROOT'].'/form/calculator.php'); ?>
	<h2>Кредитные калькуляторы банков-партнеров</h2>
	<noindex>
		<ul>
			<li>— <a href="http://alfabank.ru/retail/credit/mortgage/calculator/" rel="nofollow">Альфа-банк</a></li>
			<li>— <a href="http://www.deltacredit.ru/mortgage_calculator/" rel="nofollow">DeltaCredit Банк</a></li>
			<li>— <a href="http://www.bsgv.ru/individual/loans/mortgage/ipo_calc.php?bc_tovar_id=2" rel="nofollow">Банк Сосьете Женераль Восток (БСЖВ)</a></li>
		</ul>
	</noindex>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>