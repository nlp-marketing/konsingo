<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?$APPLICATION->SetTitle('Продажа загородной недвижимости в Подмосковье по лучшим ценам — агентство недвижимости «Консинго»'); ?>
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?> 
</div>
<div id="rightArea">
<? $APPLICATION->AddChainItem('Загородная недвижимость в Подмосковье'); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	)
);?>
	<h1>Загородная недвижимость в Подмосковье</h1>
	
	<p>С каждым годом спрос на недвижимость в Подмосковье возрастает. Это вполне логично и не вызывает удивления, потому что москвичи ищут возможность отдохнуть от сумасшедшего ритма жизни в огромном мегаполисе, а загородный дом идеально подходит для этого. А кто-то уже имеет дом под Москвой и хочет улучшить свои жилищные условия, не возвращаясь в столицу. В любом случае, если возникла необходимость купить или продать земельный участок в московской области, то агентство недвижимости «Консинго» к Вашим услугам.</p>
	<p>Работая на рынке недвижимости уже 10 лет, мы собрали большую клиентскую базу, что помогает осуществлять сделки в самые кратчайшие сроки с максимальной выгодой, как для покупателя, так и для продавца.</p>
	<p><a href="/estate/country/?location=21106&type=21117">Продажа домов в Подмосковье</a></p>
	<p><a href="/estate/apartments/?location=21106&type=21104">Продажа комнат в Подмосковье</a></p>
	<p><a href="/estate/apartments/?location=21106&type=21103">Продажа квартир в Подмосковье</a></p>
	<p><a href="/estate/country/?location=21106&type=21114">Продажа таунхаусов в Подмосковье</a></p>
	<p><a href="/estate/country/?location=21106&type=21113">Продажа участков в Подмосковье</a></p>
	<p>Мы предлагаем нашим клиентам полный комплекс услуг по сопровождению сделки:</p>
	<ul>
		<li>— персональный менеджер будет постоянно находиться на связи, информируя и помогая на всех этапах подбора или реализации собственности</li>
		<li>— полное юридическое сопровождение защит Ваши интересы. Вам окажут помощь в оформлении и проверке документов и при регистрации сделки.</li>
	</ul>
	<p>Агентство недвижимости «Консинго» обладает наиболее полной информацией о ситуации на рынке недвижимости Подмосковья, что позволяет упростить и ускорить процесс при купле-продаже объектов загородной недвижимости.</p>

	<? include($_SERVER['DOCUMENT_ROOT'].'/form/form.php'); ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>