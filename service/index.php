<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?$APPLICATION->SetTitle('Услуги агентства недвижимости «Консинго»'); ?>
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?> 
</div>
<div id="rightArea">
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"",
		Array(
			"START_FROM" => "0",
			"PATH" => "",
			"SITE_ID" => "-"
		)
	);?>
	<h1>Услуги агентства недвижимости «Консинго»</h1>
	
	<p><a href="/price.doc"><img src="/images/fticons/xls.png" alt="" /></a> <a href="/price.doc">скачать прайс</a></p>
	<p>Начиная с 2002 года, компания «Консинго» предоставляет широкий спектр услуг на рынке недвижимости России и стран зарубежья.</p>
	<p>Обратившись в агентство недвижимости «Консинго», вы всегда сможете получить консультацию по любому интересующему Вас вопросу в области недвижимости у наших специалистов.</p>
	
	<p><a href="/service/apartments/#message">Задать вопрос по покупке и продаже квартир и комнат</a></p>
	
	<p><a href="/service/country/#message">Задать вопрос по загородной недвижимости в Подмосковье</a></p>
	
	<p>Когда Вам нужно произвести какую-либо операцию с недвижимостью (продать, купить и обменять квартиру или комнату, арендовать офис или другую коммерческую недвижимость) — обратитесь к нам, и мы справимся с поставленной задачей.</p>
	<p></p>
	<div class="banners clearfix">
		<div id="bn-1" class="banner">
		<div class="pic"><a href="/service/mortgage/"><img src="//konsingo.ru/_images/thumb_f55c6a_thumb_ddedef_7d08abcf88801c8386ad835c53c7e5ba.png" alt="" /></a></div>
		<div class="head"><a href="/service/mortgage/">Кредитный калькулятор</a></div>
	</div>
	</div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>