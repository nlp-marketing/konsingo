function setItemPosition(){
    var wrapper = $('#stations'),
        wrapperWidth = wrapper.outerWidth(),
        items = wrapper.find('.stations'),
        itemWidth = $(items[0]).outerWidth(),
        margin = wrapperWidth/100,
        cols,
        stack;

        cols = 4;
        stack = {
            col0: 0,
            col1: 0,
            col2: 0,
            col3: 0
        }

    for(var i=0;i<items.length;i++){
        var curItem = $(items[i]),
            textHeight = curItem.outerHeight(),
            curItemHeight = textHeight,
            min,
            mg;

        if(i<cols) {
            stack['col'+i]=curItemHeight;
            mg = i%cols*margin;
            curItem.css({top:0,left: itemWidth*i+mg + 'px'});

        } else {
            min = getMinCol();
            mg =  + min*margin;
            curItem.css({top: stack['col'+min]+ margin*2 + 'px', left:itemWidth*min+mg + 'px'});
            stack['col'+min] += curItemHeight+margin*2;
        }
    }

    wrapper.css('height',getMaxCol() + 'px')

    function getMinCol(){
        var min,
            val;
        for(var col in stack) {
            if(!val||val>stack[col]){
                val = stack[col];
                min = col.replace('col','');
            }
        }
        return min;
    }

    function getMaxCol(){
        var val;
        for(var col in stack) {
            if(!val||val<stack[col]){
                val = stack[col];
            }
        }
        return val;
    }

}

function showAllStations(){
	$('#metroPopups .top .show').hide();
	$('#metroPopups .top .hide').show();
	$('#stations .none').show();
	setItemPosition();
}
function hideAllStations(){
	$('#metroPopups .top .hide').hide();
	$('#metroPopups .top .show').show();
	$('#stations .none').hide();
	setItemPosition();
}

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        setItemPosition();
    }
}
$(document).ready(function(){
    setTimeout(setItemPosition,100);
});
$(window).resize(setItemPosition);