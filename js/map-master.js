(function(){
	var markerCluster, activeObjs = {}, mcOptions, max = {lat:null,long:37.926379}, min = {lat:55.556477,long:37.311145};
	
	var Map = window.MapMaster = function(id,options){
		this.init(id,options);
	}

	Map.prototype.init = function(id,options){
		this.map = new google.maps.Map(document.getElementById(id),options.map);
		mcOptions = {
			gridSize: 20,
			maxZoom: 16,
			styles: [{
				width:21,
				height:33,
				url:options.mcUrl,
				opt_markerAnchor:[26,8],
				opt_hoverUrl:options.mcHoverUrl,
				textColor: '#fff',
				textSize: 10,
				anchor:[3],
			}]
		}
	}

	Map.prototype.loadObjects = function(objs) {
		activeObjs = {};
		min = {lat:55.556477,long:37.311145};
		max = {lat:55.910609,long:37.926379};
		this.addObjects(objs);
	};

	Map.prototype.addObjects = function(objs) {
		for(var i=0,len=objs.length;i<len;++i){
			this.addObject(objs[i],false);
		}
		reloadCluster(this.map);
	}

	Map.prototype.addObject = function(obj,addClaster) {
		if(addClaster === 'undefined') addClaster = true;
		if(activeObjs['id'+obj.id] instanceof Object)
			return;
		obj2 = createObject(this.map,obj);
		activeObjs['id'+obj.id]=obj2;

		if(addClaster)
			addToCluster(this.map,obj2.marker);
		
	}

	function addToCluster(map,marker){
		if(!(markerCluster instanceof MarkerClusterer))
			return;
		markerCluster.addMarker(marker);
		var minCoords = new google.maps.LatLng(min.lat,min.long);
		var maxCoords = new google.maps.LatLng(max.lat,max.long);
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(minCoords);
		bounds.extend(maxCoords);
		map.fitBounds(bounds);
	}

	function reloadCluster(map) {
		var markers = [];
		for(var key in activeObjs){
			markers.push(activeObjs[key].marker);
		}

		if(markerCluster instanceof MarkerClusterer){
			markerCluster.clearMarkers();
			markerCluster.addMarkers(markers);
		} else {
			markerCluster = new MarkerClusterer(map, markers, mcOptions);
		}
		var minCoords = new google.maps.LatLng(min.lat,min.long);
		var maxCoords = new google.maps.LatLng(max.lat,max.long);
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(minCoords);
		bounds.extend(maxCoords);
		map.fitBounds(bounds);
	}

	function createObject(map,obj) {
		var marker = createMarker(map,obj.marker);
		var infoWin = createInfoWindow(obj.info);
		google.maps.event.addListener(marker, 'click', function() {
			closeInfoWindows();
			infoWin.open(map,marker);
		});
		return {marker:marker,win:infoWin};
	}

	function createMarker(map,data) {
		if(min.lat==null)
			min.lat = data.lat;
		else
			min.lat = Math.min(data.lat,min.lat);

		if(min.long==null)
			min.long = data.long;
		else
			min.long = Math.min(data.long,min.long);

		if(max.lat==null)
			max.lat = data.lat;
		else
			max.lat = Math.max(data.lat,max.lat);

		if(max.long==null)
			max.long = data.long;
		else
			max.long = Math.max(data.long,max.long);


		return new google.maps.Marker({
			icon: data.icon,
			position: new google.maps.LatLng(data.lat,data.long)
		});
	}

	function collectContent(data) {
		return data;/*'<div class="g_maps_window">'+
			'<a class="pic" href="'+data.objectUrl+'"><img class="small" src="'+data.thumbnail+'"></a>'+
			'<div><strong><a href="'+data.objectUrl+'">'+data.address+'</a></strong></div>'+
			'<div><strong>Общая площадь:</strong> '+data.totalArea+'</div>'+
			'<div><strong>Жилая площадь:</strong> '+data.liveArea+'</div>'+
			'<div><strong>Площадь кухни:</strong> '+data.kitchenArea+'</div>'+
			'<div><strong>Общая цена:</strong> '+data.totalPrice+'</div>'+
			'<div><strong>Цена за метр:</strong> '+data.pricePerM+'</div>'+
		'</div>'*/
	}

	function createInfoWindow(data) {
		return new google.maps.InfoWindow({content: collectContent(data)});
	}

	function closeInfoWindows(){
		for(var key in activeObjs){
			activeObjs[key].win.close();
		}
	}


})();