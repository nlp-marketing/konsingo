$(document).ready( function(){
	
});

(function(){
	function start(item){
		var start = item.data('start'),
			finish = item.data('finish'),
			optedmin = item.data('optedmin'),
			optedmax = item.data('optedmax'),
			range = item.data('range'),
			serif = item.data('serif')?item.data('serif'):10,
			diff = (finish-start)/serif,
			divide = item.data('divide')?item.data('divide'):1,
			postname = item.data('postname'),
			label = item.data('label')?item.data('label'):'',
			labelHide = item.data('label-hide')?item.data('label-hide'):label,
			min = optedmin?optedmin:(Math.floor(start/divide)).toFixed(0),
			max = optedmax?optedmax:(finish/divide).toFixed(0),
			startVal = optedmin?optedmin:start,
			finishVal = optedmax?optedmax:finish,
			ajax = item.data('ajax')?item.data('ajax'):0,
			currency = item.data('currency')?item.data('currency'):0,
			showid = item.data('showid'),
			fixed = item.data('fixed')?item.data('fixed'):0;

		item.slider({
			range: true,
			values: [ startVal*divide, finishVal*divide ],
			min:start,
			max:finish,
			step: (finish-start)/100*range,
			slide: function( event, ui ) {
				var id = $(this).parent('div').parent('div').attr('id'),
					idLabel = id.substr(1);
				$('#' + id + ' .min[name="'+postname+'-min"]').val(Math.floor(ui.values[ 0 ]/divide).toFixed(fixed));
				$('#' + id + ' .max[name="'+postname+'-max"]').val((ui.values[ 1 ]/divide).toFixed(fixed));
				$('#' + idLabel + ' label').html(Math.floor(ui.values[ 0 ]/divide).toFixed(fixed)+' — '+(ui.values[ 1 ]/divide).toFixed(fixed)+labelHide+'<a class="dltStation" onclick="clearSlider(\''+postname+'\')">×</a>');
			},
			start: function( event, ui ) {
				$('#objectsTable').css('opacity','0.5');
			},
			stop: function( event, ui ) {
				if(ajax!=0){
					setFilter();
				};
			},
		});
		
		var	serifStr = "",
			width = 100/serif;
		
		for(var i = 0; i<serif; i++){
			//serifStr += '<div>';
			if(i==0){
				serifStr += '<span class="sf-one" style="width:'+width+'%;">'+(Math.floor(start/divide)).toFixed(0)+'</span>';
			}else if(i==(serif-1)){
				serifStr += '<span class="sf-one" style="width:'+width+'%;text-align:right;"><span class="numeric">'+(((i*diff)+start)/divide).toFixed(fixed)+label+'</span><span class="last">'+(finish/divide).toFixed(fixed)+'</span></span>';
			}else{
				serifStr += '<span class="sf-one" style="width:'+width+'%;"><span class="numeric">'+(((i*diff)+start)/divide).toFixed(fixed)+label+'</span></span>';
			};
			//serifStr += '</div>';
		};
		
		$('<div class="serif clearfix">'+serifStr+'</div>').appendTo(item);
		$('<span>от </span><input onkeyup="setSliderDelay(\''+postname+'-min\')" name="'+postname+'-min" class="min" type="text" size="4" value="'+min+'" /><span> до </span><input onkeyup="setSliderDelay(\''+postname+'-max\')" name="'+postname+'-max" class="max" type="text" size="4" value="'+max+'" /><span>'+label+'</span>').insertBefore(item);
		$('<input name="in-'+postname+'-min" class="hidden" type="text" value="'+start+'" /><input name="in-'+postname+'-max" class="hidden" type="text" value="'+finish+'" />').insertBefore(item);
		if(currency==1){
			$('<div class="currency clearfix">'+
				'<ul>'+
					'<li>'+
						'<input class="currSet" onclick="changeCurrency(20654)" id="20654_'+postname+'" type="radio" name="curr_'+postname+'" />'+
						'<label for="20654_'+postname+'">рублей</label>'+
					'</li>'+
					'<li>'+
						'<input class="currSet" onclick="changeCurrency(20652)" id="20652_'+postname+'" type="radio" name="curr_'+postname+'" />'+
						'<label for="20652_'+postname+'">долларов</label>'+
					'</li>'+
					'<li>'+
						'<input class="currSet" onclick="changeCurrency(20653)" id="20653_'+postname+'" type="radio" name="curr_'+postname+'" />'+
						'<label for="20653_'+postname+'">евро</label>'+
					'</li>'+
				'</ul>'+
			'</div>').insertBefore(item);
			$('#'+showid+'_'+postname).attr('checked','checked');
		};
	}
	
	jQuery.fn.smartSlider = function(){
		$(this).each(function(){
			var item = $(this);
			if(!item.data('smartSlider')){
				start(item);
				item.data('smartSlider',true);
			}
		});
		return $(this);
	}
})()