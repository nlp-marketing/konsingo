$(document).ready( function(){

	$('#openSubmenu').mouseover( function(){
		$('.institutes-menu').show();
	}).mouseout( function(){
		$('.institutes-menu').mouseover( function(){
			$('.institutes-menu').show();
		}).mouseout( function(){
			$('.institutes-menu').hide();
		});
	});
	
	$('#showHideMap #show').click( function(){
		if($('#map_holder').is(":animated") == false){ 
			$(this).hide();
			$('#showHideMap #hide').show();
			$('#map_holder').animate({
				height: "600px",
				marginBottom: "40px",
			}, 1000 );
		}
	});
	$('#showHideMap #hide').click( function(){
		if($('#map_holder').is(":animated") == false){ 
			$(this).hide();
			$('#showHideMap #show').show();
			$('#map_holder').animate({
				height: "0px",
				marginBottom: "0px",
			}, 1000 );
		}
	});
	$('#areas ul li a').click( function(){
		$('#areas ul li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('.areas ul li a').click( function(){
		$(this).parent('li').parent('ul').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('.oneChoseVal ul li a').click( function(){
		$(this).parent('li').parent('ul').parent('.oneChoseVal').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('#specOffers .tabs a').click( function(){
		$('#specOffers .tabs a').removeClass('active');
		$(this).addClass('active');
		var id = $(this).attr('id');
		$('#specOffers .offersTab').removeClass('active');
		$('#specOffers #o-'+id).addClass('active');
	});
	
	$('#miniPhotos img').click( function(){
		$('#miniPhotos img').removeClass('active');
		$(this).addClass('active');
		$('#mainPhoto a').hide();
		$('#mainPhoto #m'+$(this).attr('id')).show();
	});
	
	$('.prew-pic .small').mouseover( function(){
		$(this).parent('.prew-pic').find('.big').fadeIn(200);
	});
	$('.prew-pic .big').mouseout( function(){
		$('.prew-pic .big').fadeOut(200);
	});
	
	$('.oneChose a.ajax').click( function(){
		var id = $(this).parent().attr('id');
		$(this).parent().toggleClass('open');
		if($(this).parent().hasClass('open')){ 
			$('#f'+id).fadeIn(200);
		}else{
			$('#f'+id).fadeOut(200);
		}
	});
	
	$('span.close').click( function(){
		$(this).parent().fadeOut();
	});
	$('#metroPopups .chosen a.ajax, #metroPopups span.close').click( function(){
		$('#metroPopups').fadeOut();
		$('#chMetro').removeClass('open');
	});
	
	$('#chMetro').click( function(){
		if($(this).hasClass('open')){
			$('#metroPopups').fadeIn();
			setItemPosition();
		}else{
			$('#metroPopups').fadeOut();
		}
	})
	
	$('tr.first a.ajax').click( function(){
		$('tr.first a.ajax').removeClass('active');
		$(this).addClass('active');
	});
	
	$('#glossary').bind('keyup change', function(ev) {

        var searchTerm = $(this).val();
	
        $('.glossary').removeHighlight();
		
        if(searchTerm.length>2){
            $('.glossary').highlight(searchTerm);
            $('.glossary').addClass('hidden');
			$('.glossary').each( function(indx, element){
				$(element).find('span.highlight').parents('.glossary').removeClass('hidden');
			});
        }else{
	        $('.glossary').removeClass('hidden');
        }
    });
    
    $('#openZ').click( function(){
    	$(this).hide();
		$('#divZ').fadeIn();
    });
	
});

var delay = (function() {
	var timer = 0;
	return function(callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();

function clearSlider(name, label){
	
	var	slider = '.slider-kon[data-postname="'+name+'"]';
		min = $(slider).attr('data-start'),
		max = $(slider).attr('data-finish');
	
	$('input[name="'+name+'-min"]').val(min);
	$('input[name="'+name+'-max"]').val(max);
	
	$(slider).parents('.oneFilter').find('.oneChose label').html(label);
	$(slider).slider({
		values: [ min, max ],
	});
	setFilter();
}

function setType(url){
	$('#filter form').attr('action','/estate/'+url+'/');
	$('#regionTypes ul li').each(function(){
		if(!$(this).hasClass(url)){
			$(this).addClass('hidden');
		}else{
			$(this).removeClass('hidden');
		};
	});
}

function changeCurrency(id){
	
	$('input.currSet').removeAttr('checked');
	$('input#'+id+'_'+id+'_price').attr('checked','checked');
	//$('input#'+id+'_20653_price').attr('checked','checked');
	//$('input#'+id+'_20654_price').attr('checked','checked');
	$('input#'+id+'_'+id+'_mprice').attr('checked','checked');
	//$('input#'+id+'_20653_mprice').attr('checked','checked');
	//$('input#'+id+'_20654_mprice').attr('checked','checked');
	$('.currDiv').removeClass('show');
	$('.currDiv.cu'+id).addClass('show');
	setFilter();

}

function setRental(id,name){
	
	$('input#rental').val(id);
	$('#objectTypes ul li').each(function(){
		if(!$(this).hasClass(name)){
			$(this).addClass('hidden');
		}else{
			$(this).removeClass('hidden');
		};
	});
	setFilter();

}

function setRegion(id){

	var on = '';

	if(id == 'all'){
		on = 'all';
	}else if(id == 21105){
		on = 'a-mos';
	}else if(id == 21106){
		on = 'a-podmos';
	}else if(id == 21107){
		on = 'a-region';
	};
	
	$('#filter .oneFilter').each(function(){
		if(on == 'all'){
			$(this).removeClass('off');
		}else if(!$(this).hasClass(on) && !$(this).hasClass('a-all')){
			$(this).addClass('off');
		}else{
			$(this).removeClass('off');
		};
	});

	$('input#region').val(id);
	setFilter();
}

function setRadio(id){
	
	$('#'+id).attr('checked', 'checked');

	var label = $('#'+id).parent('li').find('label').html(),
		idName = $('#'+id).parent('li').parent('ul').parent('div.oneChoseVal').attr('id');
	
	idName = idName.substr(1);
	
	$('#'+idName+' label').html(label);
	setFilter();
}

function setCheckbox(id){

	var id = explode('-',id),
		check = 0,
		value = '',
		idParent = $('.oneChoseVal #'+id[0]+'-all').parent('li').parent('ul').parent('div.oneChoseVal').attr('id').substr(1);
	
	if(id[1]=='all'){
		$('.oneChoseVal input[name="'+id[0]+'[]"]').each( function(){
			$(this).removeAttr('checked', '');
		});
		$('.oneChoseVal #'+id[0]+'-all').attr('checked', 'checked');
		$('#'+idParent+' label').html($('.oneChoseVal label[for="'+id[0]+'-all"]').html());
	}else{
		$('#'+idParent+' label').html();
		$('.oneChoseVal input[name="'+id[0]+'[]"]').each( function(){
			if($(this).attr('checked')){
				check = check + 1;
				value = value + ' ' + $(this).parent('li').find('label').html();
			};
		});
		if(check==0){
			$('.oneChoseVal #'+id[0]+'-all').attr('checked', 'checked');
			$('#'+idParent+' label').html($('.oneChoseVal label[for="'+id[0]+'-all"]').html());
		}else{
			$('.oneChoseVal #'+id[0]+'-all').removeAttr('checked', '');
			$('#'+idParent+' label').html(value);
		};
	};
	
	setFilter();
}

function unsetStation(id){
	//удаляем
	$('#findMetroId #'+id).remove();
	$('#stations .stations #m'+id).removeAttr('checked', '');
	if($('#metroList ul li').length==0){
		$('#chMetro label').html('Любая');
	}
	setFilter();
}

function choseStations(data){
	
	$('#metroPopups').fadeOut();
	$('#metroList').show();
	$('#chMetro').removeClass('open');
	$('#chMetro label').html('');
	
	var form = $("#stations form").serialize();
	
	$.ajax({
		type: 'post',
		url: '/ajax/setmetro.php',
		data: form + '&' + data,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			$('#metroList').empty();
			$('#metroList').html(html);
			$('#objectsTable').css('opacity','1');
			$('#loaderImage').css('display','none');
			setFilter();
		}
	});
	
}

function setSliderDelay(name){
	
	var item = explode('-',name),
		min = 0,
		max = 0;
	
	if(item[1]=='min'){
		min = $('.oneChoseVal input[name="'+name+'"]').val();
		max = $('.oneChoseVal input[name="'+item[0]+'-max"]').val();	
	}else if(item[1]=='max'){
		max = $('.oneChoseVal input[name="'+name+'"]').val();
		min = $('.oneChoseVal input[name="'+item[0]+'-min"]').val();	
	};
	
	delay(function(){
		$('.slider-kon[data-postname="'+item[0]+'"]').slider({
			values: [ min, max ],
		});
		setFilter();
	}, 1000);
	
}

function setFilterDelay(name,unit){
	
	var input = $('[name="'+name+'"]'),
		id = input.parent().attr('id').substr(1),
		label = $('#'+id+' label');
		
	if(input.val()==0){
		label.html('любое');
	}else{
		label.html('до '+input.val()+' '+unit);
	}
	
	delay(function(){
		setFilter();
	}, 1000);
	
}

function setSort(name){
	
	var last_name = $('#sort').val();
	last_name = explode(';',last_name);
	last_name = last_name[0];
	
	if(name==last_name){
		
		if($('#sort').hasClass('ASC')){
			$('#sort').removeClass('ASC');
			$('#sort').addClass('DESC');
			$('#sort').val(name+';DESC');
		}else if($('#sort').hasClass('DESC')){
			$('#sort').removeClass('DESC');
			$('#sort').addClass('ASC');
			$('#sort').val(name+';ASC');
		};	
		
	}else{
	
		if($('#sort').hasClass('ASC')){
			$('#sort').val(name+';ASC');
		}else if($('#sort').hasClass('DESC')){
			$('#sort').val(name+';DESC');
		};
		
	};
	
	setFilter();
}

function setFilter(){
	
	$('#objectsTable').css('opacity','0.5');
	
	var form = $('#filter form').serialize();
	
	$.ajax({
		type: 'post',
		url: '/ajax/estate.php',
		data: form,
		dataType: 'json',
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(data){
			$('#objectsTable').html('');
			$('#objectsTable').html(data.html);
			$('#objectsTable').css('opacity','1');
			MM.loadObjects(data.forMap);
			$('#unsetFilter').fadeIn();
			$('#loaderImage').css('display','none');
		}
	});
}

function unsetFilter(url){
	
	$('#objectsTable').css('opacity','0.5');
	
	$.ajax({
		type: 'post',
		url: '/ajax/session.php',
		data: 'url=' + url,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(){
			var url = window.location.href;
			url = url.split("?")[0];
			window.location = url;
		}
	});
	
}

function setArea(area,section){
	
	//проверяем метро
	var stations = getStations();
	//проверяем улицы
	var streets = getStreets();

	$.ajax({
		type: 'post',
		url: '/ajax/filter.php',
		data: 'area=' + area + '&section=' + section + '&stations=' + stations + '&streets=' + streets,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			$('#objectsTable').empty();
			$('#objectsTable').html(html);
			$('#loaderImage').css('display','none');
		}
	});
}
function setMetro(metro,section,img,label){
	
	//проверяем метро
	var stations = getStations();
	//проверяем район
	var area = getArea();
	//проверяем улицы
	var streets = getStreets();
	
	$.ajax({
		type: 'post',
		url: '/ajax/filter.php',
		data: 'metro=' + metro + '&section=' + section + '&stations=' + stations + '&area=' + area + '&streets=' + streets,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			$('<li id="'+metro+'"></li>').append(img).append(label).append('<a onclick="dltStation('+metro+',\''+section+'\')" class="dltStation">×</a>').appendTo('#findMetroId ul');
			$('#objectsTable').empty();
			$('#objectsTable').html(html);
			$('#loaderImage').css('display','none');
		}
	});
}
function setStreet(street,section){
	
	//проверяем метро
	var stations = getStations();
	//проверяем район
	var area = getArea();
	//проверяем улицы
	var streets = getStreets();
	
	$.ajax({
		type: 'post',
		url: '/ajax/filter.php',
		data: 'street=' + street + '&section=' + section + '&stations=' + stations + '&area=' + area + '&streets=' + streets,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			var	num = $('#findStreetId ul li').length; num = num + 1;
			$('<li id="'+num+'"><num value="'+street+'">'+num+'.</num></li>').append(street).append('<a onclick="dltStreet('+num+',\''+section+'\')" class="dltStation">×</a>').appendTo('#findStreetId ul');
			$('#objectsTable').empty();
			$('#objectsTable').html(html);
			$('#loaderImage').css('display','none');
		}
	});
	
}
function dltStation(id,section){

	//удаляем
	$('#findMetroId #'+id).remove();
	//проверяем метро
	var stations = getStations();
	//проверяем район
	var area = getArea();
	//проверяем улицы
	var streets = getStreets();
	
	$.ajax({
		type: 'post',
		url: '/ajax/filter.php',
		data: 'section=' + section + '&stations=' + stations + '&area=' + area + '&streets=' + streets,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			$('#objectsTable').empty();
			$('#objectsTable').html(html);
			$('#loaderImage').css('display','none');
		}
	});
}
function dltStreet(id,section){

	//удаляем
	$('#findStreetId #'+id).remove();
	//проверяем метро
	var stations = getStations();
	//проверяем район
	var area = getArea();
	//проверяем улицы
	var streets = getStreets();
	
	$.ajax({
		type: 'post',
		url: '/ajax/filter.php',
		data: 'section=' + section + '&stations=' + stations + '&area=' + area + '&streets=' + streets,
		beforeSend: function(){
			$('#loaderImage').css('display','block');
		},
		success: function(html){
			$('#objectsTable').empty();
			$('#objectsTable').html(html);
			$('#loaderImage').css('display','none');
		}
	});
}
function getArea(){
	var id;
		$('#areas ul li').each(function(){
		if($(this).hasClass('active')){
			id = $(this).attr('id');
		}
	});
	return id;
}
function getStations(){
	var stations = [];
	$('#findMetroId ul li').each(function(){
		stations.push($(this).attr('id'));
	});
	return stations;
}
function getStreets(){
	var streets = [];
	$('#findStreetId ul li num').each(function(){
		streets.push($(this).attr('value'));
	});
	return streets;
}