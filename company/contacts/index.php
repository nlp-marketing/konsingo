<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<div id="leftArea"> 	<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?> </div>
<div id="rightArea"><? $APPLICATION->AddChainItem('Контактная информация'); ?><?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	)
);?> 
  <h1>Контактная информация</h1>
  <p>
<strong>ЗАО «Консалтинговая Инжинирингово-Внедренческая Многопрофильная Управляющая Компания „Консинго“»</strong>
</p>
<p style="text-align: left; ">
<strong>105005, Россия, </strong>
г. Москва, ул. Бауманская д. 43, строение 2
</p>
<p style="text-align: left; ">
<strong>Время работы: </strong>
Пн. — Пт. с 9:00 до 20:30 
<br>
<br>
<strong>Телефоны: </strong>
<span id="phone_contact">
(495) 777-26-20, 925-88-13, (499) 267-34-10, 267-55-30, 
</p>
<p style="text-align: left; ">
<strong>Отдел подбора персонала:</strong>
 (495)
<span style="white-space: nowrap">741-30-71</span>
,
<span style="white-space: nowrap">741-33-41</span>
</p>
<p><strong>Карта проезда</strong></p>
<img src="/images/mapcontact.png" />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>