<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?$APPLICATION->SetTitle("О компании"); ?> 
<div id="leftArea">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div> 
<div id="rightArea">
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"",
		Array(
			"START_FROM" => "0",
			"PATH" => "",
			"SITE_ID" => "-"
		)
	);?> 
	<h1>О компании Недвижимости &laquo;Консинго&raquo;</h1>
	
	<p><img src="/upload/medialibrary/c22/c22e435e7a2afd949109110d1b903219.jpg" title="thumb_b0430c_04d4d017e8b1f6078fbb6a8e342fd4cc.jpg" border="0" alt="thumb_b0430c_04d4d017e8b1f6078fbb6a8e342fd4cc.jpg" width="406" height="234"  /></p>
	
	<p>С 2002 года Агентство недвижимости &laquo;Консинго&raquo; начало свою деятельность на рынке недвижимоти. За 9 лет успешной деятельности в этой области зарекомендовало себя как надежный партнер, предоставляя услуги по продаже покупке как элитного жилья, так и жилья эконом-класса. Также имеет большой опыт работы с объектами загородной и зарубежной недвижимости.</p>
	
	<p> Главное преимущество работы с нами &mdash; это индивидуальный подход к каждому клиенту, отсутствие скрытых комиссий, высокий профессионализм сотрудников, гарантия качественной юридической поддержки. 
	<br />
	<strong>Соблюдение Ваших интересов &mdash; наша главная задача.</strong> </p>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php") ?>