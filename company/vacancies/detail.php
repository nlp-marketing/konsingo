<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $month = array('01'=>'января','02'=>'февраля','03'=>'марта','04'=>'апреля','05'=>'мая','06'=>'июня','07'=>'июля','08'=>'августа','09'=>'сентября','10'=>'октября','11'=>'ноября','12'=>'декабря');
$objects = CIBlockElement::GetList(
	Array("SORT" => "ASC"), 
	Array("IBLOCK_ID" => 4, "ACTIVE"=>"Y", "ID"=>$_GET['news'])
);
while($item = $objects->GetNextElement()){
	$itemId = $item->GetFields();
	$itemProps = $item->GetProperties();
};
?>
<? $APPLICATION->SetTitle($itemId['NAME']); ?>
<div id="leftArea">
<? $APPLICATION->AddChainItem('Вакансии','/company/vacancies'); ?>
<? $APPLICATION->AddChainItem($itemId['NAME']); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"info",
	Array(
		"ROOT_MENU_TYPE" => "submenu",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array()
	)
);?>
</div> 
<div id="rightArea">
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"",
		Array(
			"START_FROM" => "0",
			"PATH" => "",
			"SITE_ID" => "-"
		)
	);?> 
	<h1><?echo $itemId['NAME'];?></h1>
	<div class="m-b-10">
		<span class="news-date-time"><? $date = explode('.',$itemId['DATE_ACTIVE_FROM']); echo $date[0].' '.$month[$date[1]].' '.$date[2];  ?></span>
	</div>
	<div class="m-b-10">
		<img class="preview_picture" border="0" src="<?echo CFile::GetPath($itemId["DETAIL_PICTURE"]);?>" height="240" alt="<?=$itemId["NAME"]?>" title="<?=$itemId["NAME"]?>" />
	</div>
	<div class="text"><? if($itemProps['abzac']['VALUE_ENUM_ID']==5){ echo $itemId['PREVIEW_TEXT']; }; echo $itemId['DETAIL_TEXT'];?></div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>